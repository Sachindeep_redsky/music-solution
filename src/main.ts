import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { enableProdMode } from '@angular/core';
import { AppModule } from "./app/app.module";
const firebase = require("nativescript-plugin-firebase");

firebase.init({}).then(
    () => {
        console.log("firebase.init done");
    },
    error => {
        console.log(`firebase.init error: ${error}`);
    }
);

enableProdMode();
platformNativeScriptDynamic({ createFrameOnBootstrap: true }).bootstrapModule(
    AppModule
);