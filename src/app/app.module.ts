import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { UserService } from "./services/user.service";
import { NgModalModule } from "./modal/ng-modal";
import { NativeScriptUIAutoCompleteTextViewModule } from "nativescript-ui-autocomplete/angular/autocomplete-directives";
import { NativeScriptUIGaugeModule } from "nativescript-ui-gauge/angular/gauges-directives";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NotificationService } from "./services/notification.service";
import { SongService } from "./services/song.service";
import { RecentlyPlayedService } from "./services/recently-played.service";
import { PlayerModule } from './shared/player/player.module';
import { FooterModule } from "./shared/footer/footer.module";
import { NotificationActionHandler } from "./services/notification.action.handler";
import { TNSImageCacheItModule } from 'nativescript-image-cache-it/angular';

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        HttpModule,
        HttpClientModule,
        NativeScriptHttpModule,
        NativeScriptHttpClientModule,
        NativeScriptUISideDrawerModule,
        NativeScriptUIAutoCompleteTextViewModule,
        NativeScriptUIGaugeModule,
        NativeScriptCommonModule,
        NgModalModule,
        NativeScriptFormsModule,
        PlayerModule,
        FooterModule,
        TNSImageCacheItModule
    ],
    declarations: [
        AppComponent,
    ],
    providers: [UserService, NotificationService, SongService, RecentlyPlayedService, NotificationActionHandler],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
