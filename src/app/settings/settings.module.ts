import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { SettingsComponent } from './components/settings.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { PlayerModule } from './../shared/player/player.module';

@NgModule({
  imports: [
    SettingsRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule,
    PlayerModule
  ],
  declarations: [SettingsComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class SettingsModule { }
