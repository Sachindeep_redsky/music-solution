import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { HttpHeaders } from '@angular/common/http';
import { UserService } from '~/app/services/user.service';
import { Page } from 'tns-core-modules/ui/page/page';
import { Values } from '~/app/values/values';

@Component({
  selector: 'ns-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})

export class SettingsComponent implements OnInit {

  backButton: string;
  headerText: string;
  changePasswordHeading: string;
  logoutHeading: string;
  headers: HttpHeaders;
  backbtn: string;
  user: any;
  isRendering: boolean;

  constructor(private routerExtensions: RouterExtensions, private userService: UserService,
    private page: Page) {

    this.page.actionBarHidden = true;

    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });

      if (Values.readString(Values.USER, '')) {
        this.user = JSON.parse(Values.readString(Values.USER, ''));
      }
    }
  }

  ngOnInit() {

    this.headerText = "Settings";
    this.changePasswordHeading = "Change Password";
    this.logoutHeading = "Logout"
    this.backbtn = "res://back_black";
    this.userService.showHideFooter(true);
    this.userService.showHidePlayer(false);
    this.userService.activeScreen("setting");
    this.page.on('navigatedTo', (data) => {
      if (data.isBackNavigation) {
        this.userService.activeScreen("setting");
        this.userService.showHideFooter(true);
        this.userService.showHidePlayer(false);
      }
    })

  }

  onBack() {
    this.routerExtensions.back();
  }

  onChangePassword() {
    this.routerExtensions.navigate(['/change-password']);
  }

  onLogout() {
    this.userService.loginUpdate(false)
  }

}
