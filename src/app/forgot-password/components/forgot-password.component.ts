import { Component, OnInit, ViewChild } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpClient } from "@angular/common/http";
import { ad } from "tns-core-modules/utils/utils"
import { Values } from "~/app/values/values";
import { ModalComponent } from "~/app/modal/modal.component";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { UserService } from "~/app/services/user.service";

@Component({
    moduleId: module.id,
    selector: "m-forgotPassword",
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.css'],
})

export class ForgotPasswordComponent implements OnInit {

    @ViewChild('errorDialog', { static: false }) errorDialog: ModalComponent;

    pageHeading: string;
    emailHint: string;
    email: string;
    submitButton: string;
    signIn: string;
    appLogo: string;
    errorText: string;
    isLoading: boolean;
    constructor(private page: Page, private http: HttpClient, private routerExtensions: RouterExtensions, private userService: UserService) {
        this.page.actionBarHidden = true;
    }

    ngOnInit() {

        this.userService.showHideFooter(false);
        this.userService.showHidePlayer(false);
        this.userService.activeScreen("forget-password");
        this.pageHeading = "Forgot Password?";
        this.emailHint = "Email id";
        this.email = "";
        this.submitButton = "SUBMIT";
        this.signIn = "SIGN IN";
        this.appLogo = "res://logo";
        this.errorText = "";
        this.isLoading = false;
    }

    onEmailTextChange(args: any) {
        var textField = <TextField>args.object;
        this.email = textField.text;
    }

    onSubmitButton() {

        ad.dismissSoftInput();
        if (this.email == "") {
            this.errorDialog.show();
            this.errorText = "Please enter email";
            return;
        }
        else {
            this.isLoading = true;
            var forgotPasswordModel = {
                email: this.email
            }

            var headers = {
                'Content-Type': 'application/json'
            }

            this.http.post(Values.BASE_URL + `users/forgotPassword`, forgotPasswordModel, { headers: headers }).subscribe((res: any) => {
                if (res && res.isSuccess && res.data) {

                    let extendedNavigationExtras: ExtendedNavigationExtras = {
                        queryParams: {
                            email: this.email,
                            from: 'forgot-password'
                        },
                    };
                    this.isLoading = false;

                    this.routerExtensions.navigate(['/verify-otp'], extendedNavigationExtras);
                }
            }, (error) => {
                console.log('Error:', error)
                this.isLoading = false;
                if (error.error.statusCode == 404 && error.error.error == "user_not_found") {
                    this.errorText = "This user is not registered"
                } else {
                    this.errorText = "Something went wrong";
                }
                this.errorDialog.show();
            })
        }
    }

    onSignin() {
        this.routerExtensions.navigate(['/login']);
    }

    onErrorOKTap($event) {

    }

}