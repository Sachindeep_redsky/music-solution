import { ForgotPasswordRoutingModule } from './forgot-password-routing.module';
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NgModalModule } from "../modal/ng-modal";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ForgotPasswordComponent } from "./components/forgot-password.component";


@NgModule({
    bootstrap: [
        ForgotPasswordComponent
    ],
    imports: [
        HttpModule,
        NativeScriptCommonModule,
        ForgotPasswordRoutingModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        NgModalModule
    ],
    declarations: [
        ForgotPasswordComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ForgotPasswordModule { }
