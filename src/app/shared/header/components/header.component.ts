import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";


@Component({
    selector: "ns-header",
    moduleId: module.id,
    templateUrl: "./header.component.html",
    styleUrls: ["./header.component.css"]
})

export class HeaderComponent implements OnInit {
    backIcon: string;
    headerLabel: string;
    showHeader: boolean;
    showBackButton: string;
    screen: string;

    constructor(private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.backIcon = "res://back";
        this.headerLabel = "";
        this.showHeader = true;
        this.showBackButton = "visible";
        this.screen = "";
    }

    ngOnInit(): void {

    }

    onBackClick() {
        this.routerExtensions.back();
    }

    onAddClick() {
        if (this.screen == "categories") {
            this.routerExtensions.navigate(['/addCategory']);
        }
        if (this.screen == "products") {
            this.routerExtensions.navigate(['/addProduct']);
        }
    }
}
