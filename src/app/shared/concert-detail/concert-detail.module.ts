import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ConcertDetailComponent } from "./components/concert-detail.component";


@NgModule({
    imports: [NativeScriptCommonModule],
    declarations: [ConcertDetailComponent],
    schemas: [NO_ERRORS_SCHEMA],
    exports: [ConcertDetailComponent]
})

export class ConcertDetailModule { }