import { UserService } from '~/app/services/user.service';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpClient } from "@angular/common/http";
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';

@Component({
    selector: "ns-concertDetail",
    moduleId: module.id,
    templateUrl: "./concert-detail.component.html",
    styleUrls: ['./concert-detail.component.css'],
})

export class ConcertDetailComponent {

    isLoading: boolean;
    errorText: string;
    concertName: string;
    artistName: string;
    venueHeading: string;
    venueDetail: string;
    dateTimeHeading: string;
    dateTimeDetail: string;
    ticketPrice: string;
    ticketPriceHeading: string;
    ticketBuyButton: string;
    closeLabel: string;
    concertDetail: any;
    inStock: boolean;
    stockLabel: string;
    ticketQuantityHeading: string;
    minusButton: string;
    plusButton: string;
    ticketQuantity: number;
    remainingTickets: string;
    concertId: string;

    constructor(private page: Page, private http: HttpClient, private routerExtensions: RouterExtensions, private userService: UserService) {
        this.page.actionBarHidden = true;
        this.isLoading = false;
        this.errorText = "";
        this.concertName = "";
        this.artistName = "";
        this.venueHeading = "Venue";
        this.venueDetail = "";
        this.dateTimeHeading = "Date & Time";
        this.dateTimeDetail = "";
        this.inStock = false;
        this.ticketPrice = "";
        this.ticketPriceHeading = "Ticket Price";
        this.ticketBuyButton = "Buy ticket";
        this.closeLabel = "Close";
        this.stockLabel = "Stock out";
        this.ticketQuantityHeading = "Number of tickets";
        this.minusButton = "res://minus_ticket";
        this.plusButton = "res://plus_ticket";
        this.ticketQuantity = 1;
        console.log("CONCERT DETAIL::::", this.userService.concertDetail);
        this.concertDetail = this.userService.concertDetail;
        this.concertId = this.concertDetail.id;
        this.concertName = this.concertDetail.title;
        this.artistName = this.concertDetail.artist;
        this.dateTimeDetail = this.concertDetail.date;
        this.ticketPrice = this.concertDetail.price;
        this.venueDetail = this.concertDetail.address;
        this.inStock = this.concertDetail.inStock;
        this.remainingTickets = this.concertDetail.remainingTickets;
    }

    // ngOnInit() {

    // }

    onBuyTicket() {
        // alert("Buy ticket clicked");
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                name: this.concertName,
                id: this.concertId,
                price: (this.ticketQuantity * parseInt(this.ticketPrice)),
                ticketNo: this.ticketQuantity,
                for: 'ticket'
            }
        };

        this.routerExtensions.navigate(['/orderPayment'], extendedNavigationExtras);
    }

    onClose() {
        this.userService.showHideConcert(false);
    }

    onMinusTicket() {
        if (this.ticketQuantity - 1 >= 1) {
            this.ticketQuantity = this.ticketQuantity - 1;
        } else {
            alert("At least 1 ticket can be bought");
        }
    }

    onPlusTicket() {
        if (this.ticketQuantity + 1 <= parseInt(this.remainingTickets)) {
            this.ticketQuantity = this.ticketQuantity + 1;
        } else {
            alert("Only " + this.remainingTickets + " tickets are left.");
        }
    }

}