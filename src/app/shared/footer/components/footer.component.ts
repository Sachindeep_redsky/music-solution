import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { UserService } from '~/app/services/user.service';

@Component({
    selector: "ns-footer",
    moduleId: module.id,
    templateUrl: "./footer.component.html",
    styleUrls: ["./footer.component.css"]
})

export class FooterComponent implements OnInit {

    isExploreHighlighted: boolean;
    isTrendingHighlighted: boolean;
    isProfileHighlighted: boolean;
    isSearchHighlighted: boolean;
    isLabraryHighlighted: boolean;
    RouterExtensions: any;

    constructor(private page: Page, private userService: UserService, private routerExtensions: RouterExtensions) {

        this.page.actionBarHidden = true;
        this.isExploreHighlighted = false;
        this.isTrendingHighlighted = false;
        this.isProfileHighlighted = false;
        this.isSearchHighlighted = false;
        this.isLabraryHighlighted = false;

        this.userService.activescreen.subscribe((screen: string) => {
            if (screen == "home") {
                this.isExploreHighlighted = true;
                this.isTrendingHighlighted = false;
                this.isProfileHighlighted = false;
                this.isSearchHighlighted = false;
                this.isLabraryHighlighted = false;
            }
            if (screen == "trending") {
                this.isExploreHighlighted = false;
                this.isTrendingHighlighted = true;
                this.isProfileHighlighted = false;
                this.isSearchHighlighted = false;
                this.isLabraryHighlighted = false;
            }
            if (screen == "account" || screen == "subscription" || screen == "editProfile") {
                this.isExploreHighlighted = false;
                this.isTrendingHighlighted = false;
                this.isProfileHighlighted = true;
                this.isSearchHighlighted = false;
                this.isLabraryHighlighted = false;
            }
            if (screen == "library" || screen == "likedsong") {
                this.isExploreHighlighted = false;
                this.isTrendingHighlighted = false;
                this.isProfileHighlighted = false;
                this.isSearchHighlighted = false;
                this.isLabraryHighlighted = true;
            }

            if (screen == "search") {
                this.isExploreHighlighted = false;
                this.isTrendingHighlighted = false;
                this.isProfileHighlighted = false;
                this.isSearchHighlighted = true;
                this.isLabraryHighlighted = false;
            }
        });
    }

    ngOnInit(): void {

    }

    onExploreClick() {
        this.routerExtensions.navigate(['/home']);
    }

    onConcertsClick() {
        this.routerExtensions.navigate(['/concerts']);
    }

    onTrendingClick() {
        this.routerExtensions.navigate(['/trending']);
    }

    onProfileClick() {
        this.routerExtensions.navigate(['/account']);
    }

    onSearchClick() {
        this.routerExtensions.navigate(['/search']);
    }

    onLibraryClick() {
        this.routerExtensions.navigate(['/library']);
    }

}
