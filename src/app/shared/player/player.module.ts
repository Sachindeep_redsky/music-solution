import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { PlayerComponent } from "./components/player.component";


@NgModule({
    imports: [NativeScriptCommonModule],
    declarations: [PlayerComponent],
    schemas: [NO_ERRORS_SCHEMA],
    exports: [PlayerComponent]
})

export class PlayerModule { }