import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { Component, ChangeDetectorRef } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { SongService } from '~/app/services/song.service';
import { Values } from '~/app/values/values';
import { ImageSource } from '@nativescript/core/image-source/image-source';
import { UserService } from '~/app/services/user.service';

@Component({
    selector: "ns-player",
    moduleId: module.id,
    templateUrl: "./player.component.html",
    styleUrls: ["./player.component.css"]
})

export class PlayerComponent {

    musicImage: string;
    musicName: string;
    musicArtist: string;
    isPlay: boolean;
    playIcon: string;
    pauseIcon: string;
    song: any;
    isBuffering: boolean;
    songIndex: number;
    playPause: ImageSource;

    isSongLoading: boolean;

    constructor(private routerExtensions: RouterExtensions, private userService: UserService, private page: Page, private songService: SongService, private changeDetector: ChangeDetectorRef) {

        this.page.actionBarHidden = true;
        this.isSongLoading = this.songService.isSongLoading;
        this.isBuffering = this.songService.isBuffering;
        var songIndex = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);

        if (this.songService.player.isAudioPlaying()) {
            ImageSource.fromResource('pause_icon').then((image) => {
                this.playPause = image;
            })
        } else {
            ImageSource.fromResource('play_icon').then((image) => {
                this.playPause = image;
            })
        }

        this.page.on('navigatedTo', (data) => {
            if (data.isBackNavigation) {
            } else {
                if (this.userService && this.userService.isLoggedIn) {
                    if (songIndex != undefined && songIndex != null) {
                        if (!this.songService.songList) {

                            var dataString = Values.readString(Values.CURRENT_SONG_LIST, '');

                            if (dataString && dataString != '') {
                                this.songService.songList = JSON.parse(dataString);
                                console.log('Inside mini::')
                                this.songService.setSongIndex(songIndex, false);
                            } else {
                                this.userService.showHidePlayer(false);
                            }

                        } else {

                            console.log('Inside mini else')
                            this.songService.setSongIndex(songIndex, false);

                            if (this.songService.player.isAudioPlaying()) {
                                ImageSource.fromResource('pause_icon').then((image) => {
                                    this.playPause = image;
                                })
                            } else {
                                ImageSource.fromResource('play_icon').then((image) => {
                                    this.playPause = image;
                                })
                            }
                        }
                    } else {
                        //no player
                    }
                }
            }
        })


        this.songService.playerStateChanges.subscribe((state) => {

            if (state) {
                ImageSource.fromResource('pause_icon').then((image) => {
                    this.playPause = image;
                })

                this.changeDetector.detectChanges();
            } else {
                ImageSource.fromResource('play_icon').then((image) => {
                    this.playPause = image;
                })

                this.changeDetector.detectChanges();
            }
        })

        if (this.songService && this.songService.songParams && this.songService.songList) {
            this.musicName = this.songService.songParams.name;
            this.musicArtist = this.songService.songParams.artist;
            this.musicImage = this.songService.songParams.thumb;
        }

        this.songService.currentSongChanges.subscribe((index => {

            if (index) {
                this.musicName = this.songService.songParams.name;
                this.musicArtist = this.songService.songParams.artist;
                this.musicImage = this.songService.songParams.thumb;
                if (this.songService.player.isAudioPlaying()) {
                    ImageSource.fromResource('pause_icon').then((image) => {
                        this.playPause = image;
                        this.changeDetector.detectChanges();
                    })
                } else {
                    ImageSource.fromResource('play_icon').then((image) => {
                        this.playPause = image;
                        this.changeDetector.detectChanges();
                    })
                }
            }
        }))

        this.songService.songParamChanges.subscribe((params => {

            this.musicName = this.songService.songParams.name;
            this.musicArtist = this.songService.songParams.artist;
            this.musicImage = this.songService.songParams.thumb;
            if (this.songService.player.isAudioPlaying()) {
                ImageSource.fromResource('pause_icon').then((image) => {
                    this.playPause = image;
                    this.changeDetector.detectChanges();
                })
            } else {
                ImageSource.fromResource('play_icon').then((image) => {
                    this.playPause = image;
                    this.changeDetector.detectChanges();
                })
            }
        }));
        this.songService.bufferingEvent.subscribe((params: boolean) => {
            this.isBuffering = params;
        })

        this.songService.songLoadingChanges.subscribe(state => {
            this.isSongLoading = state;
            this.changeDetector.detectChanges();
        })
    }

    onPlayPause() {
        this.songService.toggle();
    }

    onPlayer() {
        this.routerExtensions.navigate(['/player']);
    }

}
