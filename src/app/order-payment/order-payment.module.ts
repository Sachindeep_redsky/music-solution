import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { OrderPaymentComponent } from './components/order-payment.component';
import { OrderPaymentRoutingModule } from './order-payment-routing.module';

@NgModule({
  imports: [
    OrderPaymentRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule
  ],
  declarations: [OrderPaymentComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class OrderPaymentModule { }
