import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { HttpHeaders } from '@angular/common/http';
import { UserService } from '~/app/services/user.service';
import { Values } from '~/app/values/values';
import { Page } from 'tns-core-modules/ui/page/page';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';

@Component({
  selector: 'ns-order-payment',
  moduleId: module.id,
  templateUrl: './order-payment.component.html',
  styleUrls: ['./order-payment.component.css']
})
export class OrderPaymentComponent implements OnInit {

  pageHeading: string;
  profileName: string;
  planName: string;
  planId: string;
  headers: HttpHeaders;
  user: any;
  for: string;
  ticketNo: number
  stripeHeading: string;
  grandTotalHeading: string;
  grandTotal: number;
  makePaymentHeading: string;
  paymentCardHeading: string;
  addCardHeading: string;
  cardIcon: string;
  nextIcon: string;
  paymentWalletsHeading: string;
  appleCardIcon: string;
  appleCardHeading: string;
  paypalIcon: string;
  makePaymentButton: string;

  constructor(private route: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private page: Page) {
    this.page.actionBarHidden = true;
    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      console.log(Values.readString(Values.X_ACCESS_TOKEN, ''));
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });
      this.user = JSON.parse(Values.readString(Values.USER, ''));
    }
  }

  ngOnInit() {

    this.userService.activeScreen("order-payment");
    this.profileName = "";
    this.pageHeading = "Order Payment";
    this.stripeHeading = "Stripe";
    this.planName = "";
    this.planId = "";
    this.grandTotalHeading = "Grand Total";
    this.grandTotal = 0;
    this.ticketNo = 0;
    this.for = ""
    this.makePaymentHeading = "Make Payment";
    this.paymentCardHeading = "Payment Cards";
    this.addCardHeading = "Add a new card";
    this.cardIcon = "res://card";
    this.nextIcon = "res://next";
    this.paymentWalletsHeading = "Payment wallets";
    this.appleCardIcon = "res://card";
    this.appleCardHeading = "Apple Pay";
    this.paypalIcon = "res://paypal";
    this.makePaymentButton = "Make Payment";

    if (this.user.profile.name) {
      this.profileName = this.user.profile.name.toUpperCase();
    }

    this.route.queryParams.subscribe(params => {
      if (params["name"] != undefined) {
        this.planName = params["name"];
      }
      if (params["id"] != undefined) {
        this.planId = params["id"];
      }
      if (params["price"] != undefined) {
        this.grandTotal = params["price"];
      }
      if (params["for"] != undefined) {
        this.for = params["for"];
      }
      if (params["ticketNo"] != undefined) {
        this.ticketNo = params["ticketNo"];
      }
    });
  }

  onMakePayment() {

    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        // "name": this.planName,
        // "price": this.grandTotal,
        "id": this.planId,
        "type": this.for,
        "ticketNo": this.ticketNo
      },
    };

    this.routerExtensions.navigate(['/payment'], extendedNavigationExtras);

    if (this.for == "song") {
    }
  }

}
