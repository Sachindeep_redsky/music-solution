import { Component, OnInit, ViewChild } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Values } from "~/app/values/values";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Page } from "tns-core-modules/ui/page/page";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { SongService } from "~/app/services/song.service";
import { ModalComponent } from "~/app/modal/modal.component";

@Component({
    selector: "trending",
    moduleId: module.id,
    templateUrl: "./trending.component.html",
    styleUrls: ['./trending.component.css']
})

export class TrendingComponent implements OnInit {
    @ViewChild('songBuyDialog', { static: false }) buyDialog: ModalComponent;

    pageHeading: string;
    genresMoods;
    selectedSong: any;
    topTrendingHeading: string;
    trendingSongs;
    playIcon: string;
    menuButton: string;
    headerPosition: number;
    categoryHeight: number;
    listHeight: number;
    radListHeight: number;
    currentScroll: number;
    user: any;
    headers: HttpHeaders;
    dialogText: string;
    searchText: string;
    isInternetConnection: boolean;
    isRendering: boolean;
    internetMessage: string;
    wifiOffIcon: string;

    constructor(private songService: SongService, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient, private page: Page) {

        this.headerPosition = 0;
        this.currentScroll = 0;
        this.radListHeight = 200

        this.isRendering = true;
        this.isInternetConnection = this.userService.isConnected;

        if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

            this.headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
            });

            this.user = JSON.parse(Values.readString(Values.USER, ''));


        } else {
            this.user = {
                _id: ""
            }
        }
        this.userService.networkChanges.subscribe((state) => {
            if (state == true) {
                this.isInternetConnection = true;
                this.getTrending();
            }
            if (state == false) {
                this.isInternetConnection = false;
            }
        });
        if (this.isInternetConnection) {

            this.getTrending();
        }
    }

    ngOnInit(): void {

        let list;
        let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
        let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');

        this.page.actionBarHidden = true;
        this.pageHeading = "Trending";
        this.wifiOffIcon = "res://wifi_off";

        this.internetMessage = "Having trouble connecting";

        this.userService.activeScreen("trending");
        this.userService.showHideFooter(true);

        if (stringifiedList) {
            list = JSON.parse(stringifiedList);
        }

        if (index != undefined && index != null) {
            if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
                this.userService.showHidePlayer(true);
            } else {
                this.userService.showHidePlayer(false)
            }
        } else {
            this.userService.showHidePlayer(false)
        }

        this.genresMoods = []
        this.topTrendingHeading = "Top Trending - 2020";
        this.playIcon = "res://play_icon_white";
        this.menuButton = "res://menu_grey";
        this.trendingSongs = [];
        this.dialogText = "Purchase this Song";
        this.searchText = "";

        this.page.on('navigatedTo', (data) => {
            if (data.isBackNavigation) {
                let list;
                let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
                let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');
                this.userService.showHideFooter(true);

                if (stringifiedList) {
                    list = JSON.parse(stringifiedList);
                }
                if (index != undefined && index != null) {
                    if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
                        this.userService.showHidePlayer(true);
                    } else {
                        this.userService.showHidePlayer(false)
                    }
                } else {
                    this.userService.showHidePlayer(false)
                }
            }
        });
    }

    onTryAgain() {
        this.userService.checkNetwork();
        this.isInternetConnection = this.userService.isConnected;
    }

    onSearchTextChange(args: any) {
        this.searchText = args.object.text;
    }


    onMusicSearch() {
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "searchText": this.searchText
            },
        };
        this.routerExtensions.navigate(['/search'], extendedNavigationExtras);
    }

    getTrending() {
        this.isRendering = false;
        this.http.get(Values.BASE_URL + `songs?userId=${this.user._id}&status=active&isTrending=true&pageNo=1&items=20`, {
            headers: this.headers
        })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        console.log('rec res', res.data)
                        if (res.data.songs.length > 0) {
                            for (var i = 0; i < res.data.songs.length; i++) {
                                this.trendingSongs.push({
                                    id: res.data.songs[i].id,
                                    name: res.data.songs[i].name,
                                    artist: res.data.songs[i].artistName,
                                    image: res.data.songs[i].image.resize_url,
                                    views: res.data.songs[i].views,
                                    isSongPurchase: res.data.songs[i].isSongPurchase,
                                    song: { url: res.data.songs[i].song.url },
                                    isLoading: false
                                })
                            }
                        } else {
                        }
                        setTimeout(() => {
                            this.isRendering = true;
                        }, 2)
                    }
                }
            }, error => {
                console.log(error.error.error);
                setTimeout(() => {
                    this.isRendering = true;
                }, 2)
                if (error.error.error == 'unauthorized_user') {
                    this.userService.showErrorDialog(true, 'Session timeout. Login again');
                    this.userService.loginUpdate(false);
                    this.routerExtensions.navigate(['/login']);
                }
            });
    }

    onTrendingSongs(item: any, i: number) {
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": item.id,
                "list": JSON.stringify(this.trendingSongs),
                "index": i
            }
        };
        this.songService.setCurrentPlaylist(this.trendingSongs, i);
        this.routerExtensions.navigate(['/player'], extendedNavigationExtras);
    }
    async onDownload(item: any, i: number) {
        item.isLoading = true
        await this.songService.downloadSong(item);
        item.isLoading = false

    }


}



