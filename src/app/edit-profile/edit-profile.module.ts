import { PlayerModule } from './../shared/player/player.module';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { EditProfileComponent } from "./components/edit-profile.component";
import { FooterModule } from "../shared/footer/footer.module";
import { EditProfileRoutingModule } from './edit-profile-routing.module';
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NgModalModule } from "../modal/ng-modal";

@NgModule({
  imports: [
    EditProfileRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    PlayerModule,
    NgModalModule
  ],
  declarations: [EditProfileComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class EditProfileModule { }
