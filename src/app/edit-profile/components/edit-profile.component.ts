import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '~/app/services/user.service';
import { Values } from '~/app/values/values';
import { ModalComponent } from '~/app/modal/modal.component';
import { ImageCropper } from 'nativescript-imagecropper';
import { ImageSource } from "tns-core-modules/image-source/image-source";
import { Page } from 'tns-core-modules/ui/page/page';
import { Image } from 'tns-core-modules/ui/image/image';
import { TextField } from 'tns-core-modules/ui/text-field/text-field';
import { ImageAsset } from 'tns-core-modules/image-asset/image-asset';

import * as camera from "nativescript-camera";
import * as bghttp from 'nativescript-background-http';
import * as permissions from "nativescript-permissions";
import * as imagepicker from "nativescript-imagepicker";
import * as fileSystemModule from "tns-core-modules/file-system";

@Component({
  selector: 'ns-account',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})

export class EditProfileComponent implements OnInit {

  @ViewChild('photoUploadDialog', { static: false }) photoDialog: ModalComponent;
  @ViewChild('errorDialog', { static: false }) errorDialog: ModalComponent;

  pageHeading: string;
  profilePic: string;
  profileName: string;
  subscriptionPlan: string;
  profileHeading: string;
  submitText: string;
  nameHint: string;
  headers: HttpHeaders;
  userId: string;
  isDialog: boolean;
  imageCropper: ImageCropper;
  errorText: string;
  imageUrls: any;
  isImageUpdate: boolean;

  isRendering: boolean;


  constructor(private route: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient, private page: Page) {
    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });

      this.nameHint = "";
      this.isRendering = false;



      var user = JSON.parse(Values.readString(Values.USER, ''));
      setTimeout(() => {
        this.userId = user._id
        if (user && user.profile && user.profile.name) {
          this.profileName = user.profile.name;
          this.nameHint = "Enter name";
        } else {
          this.nameHint = "Set name";
          this.profileName = '';
        }

        if (user && user.profile && user.profile.image.resize_url) {
          this.profilePic = user.profile.image.resize_url;
        } else {
          this.profilePic = "res://user"
        }
        this.isRendering = true;
      }, 50)



      // setTimeout(() => {
      //   if (user && user.profile && user.profile.name) {
      //     this.profileName = user.profile.name;
      //   } else {
      //     this.profileName = user.userName.charAt(0).toUpperCase() + user.profile.name.slice(1);
      //   }
      //   if (user && user.profile && user.profile.image && user.profile.image.resize_url) {
      //     this.profilePic = user.profile.image.resize_url;
      //   }
      //   // this.profilePic = user.profile.image.resize_url;
      //   this.isRendering = true;
      // }, 50)

      // this.getUser(this.userId)

    }
  }

  ngOnInit() {

    this.page.actionBarHidden = true;
    this.imageCropper = new ImageCropper();
    this.isDialog = false;
    this.userService.activeScreen("edit-profile");
    this.userService.showHideFooter(true);
    this.userService.showHidePlayer(false);
    this.pageHeading = "Edit Profile";
    // this.profilePic = "";
    // this.profileName = "";
    this.submitText = "Save";
    // this.subscriptionPlan = "$99 - Annual Plan";
    this.profileHeading = "Profile";
    this.imageUrls = [];
    this.errorText = "";
    this.isImageUpdate = false;
  }

  // getUser(id: string) {

  //   this.http.get(`${Values.BASE_URL}users/${id}`, {
  //     headers: this.headers
  //   }).subscribe((res: any) => {
  //     if (res != null && res != undefined) {
  //       if (res.isSuccess == true) {
  //         this.profileName = res.data.profile.name.charAt(0).toUpperCase() + res.data.profile.name.slice(1);
  //         this.profilePic = res.data.profile.image.resize_url;
  //       }
  //     }
  //   }, error => {
  //     console.log(error.error.error);
  //     console.log(error);
  //   });
  // }

  onGallery() {
    var that = this;
    let context = imagepicker.create({
      mode: "single"
    });

    permissions.requestPermission([android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE])
      .then(() => {
        context
          .authorize()
          .then(() => {
            return context.present();
          })
          .then(selection => {
            selection.forEach(function (selected: ImageAsset) {

              let source = new ImageSource();
              source.fromAsset(selected).then((source) => {

                that.imageCropper.show(source, { width: 250, height: 250, lockSquare: true }).then((args: any) => {
                  if (args.image !== null) {
                    var doc = fileSystemModule.knownFolders.documents();
                    var folderPath = fileSystemModule.path.join(doc.path, "music");
                    var file = fileSystemModule.File.fromPath(fileSystemModule.path.join(folderPath, "user.jpeg"));
                    args.image.saveToFile(file.path, 'jpeg');

                    var img = <Image>that.page.getViewById('img');
                    img.src = file.path;

                    that.profilePic = file.path;

                    setTimeout(() => {
                      that.isImageUpdate = true;
                      console.log('productImage', that.profilePic)
                    }, 100)
                  }
                })
                  .catch(function (e) {
                    console.log(e);
                  });
              }).catch((err) => {
                console.log("Error -> " + err.message);
              });
            });
          });
      });
  }

  onCamera() {

    if (camera.isAvailable()) {

      const imageCropper = new ImageCropper();
      var that = this;

      permissions.requestPermission([android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE])
        .then(() => {
          camera.takePicture({ width: 512, height: 512, keepAspectRatio: true })
            .then((selected: ImageAsset) => {
              let source = new ImageSource();
              source.fromAsset(selected).then((source) => {
                imageCropper.show(source, { width: 250, height: 250, lockSquare: true }).then((args) => {
                  if (args.image !== null) {

                    var doc = fileSystemModule.knownFolders.documents();
                    var folderPath = fileSystemModule.path.join(doc.path, "GoToPills");
                    var file = fileSystemModule.File.fromPath(fileSystemModule.path.join(folderPath, "user.jpeg"))
                    args.image.saveToFile(file.path, 'jpeg');
                    var img = <Image>that.page.getViewById('img');
                    img.src = file.path;
                    that.profilePic = file.path;
                    setTimeout(() => {
                      this.isImageUpdate == true;
                      console.log('productImage', that.profilePic)
                    }, 100)
                  }
                }).catch(function (e) {
                  console.log(e);
                });
              });
            }).catch((err) => {
              console.log("Error -> " + err.message);
            });
        }).catch(function () {
          this.userService.showErrorDialog(true, 'User denied permissions');
        });
    }
  }

  uploadImage(): Promise<any> {

    return new Promise((resolve, reject) => {

      var file = this.profilePic;
      var url = 'http://3.138.34.244:2000/api';
      var name = file.substr(file.lastIndexOf("/") + 1);
      var extension = name.substr(name.lastIndexOf(".") + 1);
      var session = bghttp.session("image-upload");
      var request = {
        url: url,
        method: "POST",
        headers: {
          "Content-Type": "application/octet-stream"
        },
        description: "Uploading " + name
      };

      const params = [
        { name: "file", filename: this.profilePic, mimeType: 'image/' + extension }
      ]

      var task = session.multipartUpload(params, request);
      task.on("progress", (e) => {
        console.log('progress', e);
      });
      task.on("responded", (e: any) => {
        console.trace("RESPONSE: " + e.data);

        var res = JSON.parse(e.data)

        if (res.isSuccess == true) {

          resolve({
            url: res.data.image.url,
            thumbnail: res.data.image.thumbnail,
            resize_url: res.data.image.resize_url,
            resize_thumbnail: res.data.image.resize_thumbnail
          });

          this.imageUrls.push({
            url: res.data.image.url,
            thumbnail: res.data.image.thumbnail,
            resize_url: res.data.image.resize_url,
            resize_thumbnail: res.data.image.resize_thumbnail
          })
        }
        console.log('urlss', this.imageUrls[0].url)

      });
      task.on("error", (e) => {
        console.log('error', e)
        reject(e)
      });
    })

  }

  showChooseDialog() {
    this.isDialog = true;
    setTimeout(() => {
      this.photoDialog.show()
    }, 10)
  }

  onSave() {
    if (!this.profilePic) {
      this.errorText = "Please select product image.";
      this.errorDialog.show();
    }
    else if (this.profileName == "") {
      this.errorText = "Please enter name.";
      this.errorDialog.show();
    }
    else {
      if (this.isImageUpdate) {
        this.uploadImage().then(() => {
          this.updateUser(true);
        }).catch(error => {
          console.log('Could not upload image:::', error)
        });
      } else {
        this.updateUser(false);
      }
    }
  }

  updateUser(updateImage: boolean) {

    let user;

    if (updateImage) {
      user = {
        name: this.profileName,
        profile: {
          image: this.imageUrls[0]
        }
      }
    } else {
      user = {
        name: this.profileName
      }
    }

    this.http
      .put(Values.BASE_URL + "users/" + this.userId, user, {
        headers: this.headers
      })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          console.log('res', res)
          Values.writeString(Values.USER, JSON.stringify(res.data));
          if (res.isSuccess == true) {
            this.routerExtensions.back();
          }
        }
      }, error => {
        console.log("ERROR::::", error.error.error);
      });
  }

  onNameChange(args: any) {
    var textField = <TextField>args.object;
    this.profileName = textField.text;
  }

  onErrorOKTap() {

  }

}
