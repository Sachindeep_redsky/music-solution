import { NgModule } from "@angular/core";
import { Routes, PreloadAllModules } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
   { path: "home", loadChildren: "~/app/home/home.module#HomeModule" },
   { path: "trending", loadChildren: "~/app/trending/trending.module#TrendingModule" },
   { path: "concerts", loadChildren: "~/app/concerts/concerts.module#ConcertsModule" },
   { path: "purchasedTickets", loadChildren: "~/app/purchased-tickets/purchased-tickets.module#PurchasedTicketsModule" },
   { path: "splash", loadChildren: "~/app/splash/splash.module#SplashModule" },
   { path: "login", loadChildren: "~/app/login/login.module#LoginModule" },
   { path: "register", loadChildren: "~/app/register/register.module#RegisterModule" },
   { path: "forgotPassword", loadChildren: "~/app/forgot-password/forgot-password.module#ForgotPasswordModule" },
   { path: "search", loadChildren: "~/app/search/search.module#SearchModule" },
   { path: "library", loadChildren: "~/app/library/library.module#LibraryModule" },
   { path: "account", loadChildren: "~/app/account/account.module#AccountModule" },
   { path: "payment", loadChildren: "~/app/payment/payment.module#PaymentModule" },
   { path: "player", loadChildren: "~/app/player/player.module#PlayerModule" },
   { path: "subscription", loadChildren: "~/app/subscription/subscription.module#SubscriptionModule" },
   { path: "confirmation", loadChildren: "~/app/confirmation/confirmation.module#ConfirmationModule" },
   { path: "orderPayment", loadChildren: "~/app/order-payment/order-payment.module#OrderPaymentModule" },
   { path: "likedsongs", loadChildren: "~/app/likedsongs/likedsongs.module#LikedsongsModule" },
   { path: "verify-otp", loadChildren: "~/app/verify-otp/verify-otp.module#VerifyOtpModule" },
   { path: "edit-profile", loadChildren: "~/app/edit-profile/edit-profile.module#EditProfileModule" },
   { path: "albums", loadChildren: "~/app/albums/albums.module#AlbumsModule" },
   { path: "change-password", loadChildren: "~/app/change-password/change-password.module#ChangePasswordModule" },
   { path: "settings", loadChildren: "~/app/settings/settings.module#SettingsModule" },
   { path: "comments", loadChildren: "~/app/comments/comments.module#CommentsModule" },
   { path: "current-playlist", loadChildren: "~/app/current-playlist/current-playlist.module#CurrentPlaylistModule" },
   { path: "about-app", loadChildren: "~/app/about-app/about-app.module#AboutAppModule" },
   { path: "add-songs", loadChildren: "~/app/add-songs/add-songs.module#AddSongsModule" },
   { path: "offline-songs", loadChildren: "~/app/offline-songs/offline-songs.module#OfflineSongsModule" },
   { path: "download-songs", loadChildren: "~/app/download-songs/download-songs.module#DownloadSongsModule" },
];

@NgModule({
   imports: [NativeScriptRouterModule.forRoot(
      <any>routes, { preloadingStrategy: PreloadAllModules })],
   exports: [NativeScriptRouterModule]
})

export class AppRoutingModule { }