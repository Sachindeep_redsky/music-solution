import { fromResource } from 'tns-core-modules/image-source/image-source';
import { Component, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '~/app/services/user.service';
import { Page } from 'tns-core-modules/ui/page/page';
import { TouchGestureEventData } from 'tns-core-modules/ui/gestures/gestures';
import { Values } from '~/app/values/values';
import { SongService } from "~/app/services/song.service";
import { ImageSource } from 'tns-core-modules/image-source/image-source';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';


@Component({
  selector: 'ns-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})

export class PlayerComponent {

  user: any;
  downArrow: string;
  menuIcon: string;
  headerLabel: string;
  albumName: string;
  musicImage: string | ImageSource
  songName: string;
  artistName: string;
  backwardThirty: string;
  forwardThirty: string;
  backwardIcon: string;
  forwardIcon: string;
  playPause: string | ImageSource;
  likeIcon: string;
  unlikeIcon: string;
  shuffleIconRed: string;
  shuffleIconBlack: string;
  repeatIcon: string;
  playlistIcon: string;
  commentIcon: string;
  isLike: boolean;
  isShuffle: boolean;
  isRepeat: boolean;
  isPlaylist: boolean;
  progressValue: number
  remainingTime: string;
  songId: string;
  passedTime: string;
  headers: HttpHeaders
  totalProgressWidth: number;
  likeId: string;
  isOfflineSong: boolean;
  songDuration: number;
  isBuffering: boolean;
  isSongLoading: boolean;
  constructor(private route: ActivatedRoute, private routerExtensions: RouterExtensions, private changeDetector: ChangeDetectorRef, private userService: UserService,
    private http: HttpClient, private page: Page, private songService: SongService) {
    this.page.actionBarHidden = true;
    this.songDuration = 0;

    this.downArrow = "res://down_arrow";
    this.menuIcon = "res://menu_horizontal";
    this.headerLabel = "NOW PLAYING FROM";
    this.albumName = "Unknown";
    this.musicImage = "res://music_black";
    this.songName = "Unknown";
    this.artistName = "Unknown";
    this.backwardThirty = "res://backward_thirty";
    this.forwardThirty = "res://forward_thirty";
    this.backwardIcon = "res://backward_icon";
    this.commentIcon = "res://message"
    this.forwardIcon = "res://forward_icon";
    this.likeIcon = "res://heart_icon_red";
    this.unlikeIcon = "res://heart_icon";
    this.shuffleIconBlack = "res://shuffle_icon";
    this.shuffleIconRed = "res://shuffle_icon_red";
    this.repeatIcon = "res://repeat_icon";
    this.playlistIcon = "res://playlist_icon";
    this.isLike = false;
    this.isShuffle = Values.readBoolean(Values.IS_SHUFFLE, false);
    this.isRepeat = false;
    this.isPlaylist = false;
    this.progressValue = 0;
    this.totalProgressWidth = 0;
    this.songId = "";
    this.likeId = "";
    if (this.songService.getPlayer().isAudioPlaying()) {
      this.playPause = fromResource("pause_icon_white");
    } else {
      this.playPause = fromResource("play_icon_white");
    }
    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });

      if (Values.readString(Values.USER, '')) {
        this.user = JSON.parse(Values.readString(Values.USER, ''));
        console.log('User:::', this.user)
      } else {
        console.log("NO USER")
      }

    }

    this.songService.playerStateChanges.subscribe((state) => {
      if (state) {
        ImageSource.fromResource('pause_icon_white').then(image => {
          this.playPause = image;
          this.changeDetector.detectChanges();
        })
      } else {
        ImageSource.fromResource('play_icon_white').then(image => {
          this.playPause = image;
          this.changeDetector.detectChanges();
        })
      }
    })

    if (this.songService && this.songService.songParams && this.songService.songList) {
      this.updateUiParams();
    }

    this.songService.songProgressChanges.subscribe((params) => {
      this.songDuration = params['duration']
    })

    this.songService.currentSongChanges.subscribe((songParams => {
      this.updateUiParams();
    }));

    this.songService.songParamChanges.subscribe((params) => {
      this.updateUiParams();
    })

    this.songService.songProgressChanges.subscribe((params) => {
      this.progressValue = params.progress;
      this.passedTime = params.passedTime;
      this.remainingTime = params.remainingTime;
      this.changeDetector.detectChanges();
    });
    this.songService.bufferingEvent.subscribe((params: boolean) => {
      this.isBuffering = params;
    })
    this.songService.songLoadingChanges.subscribe(state => {
      this.isSongLoading = state;
    })

    this.page.on('navigatedTo', (data) => {
      if (data.isBackNavigation) {
        this.userService.showHideFooter(false);
        this.userService.activeScreen("player");
        this.userService.showHidePlayer(false);
      }
    })

    this.userService.activeScreen("player");
    this.userService.showHideFooter(false);
    this.userService.showHidePlayer(false);

    this.route.queryParams.subscribe(params => {

      if (params["id"] && params["id"] != undefined) {
        let index

        this.songId = params["id"];

        if (params["list"]) {
          this.songService.setSongList(JSON.parse(params["list"]))
        }

        if (params["index"]) {
          if (typeof params["index"] == 'string') {
            index = parseInt(params["index"]);
          }

          this.songService.setSongIndex(index, true);
          this.isOfflineSong = false;
        }
      } else if (params["offline"]) {
        let index
        let list = JSON.parse(params["list"]);
        this.albumName = "Offline";
        this.songService.setSongList(list);
        this.isOfflineSong = true;

        if (params["index"]) {
          if (typeof params["index"] == 'string') {
            index = parseInt(params["index"]);
            this.songService.setSongIndex(index, true);
          }
        }
      } else {
        if (this.songService && this.songService.songParams) {
          console.log('player with nothing value')
          this.updateUiParams();
          this.isBuffering = this.songService.isBuffering;
          this.isSongLoading = this.songService.isSongLoading;
        }
      }
    });



  }

  updateUiParams() {
    this.songName = this.songService.songParams.name;
    this.artistName = this.songService.songParams.artist;
    this.musicImage = this.songService.songParams.thumb;
    this.albumName = this.songService.songParams.album;
    this.isOfflineSong = this.songService.songParams.isOffline;
    this.isLike = this.songService.songParams.isLiked;
    this.likeId = this.songService.songParams.likeId;
    this.songId = this.songService.songParams.id;
  }

  onMainViewLoaded(args) {
    if (this.songService && this.songService.songParams && this.songService.songList) {
      this.updateUiParams();
    }
  }

  onProgressTouch(args: TouchGestureEventData) {

    var seekTime;
    var tapX = args.getX();
    if (tapX >= 0) {
      if (tapX >= this.totalProgressWidth) {
        seekTime = this.songDuration;
        this.progressValue = 100;
      } else {
        seekTime = (tapX / this.totalProgressWidth) * this.songDuration;
        this.progressValue = (tapX / this.totalProgressWidth) * 100;
      }
    } else {
      seekTime = 0;
      this.progressValue = 0;
    }

    var remDuration = this.songDuration - seekTime;
    var currentSec = this.songService.getSeconds(seekTime);          //it will be divided by 1000 already
    var currentMin = this.songService.getMinutes(seekTime);
    var currentHour = this.songService.getHours(seekTime);

    var currentSecStr
    var currentMinStr
    var currentHourStr

    var remSec = this.songService.getSeconds(remDuration);
    var remMin = this.songService.getMinutes(remDuration);
    var remHour = this.songService.getHours(remDuration);

    var remSecStr
    var remMinStr
    var remHourStr


    if (remSec < 10) {
      remSecStr = "0" + remSec;
    }
    else {
      remSecStr = remSec;
    }

    if (remMin < 10) {
      remMinStr = "0" + remMin;
    }
    else {
      remMinStr = remMin;
    }

    if (remHour < 10) {
      remHourStr = "0" + remHour;
    }
    else {
      remHourStr = remHour;
    }

    this.remainingTime = remHourStr + ":" + remMinStr + ":" + remSecStr + "";

    if (currentSec < 10) {
      currentSecStr = "0" + currentSec;
    }
    else {
      currentSecStr = currentSec;
    }

    if (currentMin < 10) {
      currentMinStr = "0" + currentMin;
    }
    else {
      currentMinStr = currentMin;
    }

    if (currentHour < 10) {
      currentHourStr = "0" + currentHour;
    }
    else {
      currentHourStr = currentHour;
    }

    this.passedTime = currentHourStr + ":" + currentMinStr + ":" + currentSecStr + "";
    this.changeDetector.detectChanges();

    setTimeout(() => {
      this.songService.getPlayer().seekTo(seekTime / 1000);
    }, 1);

  }

  onProgressLoad(args: any) {
    setTimeout(() => {
      this.totalProgressWidth = args.object.getActualSize().width
    }, 1)
  }

  songCompleteEvent() {
    console.log('finished playing');
  }

  onForward() {
    this.songService.next();
  }

  onRewind() {
    this.songService.previous();
  }

  onForwardThirty() {
    this.songService.getPlayer().seekTo(this.songService.getPlayer().currentTime / 1000 + 30);
  }

  onRewindThirty() {
    this.songService.getPlayer().seekTo(this.songService.getPlayer().currentTime / 1000 - 30);
  }

  randomInteger(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  onPlayPause() {
    this.songService.toggle();
  }

  onLike() {
    if (this.isLike == false) {

      let playlist = {
        songId: this.songId,
        userId: this.user._id
      }
      this.http.post(Values.BASE_URL + `likes`, playlist, {
        headers: this.headers
      }
      ).subscribe((res: any) => {
        if (res && res.isSuccess) {
          console.log('Response:', res);
          this.isLike = true;
          this.likeId = res.data._id
        }
      }, (error) => {
        console.log('Error:', error);
        this.userService.showErrorDialog(true, error.error.error);
      })
    } else {
      console.log(this.likeId)
      this.http.delete(Values.BASE_URL + `likes/delete/${this.likeId}`, {
        headers: this.headers
      }).subscribe((res: any) => {
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            console.log('delete res', res);
            this.isLike = false;
          }
        }
      }, error => {
        console.log(error);
        if (error.error.error == 'unauthorized_user') {
          this.userService.showErrorDialog(true, 'Session timeout. Login again');
          this.userService.loginUpdate(false);
          this.routerExtensions.navigate(['/login']);
        }
      })
    }
  }

  onShuffle() {
    if (this.isShuffle == false) {
      this.isShuffle = true;
      Values.writeBoolean(Values.IS_SHUFFLE, true);
    } else {
      this.isShuffle = false;
      Values.writeBoolean(Values.IS_SHUFFLE, false);
    }
  }

  onRepeat() {
    if (this.isRepeat == false) {
      if (this.songService) {
        this.songService.isRepeat = true
        this.repeatIcon = "res://repeat_icon_red";
        this.isRepeat = true;
      } else {
        // this.songService.isRepeat=true
        console.log('Could not turn repeat ON');
      }
    } else {
      if (this.songService) {
        this.songService.isRepeat = false
        this.repeatIcon = "res://repeat_icon";
        this.isRepeat = false
      } else {
        console.log('Could not turn repeat OFF');
      }
    }
  }

  onPlaylist() {

    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "from": this.albumName,
      },
    };

    this.routerExtensions.navigate(['/current-playlist'], extendedNavigationExtras)
  }

  onComments() {
    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "id": this.songId,
      },
    };
    this.routerExtensions.navigate(['/comments'], extendedNavigationExtras);
  }

  onDownArrow() {
    this.userService.showHideFooter(true);
    this.userService.showHidePlayer(true);
    this.routerExtensions.back();
  }



}
