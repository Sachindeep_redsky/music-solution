import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { PlayerRoutingModule } from './player-routing.module';
import { PlayerComponent } from './components/player.component';

@NgModule({
    imports: [
        PlayerRoutingModule,
        FooterModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
    ],
    declarations: [PlayerComponent],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class PlayerModule { }
