import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from "./components/search.component";
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { PlayerModule } from './../shared/player/player.module';
import { NgModalModule } from "../modal/ng-modal";

@NgModule({
  imports: [
    SearchRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule,
    PlayerModule,
    NgModalModule
  ],
  declarations: [
    SearchComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class SearchModule { }
