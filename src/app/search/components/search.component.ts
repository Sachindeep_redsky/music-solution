import { Component, OnInit, ViewChild } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { UserService } from '~/app/services/user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Values } from '~/app/values/values';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';
import { Page } from 'tns-core-modules/ui/page/page';
import { SongService } from '~/app/services/song.service';
import { ModalComponent } from '~/app/modal/modal.component';
import { ad } from "tns-core-modules/utils/utils"
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ns-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {

  @ViewChild('songBuyDialog', { static: false }) buyDialog: ModalComponent;

  backbtn: string;
  search: string;
  searchSongResult: any;
  searchAlbumsResult: any;
  isSearch: boolean;
  isNoData: boolean;
  noResult: string;
  isNoSongData: boolean;
  isNoAlbumData: boolean;
  serachNow: string;
  dialogText: string;
  menuButton: string;
  selectedSong: any;
  selectedIndex: number;
  nextIcon: string;
  songsText: string
  isRendering: boolean;
  showAllText: string;
  headers: HttpHeaders;
  user: any;
  timeOut: any;
  isInternetConnection: boolean;
  internetMessage: string;
  wifiOffIcon: string;
  isSongSelected: boolean;
  isAlbumSelected: boolean;
  isArtistSelected: boolean;
  searchResult: any[];
  type: string;
  constructor(private routerExtensions: RouterExtensions, private activatedRoute: ActivatedRoute, private userService: UserService, private http: HttpClient, private page: Page, private songService: SongService) {

    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });

      this.user = JSON.parse(Values.readString(Values.USER, ''));
    } else {
      this.user = {
        _id: ""
      }
    }

    this.userService.networkChanges.subscribe((state) => {
      if (state == true) {
        this.isInternetConnection = true;

      }
      if (state == false) {
        this.isInternetConnection = false;
      }
    });
  }

  ngOnInit() {

    let list;
    let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
    let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');
    this.page.actionBarHidden = true;
    this.userService.showHideFooter(true);
    this.userService.activeScreen("search");

    if (stringifiedList) {
      list = JSON.parse(stringifiedList);
    }

    if (index != undefined && index != null) {
      if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
        this.userService.showHidePlayer(true);
      } else {
        this.userService.showHidePlayer(false)
      }
    } else {
      this.userService.showHidePlayer(false)
    }
    this.backbtn = "res://back_black";
    this.dialogText = "Purchase this Song";
    this.menuButton = "res://menu_grey";
    this.nextIcon = "res://right_arrow";
    this.showAllText = "SHOW ALL";
    this.noResult = "No results found";
    this.serachNow = "Search";
    this.songsText = "Songs";
    this.selectedIndex = 0;
    this.search = "";
    this.searchSongResult = [];
    this.searchAlbumsResult = []
    this.searchResult = [];
    this.isSearch = false;
    this.isNoData = false;
    this.isNoSongData = false;
    this.isNoAlbumData = false;
    this.isRendering = false;
    this.isSongSelected = true;
    this.isAlbumSelected = false;
    this.isArtistSelected = false;
    this.type = ""
    this.isInternetConnection = this.userService.isConnected;
    this.internetMessage = "There is no internet connection available."
    this.wifiOffIcon = "res://wifi_off";
    this.activatedRoute.queryParams.subscribe(params => {
      console.log('searchText', params["searchText"]);
      if (params["searchText"] != undefined) {
        this.search = params["searchText"];
      }
    });

    this.page.on('navigatedTo', (data) => {
      if (data.isBackNavigation) {

        let list;
        let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');
        let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);

        this.userService.showHideFooter(true);

        if (stringifiedList) {
          list = JSON.parse(stringifiedList);
        }

        if (index != undefined && index != null) {
          if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
            this.userService.showHidePlayer(true);
          } else {
            this.userService.showHidePlayer(false)
          }
        } else {
          this.userService.showHidePlayer(false)
        }
      }
    });
  }

  onSearchSongs(item: any, i: number) {
    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "id": item.id,
        "list": JSON.stringify(this.searchSongResult),
        "index": i
      }
    };

    this.songService.setCurrentPlaylist(this.searchSongResult, i);
    this.routerExtensions.navigate(['/player'], extendedNavigationExtras);
  }

  onTextChange(event: any) {

    this.search = event.object.text;
    // this.searchSongResult = [];
    // this.searchAlbumsResult = []
    // this.searchResult = [];
    clearTimeout(this.timeOut)
    this.timeOut = setTimeout(() => {
      this.getSearchData()
    }, 300);

  }
  getSearchData() {
    this.isRendering = true;
    this.http.get(Values.BASE_URL + `songs/search?name=${this.search}&userId=${this.user._id}&type=${this.type}`, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          // console.log('Search res:::', res)
          if (res.isSuccess == true) {
            // this.searchSongResult = [];
            // this.searchAlbumsResult = []
            this.searchResult = [];
            this.isSearch = true;
            this.isNoData = false;
            if (this.isSongSelected) {
              if (res.data && res.data.songs && res.data.songs.length > 0) {
                this.isNoSongData = false;
                console.trace('S:::R:::', res.data.songs)
                for (var i = 0; i < (res.data.songs.length > 5 ? 5 : res.data.songs.length); i++) {
                  this.searchResult.push({
                    id: res.data.songs[i].id,
                    name: res.data.songs[i].name,
                    artist: res.data.songs[i].artistName,
                    image: res.data.songs[i].image.resize_url,
                    song: { url: res.data.songs[i].song.url },
                    price: res.data.songs[i].price,
                    isSongPurchase: res.data.songs[i].isSongPurchase,
                    isLoading: false
                  })
                }
              } else {
                this.isNoSongData = true;
              }
            } else {
              if (res.data && res.data.category && res.data.category.length > 0) {
                console.trace('C:::R:::', res.data.category)
                this.isNoAlbumData = false;
                for (var i = 0; i < res.data.category.length; i++) {
                  this.searchResult.push({
                    id: res.data.category[i]._id,
                    name: res.data.category[i].name,
                    artist: res.data.category[i].artistName,
                    image: res.data.category[i].image.resize_url
                  })
                }
              } else {
                this.isNoAlbumData = true;
              }
            }
            // if (this.isNoAlbumData && this.isNoSongData) {
            //   this.isSearch = false;
            //   this.isNoData = true;
            // } else {
            //   this.isNoData = false;
            // }
            this.isRendering = false;
          }
        }
      }, error => {
        console.log(error);
        this.isRendering = false;
        if (error.error.error == 'unauthorized_user') {
          this.userService.showErrorDialog(true, 'Session timeout. Login again');
          this.userService.loginUpdate(false);
          this.routerExtensions.navigate(['/login']);
        }
      });
  }

  onAlbum(item) {

    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "id": item.id,
        "type": 'categoryId'
      },
    };

    this.routerExtensions.navigate(['/albums'], extendedNavigationExtras);
  }

  onBack() {
    this.routerExtensions.back();
  }

  showAll() {
    ad.dismissSoftInput();
    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "search": this.search,
        "from": "search"
      },
    };

    this.routerExtensions.navigate(['/add-songs'], extendedNavigationExtras);
  }

  onTryAgain() {
    this.userService.checkNetwork();
    this.isInternetConnection = this.userService.isConnected;
  }
  onFilters(filter: string) {
    console.log('filter', filter);
    switch (filter) {
      case 'songs':
        this.isSongSelected = true;
        this.isAlbumSelected = false;
        this.isArtistSelected = false;
        this.type = "";
        break;
      case 'albums':
        this.type = "album"
        this.isSongSelected = false;
        this.isAlbumSelected = true;
        this.isArtistSelected = false;
        break;
      case 'artists':
        this.type = "artist"
        this.isSongSelected = false;
        this.isAlbumSelected = false;
        this.isArtistSelected = true;
        break;

      default:
        break;

    }
    this.getSearchData()
  }
  async onDownload(item: any, i: number) {
    item.isLoading = true
    await this.songService.downloadSong(item);
    item.isLoading = false
  }

  onSearchItem(item) {

  }
  onFooterLoaded($event) {

  }

}
