import { Component, OnInit, ViewChild } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ad } from "tns-core-modules/utils/utils"
import { Values } from "~/app/values/values";
import { ModalComponent } from "~/app/modal/modal.component";
import { UserService } from "~/app/services/user.service";

@Component({
    selector: "app-change-password",
    moduleId: module.id,
    templateUrl: "./change-password.component.html",
    styleUrls: ['./change-password.component.css'],
})

export class ChangePasswordComponent implements OnInit {
    @ViewChild('errorDialog', { static: false }) errorDialog: ModalComponent;

    pageHeading: string;

    currentPasswordHint: string;
    newPasswordHint: string;
    cnfPasswordHint: string;
    newPassword: string;
    currentPassword: string;
    cnfPassword: string;
    signinButton: string;
    backHeading: string;
    appLogo: string;

    errorText: string;
    user: any;
    headers: HttpHeaders


    constructor(private page: Page, private http: HttpClient, private routerExtensions: RouterExtensions,
        private userService: UserService) {

        this.page.actionBarHidden = true;

        if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

            this.headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
            });

            this.user = JSON.parse(Values.readString(Values.USER, ''));
        }
    }

    ngOnInit() {

        this.userService.showHideFooter(false);
        this.userService.activeScreen("change-password");
        this.userService.showHidePlayer(false);
        this.pageHeading = "Change Password";
        this.currentPasswordHint = "Current Password";
        this.newPasswordHint = "New Password";
        this.cnfPasswordHint = "Confirm Password"
        this.currentPassword = "";
        this.newPassword = "";
        this.cnfPassword = "";
        this.signinButton = "Submit";
        this.backHeading = "Back";
        this.appLogo = "res://logo";

    }

    onCurrentTextChange(args: any) {
        var textField = <TextField>args.object;
        this.currentPassword = textField.text;
    }

    onNewTextChange(args: any) {
        var textField = <TextField>args.object;
        this.newPassword = textField.text;
    }

    onCnfTextChange(args: any) {
        var textField = <TextField>args.object;
        this.cnfPassword = textField.text;
    }

    onSigninButton() {

        ad.dismissSoftInput();
        if (this.currentPassword == "") {
            this.errorDialog.show();
            this.errorText = "Please enter username";
            return;
        }
        else if (this.newPassword == "") {
            this.errorDialog.show();
            this.errorText = "Please enter password";
            return;
        }
        else if (this.newPassword.length < 6) {
            this.errorDialog.show();
            this.errorText = "Password should be at least 6 digits";
            return;
        }
        else if (this.newPassword != this.cnfPassword) {
            this.errorDialog.show();
            this.errorText = "Password mismatch";
            return;
        } else {

            var user = {
                newPassword: this.newPassword,
                password: this.currentPassword
            }

            this.http.post(Values.BASE_URL + `users/changePassword`, user, { headers: this.headers }).subscribe((res: any) => {
                console.trace('Response:', res);
                if (res && res.isSuccess) {
                    this.userService.showErrorDialog(true, 'Password Changed Successfully');
                    this.routerExtensions.navigate(['/login'], { clearHistory: true })
                }
            }, (error) => {
                console.log('Error:', error);
                if (error.error.error == 'unauthorized_user') {
                    this.userService.showErrorDialog(true, 'Session timeout. Login again');
                    this.userService.loginUpdate(false);
                } else {
                    this.errorText = "Something went wrong";
                    this.errorDialog.show();
                }
            })
        }
    }

    onErrorOKTap($event) {
        this.errorDialog.hide();
    }

    onBack() {
        this.routerExtensions.back();
    }

}