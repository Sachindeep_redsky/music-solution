import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommentsRoutingModule } from './comments-routing.module';
import { CommentsComponent } from "./components/comments.component";
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
  imports: [
    CommentsRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule
  ],
  declarations: [
    CommentsComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class CommentsModule { }
