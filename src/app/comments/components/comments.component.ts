import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { UserService } from '~/app/services/user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Values } from '~/app/values/values';
import { Page, EventData } from 'tns-core-modules/ui/page/page';
import { ObservableArray } from 'tns-core-modules/data/observable-array/observable-array';

import * as Toast from 'nativescript-toast';

@Component({
  selector: 'ns-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})

export class CommentsComponent implements OnInit {

  backbtn: string;
  comments = new ObservableArray()
  searchSongResult = [];
  commentText: string;
  menuButton: string;
  isRendering: boolean;
  noComments: boolean;
  user: any;
  headers: HttpHeaders;
  songId: string;
  from: string;
  sendBtn: string;
  commentValue: string;
  
  constructor(private page: Page, private route: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService,
    private http: HttpClient) {

    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });

      this.user = JSON.parse(Values.readString(Values.USER, ''));
    }
  }

  ngOnInit() {

    this.page.actionBarHidden = true;
    this.userService.activeScreen("comments");
    this.userService.showHideFooter(false);
    this.userService.showHidePlayer(false);
    this.backbtn = "res://back_black";
    this.menuButton = "res://menu_grey";
    this.noComments = false;
    this.commentValue = "";
    this.commentText = "Comments"
    this.sendBtn = "res://send"
    this.route.queryParams.subscribe(params => {
      console.log('params', params["id"]);
      if (params["id"] != undefined) {
        this.songId = params["id"];
      }
    });

    this.getComments();
  }

  getComments() {

    this.comments = new ObservableArray();

    this.http.get(Values.BASE_URL + `comments?songId=${this.songId}`, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            this.isRendering = true;
            console.log('comments res', res.data)
            if (res.data.length > 0) {
              this.noComments = false;
              for (var i = 0; i < res.data.length; i++) {
                this.comments.push({
                  id: res.data[i].id,
                  userName: res.data[i].userName,
                  text: res.data[i].text,
                  time: res.data[i].timeStamp
                })
              }
            } else {
              this.noComments = true;
            }
          }
        }
      }, error => {
        console.log(error.error.error);
        if (error.error.error == 'unauthorized_user') {
          this.userService.showErrorDialog(true, 'Session timeout. Login again');
          this.userService.loginUpdate(false);
          this.routerExtensions.navigate(['/login']);
        }
      });
  }

  onSendButton() {

    if (this.commentValue == "") {
      this.userService.showErrorDialog(true, 'Type Your Comment');
      return false;
    }

    this.isRendering = false;

    let model = {
      "text": this.commentValue,
      "songId": this.songId
    };

    this.http.post(Values.BASE_URL + "comments", model, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          console.log('res', res)
          this.isRendering = true;
          if (res.isSuccess == true) {
            this.commentValue = "";
            Toast.makeText("Comment successfully Added!!!", "long").show();
            this.getComments();
          }
        }
      }, error => {
        console.log("ERROR::::", error);
      });
  }

  commentTextField(event: any) {
    this.commentValue = event.object.text;
  }

  onBack() {
    this.routerExtensions.back();
  }

  pageLoaded(args: EventData) {
    this.page = args.object as Page;
  }

  pageUnloaded() {
    this.comments = new ObservableArray();
  }

}
