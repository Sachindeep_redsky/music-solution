import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { LibraryComponent } from "./components/library.component";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { LibraryRoutingModule } from './library-routing.module';
import { NgModalModule } from "../modal/ng-modal";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { PlayerModule } from './../shared/player/player.module';
import { FooterModule } from "../shared/footer/footer.module";

@NgModule({
  imports: [
    LibraryRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NgModalModule,
    NativeScriptUIListViewModule,
    PlayerModule
  ],
  declarations: [
    LibraryComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class LibraryModule { }
