import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { UserService } from '~/app/services/user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModalComponent } from '~/app/modal/modal.component';
import { ad } from "tns-core-modules/utils/utils"
import { Values } from '~/app/values/values';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';
import { Page } from 'tns-core-modules/ui/page/page';
import { SongService } from '~/app/services/song.service';

@Component({
  selector: 'ns-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})
export class LibraryComponent implements OnInit {
  @ViewChild('createDialog', { static: false }) createDialog: ModalComponent;

  pageHeading: string;
  librarys = [];
  isDialog: boolean;
  playlistName: string;
  plus_icon: string;
  create_playlist: string;
  user: any;
  offline_music_icon: string;
  headers: HttpHeaders;
  isLogin: boolean;
  isRendering: boolean;
  isInternetConnection: boolean;

  constructor(private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient,
    private page: Page, private changeDetectorRef: ChangeDetectorRef, private songService: SongService) {
    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {
      this.isLogin = true;
      console.log(Values.readString(Values.X_ACCESS_TOKEN, ''));
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });
      this.user = JSON.parse(Values.readString(Values.USER, ''));
      
      // this.getLikedSongs();

    } else {
      this.librarys = [
        {
          name: "Offline Music", songs: []
        },
        {
          name: "Downloads", songs: []
        }
      ]
      this.isLogin = false;
      this.isRendering = true;


    }
    this.offline_music_icon = "res://music_black"
    this.pageHeading = "Library";
    this.plus_icon = "res://plus";
    this.isDialog = false;
    this.isInternetConnection = this.userService.isConnected;
    this.userService.networkChanges.subscribe((state) => {
      if (state == true) {
        this.isInternetConnection = true;
        if (this.isLogin) {
          this.getPlaylists();
        }
        // this.isRendering = false;
      }
      if (state == false) {
        // this.isRendering = true;
        this.isInternetConnection = false;
      }
    });
    if (this.isInternetConnection) {
      if (this.isLogin) {
        this.getPlaylists();
        this.isRendering = false;
      }
      
    }
  }

  ngOnInit() {
    this.page.actionBarHidden = true;
    this.userService.activeScreen("library");
    this.userService.showHideFooter(true);
    let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
    let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');
    let list
    if (stringifiedList) {
      list = JSON.parse(stringifiedList);
    }

    if (index != undefined && index != null) {
      if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
        this.userService.showHidePlayer(true);
      } else {
        this.userService.showHidePlayer(false)
      }
    } else {
      this.userService.showHidePlayer(false)
    }
    this.page.on('navigatedTo', (data) => {
      // console.log(data)
      if (data.isBackNavigation) {
        this.userService.activeScreen("library");
        this.userService.showHideFooter(true);
        let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
        let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');
        let list
        if (stringifiedList) {
          list = JSON.parse(stringifiedList);
        }

        if (index != undefined && index != null) {
          if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
            this.userService.showHidePlayer(true);
          } else {
            this.userService.showHidePlayer(false)
          }
        } else {
          this.userService.showHidePlayer(false)
        }
        if (this.isLogin) {
          
          this.getPlaylists();
        }
      }
    })
  }

  getPlaylists() {
    // this.librarys.length = 3;
    this.isRendering = false;
    this.librarys = [
      {
        name: "Liked Songs", songs: []
      },
      {
        name: "Offline Music", songs: []
      },
      {
        name: "Downloads", songs: []
      }
    ]

    this.http.get(Values.BASE_URL + `playlists?userId=${this.user._id}`, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          console.log('playlist res', res)
          if (res.isSuccess == true) {
            if (res.data.length > 0) {
              for (var i = 0; i < res.data.length; i++) {
                this.librarys.push({
                  id: res.data[i]._id,
                  name: res.data[i].name,
                  songs: res.data[i].songs
                })
              }
            }

            // this.librarys.push({
            //   name: "Downloads", songs: [
            //     { name: "Sounds Like Gorilla", image: "~/assets/hiphop2.jpg" },
            //     { name: "061 Marketing", image: "~/assets/musicposter2.jpg" },
            //     { name: "Sounds Like Gorilla", image: "~/assets/musicposter3.jpg" },
            //     { name: "061 Marketing", image: "~/assets/musicimage.png" },
            //   ]
            // });

            this.librarys.push({
              name: "Create playlist", songs: []
            });
            this.getLikedSongs();
          }
        }
      }, error => {
        console.log(error.error.error);
        if (error.error.error == 'unauthorized_user') {
          this.userService.showErrorDialog(true, 'Session timeout. Login again');
          this.userService.loginUpdate(false);
        }
        setTimeout(() => {
          this.isRendering = true;
          this.changeDetectorRef.detectChanges()
        }, 2)
      });
  }

  getLikedSongs() {
    this.http.get(Values.BASE_URL + `likes?userId=${this.user._id}`, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          console.log('like res', res.data.likes[0])
          if (res.isSuccess == true) {
            if (res.data.likes.length > 0) {
              let length = res.data.likes.length
              for (var i = 0; i < (length < 4 ? length : 4); i++) {
                this.librarys[0].songs.push({
                  id: res.data.likes[i].id,
                  name: res.data.likes[i].name,
                  image: res.data.likes[i].songImage
                })
              }
            } else {
            }
            setTimeout(() => {
              this.isRendering = true;
              this.changeDetectorRef.detectChanges()
            }, 2)
          }
        }
      }, error => {
        console.log(error.error.error);
        if (error.error.error == 'unauthorized_user') {
          this.userService.showErrorDialog(true, 'Session timeout. Login again');
          this.userService.loginUpdate(false);
        }
        setTimeout(() => {
          this.isRendering = true;
          this.changeDetectorRef.detectChanges()
        }, 2)
      });
  }
  onFooterLoaded($event) {

  }

  onLibrary(item) {

    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "id": item.id,
        "type": 'library'
      },
    };

    this.routerExtensions.navigate(['/albums'], extendedNavigationExtras);
  }

  onDownload() {
    this.routerExtensions.navigate(['/download-songs']);

  }

  onLike(item) {

    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "id": item.id,
        "type": 'like'
      },
    };

    this.routerExtensions.navigate(['/likedsongs'], extendedNavigationExtras);

  }

  onOfflineMusic() {
    this.routerExtensions.navigate(['/offline-songs']);
  }

  showDialog() {
    this.isDialog = true;
    setTimeout(() => {
      this.createDialog.show();
    }, 10)
  }

  onPlaylistChange(event) {
    console.log(event.object.text)
    this.playlistName = event.object.text;
  }

  dialogTap(value: string) {
    ad.dismissSoftInput();
    this.createDialog.hide();
    if (value == 'no') {
      this.createDialog.hide();
      this.playlistName = '';
    } else {
      if (this.playlistName == '' || this.playlistName == undefined) {
        this.userService.showErrorDialog(true, 'Playlist name is required');
      } else {
        let playlist = {
          name: this.playlistName,
          userId: this.user._id
        }
        this.http.post(Values.BASE_URL + `playlists`, playlist, { headers: this.headers }).subscribe((res: any) => {
          if (res && res.isSuccess) {

            let extendedNavigationExtras: ExtendedNavigationExtras = {
              queryParams: {
                "id": res.data._id,
                "type": 'library'
              },
            };
            this.routerExtensions.navigate(['/albums'], extendedNavigationExtras);

          }
        }, (error) => {
          console.log('Error:', error);
          this.userService.showErrorDialog(true, error.error.error);
        })
      }
    }

  }

}
