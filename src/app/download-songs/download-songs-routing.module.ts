import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { DownloadSongsComponent } from "./components/download-songs.component";

const routes: Routes = [
  { path: "", component: DownloadSongsComponent }

];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class DownloadRoutingModule { }
