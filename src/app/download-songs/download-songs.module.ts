import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { DownloadRoutingModule } from './download-songs-routing.module';
import { DownloadSongsComponent } from "./components/download-songs.component";
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
  imports: [
    DownloadRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule
  ],
  declarations: [
    DownloadSongsComponent
  ],
  schemas: [
      NO_ERRORS_SCHEMA
  ]
})
export class DownloadSongsModule { }
