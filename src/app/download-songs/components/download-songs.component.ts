import { ImageSource } from 'tns-core-modules/image-source/image-source';
import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { UserService } from '~/app/services/user.service';
import { HttpHeaders } from '@angular/common/http';
import { Values } from '~/app/values/values';
import { Page } from 'tns-core-modules/ui/page/page';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';
import { fromNativeSource } from "tns-core-modules/image-source";
import { isAndroid } from "tns-core-modules/platform";
import { SongService } from '~/app/services/song.service';
import { getConnectionType, connectionType } from "tns-core-modules/connectivity";
import * as Toast from 'nativescript-toast';
import * as utils from "tns-core-modules/utils/utils";
import { ActivatedRoute } from '@angular/router';
// import { Folder } from "tns-core-modules/file-system";
import * as fs from "tns-core-modules/file-system";

const permissions = require('nativescript-permissions');

declare const android: any;

@Component({
  selector: 'ns-download-songs',
  templateUrl: './download-songs.component.html',
  styleUrls: ['./download-songs.component.css']
})

export class DownloadSongsComponent implements OnInit {
  backbtn: string;
  search: string;
  downloadedSongs = [];
  isSearch: boolean;
  menuButton: string;
  nextIcon: string;
  addButton: string;
  addSongsText: string;
  user: any;
  headers: HttpHeaders;
  playlistId: string;
  from: string;
  timage: ImageSource;

  constructor(private page: Page, private route: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private songService: SongService) {
    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      console.log(Values.readString(Values.X_ACCESS_TOKEN, ''));
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });
      this.user = JSON.parse(Values.readString(Values.USER, ''));
    }
  }

  ngOnInit() {

    let list;
    let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
    let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');
    this.isSearch = false;
    this.page.actionBarHidden = true;
    this.userService.activeScreen("download-songs");
    this.userService.showHideFooter(true);

    if (stringifiedList) {
      list = JSON.parse(stringifiedList);
    }

    if (index != undefined && index != null) {
      if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
        this.userService.showHidePlayer(true);
      } else {
        this.userService.showHidePlayer(false)
      }
    } else {
      this.userService.showHidePlayer(false)
    }
    this.backbtn = "res://back_black";
    this.menuButton = "res://menu_grey";
    this.nextIcon = "res://right_arrow"
    this.search = "";
    this.addButton = "res://plus_black"
    this.addSongsText = "Downloaded Songs"
    this.downloadedSongs = [];
    this.route.queryParams.subscribe(params => {
      console.log('params', params['from'])
      if ( params['from'] == "app") {
        this.from = "app"

      } else {
        this.from = "libary";
      }
    });
    if (isAndroid) {
      if (permissions.hasPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
        this.getAndroidSongs().then(() => {
          console.log('Promise Fullfilled')
        }, error => {
          console.log('Not Fullfilled')
        });
      } else {
        permissions.requestPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE, "I need these permissions because I'm cool")
          .then(() => {
            console.log("Got permissions");
            this.getAndroidSongs().then(() => {
              console.log('Promise Fullfilled')
            }, error => {
              console.log('Not Fullfilled')

            });
          })
          .catch(() => {
            console.log("Could not get permissions");
          })
      }
    }

    this.page.on('navigatedTo', (data) => {
      if (data.isBackNavigation) {
        this.userService.activeScreen("download-songs");
        let list;
        let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
        let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');
        this.userService.showHideFooter(true);

        if (stringifiedList) {
          list = JSON.parse(stringifiedList);
        }

        if (index != undefined && index != null) {
          if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
            this.userService.showHidePlayer(true);
          } else {
            this.userService.showHidePlayer(false)
          }
        } else {
          this.userService.showHidePlayer(false)
        }
      }
    })

  }

  getAndroidSongs(): Promise<any> {

    return new Promise((resolve, reject) => {
        let documents = fs.knownFolders.currentApp();
        console.log('DOC::::', documents)
        let appPath = documents.getFolder("MusicSolution").path;
        console.log('AppPath::::', appPath)
        

        try {

          const folder = fs.Folder.fromPath(appPath);
          folder.getEntities()
          .then((entities) => {
              // entities is an array of files and folders.
              entities.forEach((entity) => {
                console.log(entity.name);
                console.log(entity.path);
                this.downloadedSongs.push({
                  name: entity.name,
                  id: 0,
                  song: { url: entity.path },
                  album: "Downloaded Songs",
                  artist: "",
                  image: 'offline',
                  isOfflineSong: true
                })
                //  entity.path
              });
          }).catch((err) => {
              // Failed to obtain folder's contents.
              console.log(err);
          });
          

            
        } catch (error) {
          reject(false);
          console.log(error)
        }
      })

  }

  getImage(data): Promise<any> {

    return new Promise((resolve, reject) => {
      try {
        const mmr = new android.media.MediaMetadataRetriever();
        mmr.setDataSource(data);

        let bfo = new android.graphics.BitmapFactory.Options()
        let rawArt = mmr.getEmbeddedPicture();

        if (rawArt != null && rawArt != undefined) {
          let art = android.graphics.BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);
          let image = fromNativeSource(art);
          resolve(image)
        }
      } catch (error) {
        console.log('Error:', error)
        var thumb = new ImageSource();
        thumb.fromResource("music_black").then(loaded => {
          resolve(thumb)
        })
      }
    })
  }

  onSong(item, index: number) {

    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "offline": true,
        "index": index,
        "list": JSON.stringify(this.downloadedSongs)
      },
    };
    this.routerExtensions.navigate(['/player'], extendedNavigationExtras);

  }

  onFooterLoaded($event) {

  }

  onBack() {
    const type = getConnectionType();
    console.log(this.from)
    if (type == connectionType.none) {
      this.userService.showErrorDialog(true, 'No internet connection !!');
    } else if (this.from == "app") {
      this.routerExtensions.navigate(['/home']);
    } else {
      console.log(this.from)
      this.routerExtensions.back();
    }
  }

}
