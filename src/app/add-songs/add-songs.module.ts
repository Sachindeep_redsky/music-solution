import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AddSongsRoutingModule } from './add-songs-routing.module';
import { AddSongsComponent } from "./components/add-songs.component";
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NgModalModule } from "../modal/ng-modal";

@NgModule({
  imports: [
    AddSongsRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule,
    NgModalModule
  ],
  declarations: [
    AddSongsComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AddSongsModule { }
