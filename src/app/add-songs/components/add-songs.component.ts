import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { UserService } from '~/app/services/user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Values } from '~/app/values/values';
import { Page } from 'tns-core-modules/ui/page/page';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';
import { SongService } from '~/app/services/song.service';
import { ModalComponent } from '~/app/modal/modal.component';

import * as Toast from 'nativescript-toast';

@Component({
  selector: 'ns-add-songs',
  templateUrl: './add-songs.component.html',
  styleUrls: ['./add-songs.component.css']
})
export class AddSongsComponent implements OnInit {

  @ViewChild('songBuyDialog', { static: false }) buyDialog: ModalComponent;

  backbtn: string;
  search: string;
  searchSongResult = [];
  isSearch: boolean;
  menuButton: string;
  nextIcon: string;
  addButton: string;
  selectedSong: any;
  selectedIndex: number;
  addSongsText: string;
  dialogText: string;
  user: any;
  headers: HttpHeaders;
  playlistId: string;
  from: string;
  timeOut: any;
  isRendering: boolean;

  constructor(private page: Page, private route: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService,
    private http: HttpClient, private songService: SongService) {
    this.isRendering = false;
    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });

      this.user = JSON.parse(Values.readString(Values.USER, ''));
    } else {
      this.user = {
        _id: ""
      }
    }
  }

  ngOnInit() {
    this.isSearch = false;
    this.page.actionBarHidden = true;
    this.userService.activeScreen("add-songs");
    this.userService.showHideFooter(true);
    this.userService.showHidePlayer(false);
    this.backbtn = "res://back_black";
    this.menuButton = "res://menu_grey";
    this.nextIcon = "res://right_arrow"
    this.search = "";
    this.addButton = "res://plus_black";
    this.dialogText = "Purchase this Song";
    this.addSongsText = "Add Songs";
    this.selectedIndex = 0;
    this.searchSongResult = [];

    this.route.queryParams.subscribe(params => {
      console.log('params', params)
      if (params['from'] == "album") {
        if (params["id"] != undefined) {
          this.playlistId = params["id"];
          this.from = "playlist";
          this.getRecommendedSongs();
        }
      } else if (params['from'] == 'search') {
        if (params["search"] != undefined) {
          this.playlistId = ""
          this.search = params["search"];
          this.from = "search";
          // this.getSearch()
        }
      }
    });
  }

  getRecommendedSongs() {
    this.searchSongResult = []

    this.http.get(Values.BASE_URL + `songs?userId=${this.user._id}&status=active&isRecommanded=true&pageNo=1&items=20`, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            console.log('rec res', res.data)
            if (res.data.songs.length > 0) {

              for (var i = 0; i < res.data.songs.length; i++) {
                this.searchSongResult.push({
                  id: res.data.songs[i].id,
                  name: res.data.songs[i].name,
                  artist: res.data.songs[i].artistName,
                  image: res.data.songs[i].image.resize_url,
                  isSongPurchase: res.data.songs[i].isSongPurchase,
                  price: res.data.songs[i].price,
                  song: { url: res.data.songs[i].song.url }
                })
              }
            } else {
            }

            setInterval(() => {
              this.isRendering = true;
            }, 2)
          }
        }
      }, error => {
        console.log(error.error.error);
        if (error.error.error == 'unauthorized_user') {
          this.userService.showErrorDialog(true, 'Session timeout. Login again');
          this.userService.loginUpdate(false);
          this.routerExtensions.navigate(['/login']);
        }
      });
  }

  onAdd(item: any, i: number) {

    if (item.isSongPurchase == 'true' || this.user.subscriptionPlan != "") {
      let model = {
        song: {
          _id: item.id
        }
      };
      this.http.put(Values.BASE_URL + "playlists/" + this.playlistId, model, {
        headers: this.headers
      })
        .subscribe((res: any) => {
          if (res != null && res != undefined) {
            console.log('res', res)
            if (res.isSuccess == true) {
              // this.searchSongResult.splice(i, 1);
              Toast.makeText("Song successfully Added!!!", "long").show();
            }
          }
        }, error => {
          console.log("ERROR::::", error.error.error);
          if (error.error.error == "song_already_exists") {
            alert('This song is already added');
          }
        });
    } else {
      this.selectedIndex = i;
      this.selectedSong = item;
      setTimeout(() => {
        this.buyDialog.show()
      }, 50)
    }
  }

  onSearchSongs(item, i: number) {
    if (this.from == "search") {

      let extendedNavigationExtras: ExtendedNavigationExtras = {
        queryParams: {
          "id": item.id,
          "list": JSON.stringify(this.searchSongResult),
          "index": i
        },
      };
      this.songService.setCurrentPlaylist(this.searchSongResult, i);
      this.routerExtensions.navigate(['/player'], extendedNavigationExtras);
    }
  }

  onTextChange(event: any) {
    this.search = event.object.text;
    this.searchSongResult = [];
    clearTimeout(this.timeOut)
    this.timeOut = setTimeout(() => {
      this.getSearch();
    }, 300);

  }

  getSearch() {
    this.http.get(Values.BASE_URL + `songs/search?name=${this.search}&userId=${this.user._id}`, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            this.searchSongResult = []
            console.trace('search res::', res.data.songs)
            if (res.data.songs.length > 0) {
              for (var i = 0; i < res.data.songs.length; i++) {
                this.searchSongResult.push({
                  id: res.data.songs[i].id,
                  name: res.data.songs[i].name,
                  artist: res.data.songs[i].artistName,
                  image: res.data.songs[i].image.resize_url,
                  price: res.data.songs[i].price,
                  isSongPurchase: res.data.songs[i].isSongPurchase
                })
              }
            }
            setInterval(() => {
              this.isRendering = true;
            }, 2)
          }
        }
      }, error => {
        console.log(error.error.error);
        if (error.error.error == 'unauthorized_user') {
          this.userService.showErrorDialog(true, 'Session timeout. Login again');
          this.userService.loginUpdate(false);
          this.routerExtensions.navigate(['/login']);
        }
      });
  }

  onFooterLoaded($event) {
  }


  onBack() {
    this.routerExtensions.back();
  }

  buySong() {

    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        name: this.selectedSong.name,
        id: this.selectedSong.id,
        price: this.selectedSong.price,
        for: 'song'
      }
    };

    this.routerExtensions.navigate(['/orderPayment'], extendedNavigationExtras);
  }

  onCancel() {
    this.buyDialog.hide()
  }

}
