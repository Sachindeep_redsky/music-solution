import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { AddSongsComponent } from "./components/add-songs.component";

const routes: Routes = [
  { path: "", component: AddSongsComponent }

];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})

export class AddSongsRoutingModule { }
