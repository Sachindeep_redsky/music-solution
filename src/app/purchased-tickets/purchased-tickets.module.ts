import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NgModalModule } from "../modal/ng-modal";
import { GridViewModule } from "nativescript-grid-view/angular";
import { PlayerModule } from '../shared/player/player.module';
import { PurchasedTicketsRoutingModule } from "./purchased-tickets-routing.module";
import { PurchasedTicketsComponent } from "./components/purchased-tickets.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        GridViewModule,
        PurchasedTicketsRoutingModule,
        FooterModule,
        NativeScriptUIListViewModule,
        PlayerModule,
        NgModalModule,
    ],
    declarations: [
        PurchasedTicketsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class PurchasedTicketsModule { }
