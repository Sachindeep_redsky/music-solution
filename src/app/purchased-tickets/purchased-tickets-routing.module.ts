import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { PurchasedTicketsComponent } from "./components/purchased-tickets.component";


const routes: Routes = [
    { path: "", component: PurchasedTicketsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PurchasedTicketsRoutingModule { }
