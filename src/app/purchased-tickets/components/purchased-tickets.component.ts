import { Component, OnInit, ViewChild } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Values } from "../../values/values";
import { UserService } from "../../services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Page } from "tns-core-modules/ui/page/page";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { SongService } from "../../services/song.service";

@Component({
    selector: "purchasedTickets",
    moduleId: module.id,
    templateUrl: "./purchased-tickets.component.html",
    styleUrls: ['./purchased-tickets.component.css']
})

export class PurchasedTicketsComponent implements OnInit {

    // @ViewChild('songBuyDialog', { static: false }) buyDialog: ModalComponent;

    pageHeading: string;
    ticketsList;
    user: any;
    headers: HttpHeaders;
    isRendering: boolean;
    stock_out: string;
    buy_now: string;
    isConcertDetail: boolean;
    concertViewClass: string;
    buyTickets: string;

    constructor(private songService: SongService, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient, private page: Page) {


        this.isRendering = false;

        if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

            this.headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
            });

            this.user = JSON.parse(Values.readString(Values.USER, ''));
            this.isConcertDetail = false;
            this.ticketsList = [];
            this.getTickets();
        }
    }

    ngOnInit(): void {

        let list;
        let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
        let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');

        this.page.actionBarHidden = true;
        this.userService.activeScreen("concerts");
        this.userService.showHideFooter(true);

        if (stringifiedList) {
            list = JSON.parse(stringifiedList);
        }

        if (index != undefined && index != null) {
            if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
                this.userService.showHidePlayer(true);
            } else {
                this.userService.showHidePlayer(false)
            }
        } else {
            this.userService.showHidePlayer(false)
        }

        this.pageHeading = "Purchased Tickets";
        this.stock_out = "Stock out";
        this.buy_now = "Buy now";
        this.buyTickets = "Buy tickets";

        this.userService.showConcert.subscribe((state: boolean) => {
            if (state != undefined && state != null) {
                this.concertViewClass = "view-in";
                setTimeout(() => {
                    this.isConcertDetail = state;
                }, 1000);
            }
        });
    }

    getTickets() {
        this.ticketsList = [];
        var id = this.user._id;
        this.http.get(`${Values.BASE_URL}users/${id}`, {
            headers: this.headers
        }).subscribe((res: any) => {
            console.log("RESPONSE:::", res);
            if (res != null && res != undefined) {
                if (res.isSuccess == true) {
                    if (res.data.events.length > 0) {
                        for (var i = 0; i < res.data.events.length; i++) {
                            let ticketNumber = res.data.events[i].tickets;
                            console.log(ticketNumber);
                            this.http.get(`${Values.BASE_URL}events/${res.data.events[i].id}`, {
                                headers: this.headers
                            }).subscribe((res: any) => {
                                console.log("EVENT RESPONSE::::", res);
                                if (res != null && res != undefined) {
                                    if (res.isSuccess == true) {
                                        this.ticketsList.push({
                                            title: res.data.title,
                                            artist: res.data.artistName,
                                            date: res.data.dateAndTime,
                                            address: res.data.address,
                                            ticketNumber: ticketNumber,
                                            inStock: true,
                                        })
                                    }
                                }
                            }, error => {
                                console.log(error.error.error);
                                console.log(error);
                                setTimeout(() => {
                                    this.isRendering = true;
                                }, 10)
                            });
                        }
                        this.isRendering = true;
                    }
                }
            }
        }, error => {
            this.isRendering = true;
            console.log(error.error.error);
            console.log(error);
            setTimeout(() => {
                this.isRendering = true;
            }, 10)
        });
    }

}