import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { ModalComponent } from "~/app/modal/modal.component";
import { HttpClient } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { UserService } from "~/app/services/user.service";

@Component({
    moduleId: module.id,
    selector: "m-verify-otp",
    templateUrl: './verify-otp.component.html',
    styleUrls: ['./verify-otp.component.css'],
})

export class VerifyOtpComponent implements OnInit {

    @ViewChild('errorDialog', { static: false }) errorDialog: ModalComponent;

    pageHeading: string;
    otpHint: string;
    email: string;
    otp: string;
    submitButton: string;
    signIn: string;
    appLogo: string;
    errorText: string;
    passwordHint: string;
    password: string;
    confirmPasswordHint: string;
    confirmPassword: string;
    isForgotPassword: boolean;
    isLoading: boolean;
    constructor(private route: ActivatedRoute, private page: Page, private http: HttpClient, private routerExtensions: RouterExtensions, private changeDetector: ChangeDetectorRef,
        private userService: UserService) {
        this.page.actionBarHidden = true;
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            console.log(params);
            if (params["email"] != undefined) {
                this.email = params["email"];
            }
            if (params["from"] != undefined) {
                if (params["from"] == 'register') {
                    this.isForgotPassword = false;
                } else {
                    this.isForgotPassword = true;
                }
            }
        });
        this.userService.showHideFooter(false);
        this.userService.showHidePlayer(false);
        this.userService.activeScreen("verify-otp");
        this.passwordHint = "Password";
        this.password = "";
        this.confirmPasswordHint = "Confirm Password";
        this.confirmPassword = "";
        this.pageHeading = "Confirm OTP";
        this.otpHint = "OTP";
        this.otp = "";
        this.errorText = "";
        this.submitButton = "SUBMIT";
        this.signIn = "SIGN IN";
        this.isLoading = false;
        this.appLogo = "res://logo";
    }

    onSubmitButton() {

        let otpModel;
        let route;

        if (this.otp == '') {
            this.errorText = "OTP can not be empty";
            this.errorDialog.show();
            return;
        }
        else if (this.otp.length < 6) {
            this.errorText = "OTP must be of 6 digits";
            this.errorDialog.show();
            return;
        }
        else {
            if (this.isForgotPassword) {
                if (this.password == "") {
                    this.errorDialog.show();
                    this.errorText = "Please enter password";
                    return;
                }
                else if (this.password.length < 6) {
                    this.errorDialog.show();
                    this.errorText = "Password should be at least 6 digits";
                    return;
                }
                else if (this.confirmPassword != this.password) {
                    this.errorDialog.show();
                    this.errorText = "Password mismatch";
                    return;
                } else {
                    otpModel = {
                        email: this.email,
                        otp: this.otp,
                        newPassword: this.password
                    }
                    route = "resetPassword"
                }
            } else {
                otpModel = {
                    email: this.email,
                    otp: this.otp
                }
                route = "verifyUser"
            }
            this.isLoading = true;
            var headers = {
                'Content-Type': 'application/json',
            }

            this.http.post(Values.BASE_URL + `users/${route}`, otpModel, { headers: headers }).subscribe((res: any) => {
                if (res && res.isSuccess) {
                    if (this.isForgotPassword) {
                        this.userService.showErrorDialog(true, 'Password changed successfully');
                        this.routerExtensions.navigate(['/login']);
                    } else {
                        if (res.data && res.data.token && res.data.token != undefined && res.data.token != null) {
                            Values.writeString(Values.USER, JSON.stringify(res.data));
                            Values.writeString(Values.X_ACCESS_TOKEN, res.data.token);
                            this.isLoading = false;
                            this.userService.showHideFooter(true);
                            this.routerExtensions.navigate(['/home'], {
                                clearHistory: true
                            });
                        }
                    }
                }
            }, (error) => {
                console.log('Error:', error)
                this.isLoading = false;
                if (error.error.statusCode == 401 && error.error.error == "invalid_otp") {
                    this.errorText = "Invalid OTP entered"
                } else if (error.error.statusCode == 406 && error.error.error == "otp_expired") {
                    this.errorText = "OTP expired"
                } else {
                    this.errorText = "Something went wrong";
                }
                this.errorDialog.show();
            })
        }
    }

    onErrorOKTap($event) {
        setTimeout(() => {
            this.errorDialog.hide();

        }, 10)
    }

    onOtpTextChange(args: any) {
        var textField = <TextField>args.object;
        this.otp = textField.text;
    }

    onPasswordTextChange(args: any) {
        var textField = <TextField>args.object;
        this.password = textField.text;
    }

    onConfirmPasswordTextChange(args: any) {
        var textField = <TextField>args.object;
        this.confirmPassword = textField.text;
    }

    onSignin() {
        this.routerExtensions.navigate(['/login']);
    }
}