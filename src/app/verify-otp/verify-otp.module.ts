import { ForgotPasswordRoutingModule } from './verify-otp-routing.module';
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NgModalModule } from "../modal/ng-modal";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { VerifyOtpComponent } from "./components/verify-otp.component";


@NgModule({
    bootstrap: [
        VerifyOtpComponent
    ],
    imports: [
        HttpModule,
        NativeScriptCommonModule,
        ForgotPasswordRoutingModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        NgModalModule
    ],
    declarations: [
        VerifyOtpComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class VerifyOtpModule { }
