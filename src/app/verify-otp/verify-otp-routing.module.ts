import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { VerifyOtpComponent } from "./components/verify-otp.component";

const routes: Routes = [
    { path: "", component: VerifyOtpComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ForgotPasswordRoutingModule { }
