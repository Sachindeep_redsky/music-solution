import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { AboutAppComponent } from './components/about-app.component';
import { AboutAppRoutingModule } from './about-app-routing.module';
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { PlayerModule } from '../shared/player/player.module';

@NgModule({
  imports: [
    AboutAppRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule,
    PlayerModule
  ],
  declarations: [AboutAppComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AboutAppModule { }
