import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { HttpHeaders } from '@angular/common/http';
import { UserService } from '~/app/services/user.service';
import { Page } from 'tns-core-modules/ui/page/page';
import { Values } from '~/app/values/values';

import * as appversion from "nativescript-appversion";
import * as utils from "tns-core-modules/utils/utils";

@Component({
  selector: 'ns-settings',
  templateUrl: './about-app.component.html',
  styleUrls: ['./about-app.component.css']
})

export class AboutAppComponent implements OnInit {

  backButton: string;
  headerText: string;
  versionHeading: string;
  currentVersion: string;
  termsAndConditionsHeading: string;
  termsAndConditionsSubHeading: string
  privacyPolicyHeading: string;
  privacyPolicySubHeading: string;
  supportHeading: string;
  supportSubHeading: string;
  headers: HttpHeaders;
  backbtn: string;
  user: any;
  isRendering: boolean;

  constructor(private routerExtensions: RouterExtensions, private userService: UserService,
    private page: Page) {
    this.page.actionBarHidden = true;
    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      console.log(Values.readString(Values.X_ACCESS_TOKEN, ''));
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });
      if (Values.readString(Values.USER, '')) {
        this.user = JSON.parse(Values.readString(Values.USER, ''));
      }
    }
  }

  ngOnInit() {
    this.headerText = "About Us";
    this.versionHeading = "Version";
    this.termsAndConditionsHeading = "Terms And Conditions";
    this.termsAndConditionsSubHeading = "All the stuff you need to know"
    this.privacyPolicyHeading = "Privacy Policy";
    this.privacyPolicySubHeading = "Important for both of us";
    this.supportHeading = "Support";
    this.supportSubHeading = "Get help from us and the community"
    this.backbtn = "res://back_black";
    this.userService.showHideFooter(true);
    this.userService.showHidePlayer(false);
    this.userService.activeScreen('about-app');
    this.page.on('navigatedTo', (data) => {
      if (data.isBackNavigation) {
    this.userService.activeScreen('about-app');
    this.userService.showHideFooter(true);
        this.userService.showHidePlayer(false);
      }
    })

    appversion.getVersionName().then((v: string) => {
      console.log("Your app's version is: " + v);
      this.currentVersion = v;
    });

  }

  onBack() {
    this.routerExtensions.back();
  }

  onTermsConditions() {
    utils.openUrl("http://redskyatech.com");
  }

  onPrivacy() {
    utils.openUrl("http://redskyatech.com");
  }

  onSupport() {
    utils.openUrl("http://redskyatech.com");
  }

}
