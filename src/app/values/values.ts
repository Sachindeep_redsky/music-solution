import * as appSettings from "tns-core-modules/application-settings";

export class Values {

    public static X_ACCESS_TOKEN: string = "x_access_token";
    public static LOGIN_STATE: string = "login_state";
    // public static BASE_URL: string = "http://3.0.96.134:2000/api/";
    public static BASE_URL: string = "http://35.154.40.236:3000/api/";
    public static SELECTED_OPTION: string = "selected_option";
    public static isNotNewUser: string = "is_not_new_user";
    public static LIBRARY_ID: string = "library_id";
    public static RECENTLY_PLAYED: string = "recently_played";
    public static FACEBOOK_ACCESS_TOKEN = "facebook_access_token"
    public static USER = "user";
    public static IS_SHUFFLE = "isShuffle";
    public static IS_REPEATED = "isRepeat";
    public static CURRENT_SONG_LIST = "current_song_list";
    public static CURRENT_SONG_INDEX: string = "current_song_index";
    public static CURRENT_SONG: string = "current_song";
    public static HAS_SUBSCRIPTION: string = "hasSubscription";

    public static TESTING: boolean = true;
    // uncomment for relese builds
    // public static INTERSTITIAL_ID: string = "ca-app-pub-9698230447000562/3430457479";
    // public static IOS_INTERSTITIAL_ID: string = "ca-app-pub-3940256099942544/4411468910";

    // uncoment for teasting
    public static INTERSTITIAL_ID: string = "ca-app-pub-3940256099942544/8691691433";
    public static IOS_INTERSTITIAL_ID: string = "ca-app-pub-3940256099942544/4411468910";


    public static writeString(key: string, value: string): void {
        appSettings.setString(key, value);
    }

    public static readString(key: string, defaultValue: string): string {
        return appSettings.getString(key, defaultValue);
    }

    public static writeNumber(key: string, value: number): void {
        appSettings.setNumber(key, value);
    }

    public static readNumber(key: string, defaultValue: number): number {
        return appSettings.getNumber(key, defaultValue);
    }

    public static writeBoolean(key: string, value: boolean): void {
        appSettings.setBoolean(key, value);
    }

    public static readBoolean(key: string, defaultValue: boolean): boolean {
        return appSettings.getBoolean(key, defaultValue);
    }

    public static doesExist(key: string): boolean {
        return appSettings.hasKey(key);
    }

    public static remove(key: string) {
        appSettings.remove(key);
    }

}