import { UserService } from './../../services/user.service';
import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from '@angular/router';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';

@Component({
    moduleId: module.id,
    selector: "m-splash",
    templateUrl: './splash.component.html',
    styleUrls: ['./splash.component.css'],
})
export class SplashComponent implements OnInit {
    appLogo: string;
    signinButton: string;
    signupButton: string;
    rightsLabel: string;
    from:string;

    constructor(private route: ActivatedRoute,private page: Page, private routerExtensions: RouterExtensions, private userService: UserService) {
        this.page.actionBarHidden = true;
        this.route.queryParams.subscribe(params => {
            console.log('login params', params)
            if (params["from"] && params["from"] != undefined) {
                this.from = params["from"]
            }
        })
    }

    ngOnInit() {

        this.userService.showHideFooter(false);
        this.userService.showHidePlayer(false); 
        this.userService.activeScreen("splash")
        this.signinButton = "SIGN IN";
        this.signupButton = "SIGN UP";
        this.appLogo = "res://logo";
        this.rightsLabel = "SeyBitz @ 2020. All Rights Reserved.";

    }

    onSigninButton() {
        if (this.from == 'player') {
            let extendedNavigationExtras: ExtendedNavigationExtras = {
                queryParams: {
                    from:this.from
                }
            };
            this.routerExtensions.navigate(['/login'],extendedNavigationExtras);
        }else{

            this.routerExtensions.navigate(['/login']);
        }
        
    }

    onSignupButton() {
        this.routerExtensions.navigate(['/register']);
    }

}