import { SplashComponent } from './components/splash.component';
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NgModalModule } from "../modal/ng-modal";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { SplashRoutingModule } from './splash-routing.module';


@NgModule({
    bootstrap: [
        SplashComponent
    ],
    imports: [
        HttpModule,
        NativeScriptCommonModule,
        SplashRoutingModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        NgModalModule
    ],
    declarations: [
        SplashComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SplashModule { }
