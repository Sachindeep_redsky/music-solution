import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { OfflineRoutingModule } from './offline-songs-routing.module';
import { OfflineSongsComponent } from "./components/offline-songs.component";
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
  imports: [
    OfflineRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule
  ],
  declarations: [
    OfflineSongsComponent
  ],
  schemas: [
      NO_ERRORS_SCHEMA
  ]
})
export class OfflineSongsModule { }
