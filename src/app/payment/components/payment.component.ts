import { Values } from './../../values/values';
import { Component, AfterViewInit, OnDestroy } from "@angular/core";
import { CreditCardView, Stripe } from "nativescript-stripe";
import { TextField } from "ui/text-field/text-field"
import { RouterExtensions } from "nativescript-angular";
import { ActivatedRoute } from "@angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ScrollView } from "tns-core-modules/ui/scroll-view/scroll-view";
import { Page } from 'tns-core-modules/ui/page/page';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';
import { UserService } from '~/app/services/user.service';

@Component({
    selector: "app-payment",
    moduleId: module.id,
    templateUrl: "./payment.component.html",
    styleUrls: ['./payment.component.css']
})

export class PaymentComponent implements AfterViewInit, OnDestroy {

    private stripe: Stripe;

    public name: string;
    public email: string;
    public country: string;
    public state: string;
    public city: string;
    public line1: string;
    public line2: string;
    public zipCode: string;
    public phone: string;

    isLoading: boolean;
    isRendering: boolean
    token: string;

    loadingTimeout;
    renderingTimeout;
    subscription;
    scrollView;

    constructor(private activatedRouter: ActivatedRoute, private routerExtensions: RouterExtensions, private http: HttpClient, private page: Page,
        private userService: UserService) {

        this.isLoading = false;
        this.isRendering = false;
        this.email = '';
        this.subscription = {};
        this.userService.activeScreen("payment");
        this.page.actionBarHidden = true;
        this.activatedRouter.queryParams.subscribe((params) => {
            if (params) {
                if (params.id && params.ticketNo && params.type) {
                    this.subscription = params;
                } else {
                    console.log('Handle accordingly')
                }
            }
            else {
                console.log('Handle accordingly')
            }
        })

        this.token = '';
        this.stripe = new Stripe("pk_test_oY1TEuIOJuak6zd72mtq2MRC00oAMKujBK");
    }

    ngAfterViewInit(): void {
        this.renderingTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 100)
    }

    onScrollViewLoaded(args: any) {
        this.scrollView = <ScrollView>args.object;
    }

    onCardFieldFocus(args: any) {
        this.scrollView.scrollToVerticalOffset(100, true);
    }

    onNameTextChange(args: any) {
        let textField = <TextField>args.object;
        this.name = textField.text;
    }

    onEmailTextChange(args: any) {
        let textField = <TextField>args.object;
        this.email = textField.text;
    }

    createToken(cardView: CreditCardView): void {

        this.isLoading = true;

        if (this.name == '' || this.name == null || this.name == undefined) {
            this.userService.showErrorDialog(true, 'Please enter full name as on card');
            return;
        }

        if (this.email == '' || this.email == null || this.email == undefined) {
            this.userService.showErrorDialog(true, 'Please enter email to continue');
            return;
        }

        cardView.card.name = this.name;

        this.loadingTimeout = setTimeout(async () => {
            await this.stripe.createToken(cardView.card, (error, token) => {

                if (error) {
                    console.log("ERR:", error);
                    return;
                } else if (token) {
                    this.token = token.id;
                    console.log('TOK:', this.token)
                    if (this.token == '') {
                        console.log("TOKEN is empty")
                        return;
                    }

                    var headers = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
                    });

                    var postBody = {
                        source: this.token,
                        id: this.subscription.id,
                        type: this.subscription.type,
                        ticketNo: this.subscription.ticketNo
                    }

                    this.http.post(Values.BASE_URL + 'payments/capture', postBody, { headers: headers }).subscribe((res: any) => {
                        if (res.statusCode == 200) {

                            this.loadingTimeout = setTimeout(() => {
                                try {
                                    this.userService.showErrorDialog(true, 'Payment Successful');
                                    let extendedNavigationExtras: ExtendedNavigationExtras = {
                                        queryParams: {
                                            "transactionId": res.transactionId
                                        }
                                    };
                                    this.routerExtensions.navigate(['/confirmation'], extendedNavigationExtras);
                                }
                                catch (error) {
                                    this.isLoading = false;
                                    console.log('ERRR-INT', error)
                                }
                            }, 200)
                        }
                        else {
                            this.userService.showErrorDialog(true, 'Error: Payment Failed, Please try again.');
                            console.log('ERROR:::PAYMENT:::', res)
                            this.isLoading = false;

                        }
                    }, error => {

                        this.userService.showErrorDialog(true, 'Error: Payment Failed, Please try again.');
                        console.log('Error:::', error)
                        this.isLoading = false;
                    })

                }
            });
        }, 200)
    }


    onBack() {
        this.isLoading = true;
        this.loadingTimeout = setTimeout(() => {
            this.routerExtensions.back();
            this.isLoading = false;
        }, 100)
    }

    ngOnDestroy(): void {
        clearTimeout(this.renderingTimeout);
        clearTimeout(this.loadingTimeout);
    }

}
