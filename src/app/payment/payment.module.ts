import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";

import { HttpClientModule } from "@angular/common/http";
import { PaymentRoutingModule } from "./payment-routing.module";
import { PaymentComponent } from "./components/payment.component";
import { CreditCardViewModule } from "nativescript-stripe/angular"
import { NativeScriptCommonModule } from "nativescript-angular/common";

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    HttpClientModule,
    CreditCardViewModule,
    PaymentRoutingModule
  ],
  declarations: [
    PaymentComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})

export class PaymentModule { }
