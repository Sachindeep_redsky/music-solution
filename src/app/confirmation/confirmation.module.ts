import { ConfirmationComponent } from './components/confirmation.component';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ConfirmationRoutingModule } from './confirmation-routing.module';

@NgModule({
  imports: [
    ConfirmationRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule
  ],
  declarations: [ConfirmationComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class ConfirmationModule { }
