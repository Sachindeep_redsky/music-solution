import { RouterExtensions } from 'nativescript-angular/router';
import { Component, OnInit } from '@angular/core';
import { UserService } from '~/app/services/user.service';
import { Page } from 'tns-core-modules/ui/page/page';
import { HttpClient } from '@angular/common/http';
import { Values } from '~/app/values/values';

@Component({
  selector: 'ns-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})

export class ConfirmationComponent implements OnInit {

  confirmationPic: string;
  confirmationHeading: string;
  confirmationMessage: string;

  constructor(private userService: UserService,
    private page: Page, private http: HttpClient, private routerExtensions: RouterExtensions) {
  }

  ngOnInit() {

    this.page.actionBarHidden = true;
    this.userService.activeScreen("confirmation");
    this.userService.showHideFooter(true);
    this.userService.showHidePlayer(false);
    this.confirmationPic = "res://done";
    this.confirmationHeading = "Congratulations for your subscription";
    this.confirmationMessage = "We will send you an email or sms on confirmation of your order";
    var user = JSON.parse(Values.readString(Values.USER, ''));

    this.getUser(user);

  }

  getUser(user) {

    var headers = {
      'Content-Type': 'application/json',
      'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, '')
    }

    this.http.get(Values.BASE_URL + `users/${user._id}`, { headers: headers }).subscribe((res: any) => {
      console.trace('Response:', res);
      if (res && res.isSuccess) {
        Values.writeString(Values.USER, JSON.stringify(res.data));
        Values.writeString(Values.HAS_SUBSCRIPTION, res.data.hasSubscription);
      }
    }, (error) => {
      console.log('Error:', error);
      if (error.error.error == 'unauthorized_user') {
        this.userService.showErrorDialog(true, 'Session timeout. Login again');
        this.userService.loginUpdate(false);
        this.routerExtensions.navigate(['/login']);
      }
    })
  }

}
