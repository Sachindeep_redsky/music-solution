import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { UserService } from '~/app/services/user.service';
import { Page } from 'tns-core-modules/ui/page/page';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SongService } from '~/app/services/song.service';
import { Values } from '~/app/values/values';
import { ImageSource } from 'tns-core-modules/image-source/image-source';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';
import * as Toast from 'nativescript-toast';

@Component({
  selector: 'ns-likedsongs',
  templateUrl: './likedsongs.component.html',
  styleUrls: ['./likedsongs.component.css']
})
export class LikedsongsComponent implements OnInit {

  backbtn: string;
  search: string;
  likedsongs: any;
  isSearch: boolean;
  hasSongs: boolean;
  headers: HttpHeaders;
  albumImage: any;
  showThumb: boolean;
  likeIcon: string;
  removeIcon: string;
  playButton: string;
  noSongsAdded: string;
  user: any;
  demoImage: string;
  backButton: string;
  isLoadingSongs: boolean;
  constructor(private routerExtensions: RouterExtensions, private userService: UserService, private page: Page, private http: HttpClient,
    private changeDetectorRef: ChangeDetectorRef, private songService: SongService) {

    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });

      this.user = JSON.parse(Values.readString(Values.USER, ''));
    }
  }

  ngOnInit() {
    this.page.actionBarHidden = true;
    this.userService.activeScreen("likedsong");
    this.userService.showHideFooter(true);
    this.userService.showHidePlayer(false);
    this.playButton = "res://play_icon_white";
    this.backButton = "res://back_white";
    this.likeIcon = "res://heart_icon_red";
    this.removeIcon = "res://minus";
    this.noSongsAdded = "No Songs Added";
    this.demoImage = "http://3.0.96.134:2000/files/images/FaceBook_Cover_vickysrk770_93-1587111434499.jpg";
    this.search = "";

    this.likedsongs = [];
    this.getLikedSongs()
    this.page.on('navigatedTo', (data) => {
      if (data.isBackNavigation) {
    this.userService.activeScreen("likedsong");
    this.getLikedSongs()

      }
    })
  }


  getLikedSongs() {
    this.likedsongs = [];
    this.hasSongs = false;
    this.isLoadingSongs = true;

    this.http.get(Values.BASE_URL + `likes?userId=${this.user._id}`, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            console.log('liked song',res)
            if (res.data.likes.length > 0) {
              console.log('image', res.data.likes[0].songImage);
              if (res.data.likes && res.data.likes[0] && res.data.likes[0].songImage && res.data.likes[0].songImage.url) {
                // this.albumImage = res.data.songs[0].image.resize_url;
                ImageSource.fromUrl(res.data.likes[0].songImage.url).then((image) => {
                  this.albumImage = image;
                  this.showThumb = true;
                }, error => {
                  ImageSource.fromUrl(this.demoImage).then((image) => {
                    this.albumImage = image;
                    this.showThumb = true;
                  })
                })
              } else {
                ImageSource.fromUrl(this.demoImage).then((image) => {
                  this.albumImage = image;
                  this.showThumb = true;
                })
              }

              for (var i = 0; i < res.data.likes.length; i++) {
                this.likedsongs.push({
                  id: res.data.likes[i].songId,
                  likeId: res.data.likes[i].id,
                  name: res.data.likes[i].songName,
                  image: res.data.likes[i].songImage.resize_url,
                  artist: res.data.likes[i].songArtistName,
                  song: { url: res.data.likes[i].songUrl.url },
                })
              }

              this.hasSongs = true;

            } else {
              this.hasSongs = false;
              ImageSource.fromUrl(this.demoImage).then((image) => {
                this.albumImage = image;
                this.showThumb = true;
              })
            }
            setTimeout(() => {
              this.isLoadingSongs = false;

              this.changeDetectorRef.detectChanges()
            }, 2)
          }
        }
      }, error => {
        ImageSource.fromResource('music_black').then((image) => {
          this.albumImage = image;
          this.showThumb = true;
        })

        setTimeout(() => {
          this.isLoadingSongs = false;

          this.changeDetectorRef.detectChanges()
        }, 2)
        console.log(error.error.error);
        if (error.error.error == 'unauthorized_user') {
          this.userService.showErrorDialog(true, 'Session timeout. Login again');
          this.userService.loginUpdate(false);
          this.routerExtensions.navigate(['/login']);
        }
      });
    this.hasSongs = false;
  }

  onUnlike(item: any, i: number) {

    this.http.delete(Values.BASE_URL + `likes/delete/${item.likeId}`, {
      headers: this.headers
    }).subscribe((res: any) => {
      if (res != null && res != undefined) {
        if (res.isSuccess == true) {
          console.log('delete res', res)
          this.likedsongs.splice(i, 1);
          Toast.makeText("Song successfully Removed!!!", "long").show();
        }
      }
    }, error => {
      console.log(error.error.error);
      if (error.error.error == 'unauthorized_user') {
        this.userService.showErrorDialog(true, 'Session timeout. Login again');
        this.userService.loginUpdate(false);
        this.routerExtensions.navigate(['/login']);
      }
    })
  }

  onSong(item: any, i: number) {

    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "id": item.id,
        "index": i,
        "list": JSON.stringify(this.likedsongs)
      }
    }

    this.songService.setCurrentPlaylist(this.likedsongs, i);
    this.routerExtensions.navigate(['/player'], extendedNavigationExtras);

  }
  onPlayTap() {
    this.onSong(this.likedsongs[0], 0)
  }

  onFooterLoaded($event) {

  }



  onBack() {
    this.routerExtensions.back();
  }

}
