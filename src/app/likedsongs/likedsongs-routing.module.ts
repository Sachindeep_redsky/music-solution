import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { LikedsongsComponent } from "./components/likedsongs.component";

const routes: Routes = [
  { path: "", component: LikedsongsComponent }

];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class LikedsongsRoutingModule { }
