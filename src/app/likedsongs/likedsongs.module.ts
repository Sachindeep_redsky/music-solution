import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { LikedsongsComponent } from "./components/likedsongs.component";
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { LikedsongsRoutingModule } from './likedsongs-routing.module';
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
  imports: [
    LikedsongsRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule
  ],
  declarations: [
    LikedsongsComponent
  ],
  schemas: [
      NO_ERRORS_SCHEMA
  ]
})
export class LikedsongsModule { }
