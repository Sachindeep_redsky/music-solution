import { screen } from 'tns-core-modules/platform';

export class ScreenDimension {

    public static screenwidth = screen.mainScreen.widthDIPs;
    public static screenheight = screen.mainScreen.heightDIPs;

    public static getheight(percentage: number) {
        return (this.screenheight * percentage) / 100;
    }

    public static getwidth(percentage: number) {
        return (this.screenwidth * percentage) / 100;
    }
}