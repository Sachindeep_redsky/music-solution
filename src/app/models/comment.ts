import { User } from "./user";
import { Comments } from "./comments";

export class Comment {

    comments: Comments[];
    createdBy: User;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.comments = obj.comments;
        this.createdBy = obj.createdBy;
    }
}