import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { UserService } from '~/app/services/user.service';
import { HttpHeaders } from '@angular/common/http';
import { Values } from '~/app/values/values';
import { Page } from 'tns-core-modules/ui/page/page';
import { SongService } from '~/app/services/song.service';

@Component({
  selector: 'ns-current-playlist',
  templateUrl: './current-playlist.component.html',
  styleUrls: ['./current-playlist.component.css']
})

export class CurrentPlaylistComponent implements OnInit {

  backbtn: string;
  search: string;
  songList = [];
  isSearch: boolean;
  menuButton: string;
  nextIcon: string;
  addButton: string;
  headerText: string;
  user: any;
  headers: HttpHeaders;
  playlistId: string;
  from: string;

  constructor(private page: Page, private route: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private songService: SongService) {

    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });

      this.user = JSON.parse(Values.readString(Values.USER, ''));
    }
  }

  ngOnInit() {

    this.isSearch = false;
    this.page.actionBarHidden = true;
    this.userService.activeScreen("current-playlist");
    this.userService.showHideFooter(false);
    this.userService.showHidePlayer(true)
    let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
    let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');
    let list

    if (stringifiedList) {
      list = JSON.parse(stringifiedList);
    }

    if (index != undefined && index != null) {
      if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
        this.userService.showHidePlayer(true);
      } else {
        this.userService.showHidePlayer(false)
      }
    } else {
      this.userService.showHidePlayer(false)
    }

    this.backbtn = "res://close";
    this.search = "";
    this.headerText = "Playlist"
    this.songList = [];
    this.route.queryParams.subscribe(params => {
      console.log('params', params["from"]);
      if (params["from"] != undefined) {
        this.headerText = params["from"];
      }
    })
    this.getSongs();
    this.page.on('navigatedTo', (data) => {
      // console.log(data)
      if (data.isBackNavigation) {
        this.userService.activeScreen("current-playlist");
        this.userService.showHideFooter(false);
        this.userService.showHidePlayer(true)

      }
    })
  }

  getSongs() {

    let tempList = this.songService.songList
    if (tempList != null && tempList != undefined) {
      if (tempList.length > 0) {
        for (var i = 0; i < tempList.length; i++) {
          this.songList.push(tempList[i])
        }
      } else {
      }
    }
  }

  onFooterLoaded($event) {

  }

  onItemTap(index: number) {
    this.songService.setSongIndex(index, true)
  }

  onBack() {
    this.routerExtensions.back();
  }

}
