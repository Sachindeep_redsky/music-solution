import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { CurrentPlaylistComponent } from "./components/current-playlist.component";

const routes: Routes = [
  { path: "", component: CurrentPlaylistComponent }

];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class CurrentPlaylistRoutingModule { }
