import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CurrentPlaylistRoutingModule } from './current-playlist-routing.module';
import { CurrentPlaylistComponent } from "./components/current-playlist.component";
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
  imports: [
    CurrentPlaylistRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule
  ],
  declarations: [
    CurrentPlaylistComponent
  ],
  schemas: [
      NO_ERRORS_SCHEMA
  ]
})
export class CurrentPlaylistModule { }
