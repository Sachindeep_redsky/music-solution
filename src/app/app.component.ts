import { RecentlyPlayedService } from '~/app/services/recently-played.service';
import { RouterExtensions } from 'nativescript-angular/router';
import { Component, ViewChild, NgZone } from "@angular/core";
import { registerElement } from 'nativescript-angular/element-registry';
import { CardView } from "@nstudio/nativescript-cardview"
import { Values } from './values/values';
import { HttpHeaders } from '@angular/common/http';
import { Page } from 'tns-core-modules/ui/page/page';
import { UserService } from './services/user.service';
import { initializeOnAngular } from 'nativescript-image-cache';
import { SongService } from './services/song.service';
import { NotificationInstance } from './services/notification.instance';
import { ModalComponent } from './modal/modal.component';
import { NotificationService } from './services/notification.service';
import * as admob from "nativescript-admob";
import * as application from "tns-core-modules/application";
import { isAndroid } from "tns-core-modules/platform";
import * as Toast from 'nativescript-toast';
import { getConnectionType, connectionType } from "tns-core-modules/connectivity";
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';
import { ImageCacheIt } from 'nativescript-image-cache-it';
registerElement("CardView", () => CardView as any);
registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);
declare const android: any;

const imageCache = require('nativescript-image-cache');
@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "app.component.html",
})


export class AppComponent {
    @ViewChild('errorDialog', { static: false }) errorDialog: ModalComponent;
    @ViewChild('songBuyDialog', { static: false }) buyDialog: ModalComponent;

    searchSongs = [];
    headers: HttpHeaders;
    tries: number;
    user: any;
    showPlayer: boolean;
    showFooter: boolean;
    errorText: string;
    isAdLoaded: boolean;
    moveTaskToBack: any;
    dialogText: string;

    connectionStatusText: string;
    labelBackground: string;

    constructor(private page: Page, private routerExtensions: RouterExtensions, private ngZone: NgZone,
        private userService: UserService, private songService: SongService, private notificationService: NotificationService, private recentlyPlayedService: RecentlyPlayedService) {
        initializeOnAngular();
        ImageCacheIt.enableAutoMM();

        this.showPlayer = true;
        this.showFooter = true;
        this.connectionStatusText = '';

        this.ngZone.run(() => {
            // this.tries = 0;
            application.android.on(application.AndroidApplication.activityBackPressedEvent, (data: application.AndroidActivityBackPressedEventData) => {
                let activeScreen = this.userService.currentPage
                console.log("SCREEN::::", activeScreen);
                if (activeScreen == "home") {
                    data.cancel = true;
                    setTimeout(() => {
                        if (isAndroid) {
                            application.android.startActivity.moveTaskToBack(true);
                        }
                    }, 50);
                } else if (activeScreen == "confirmation") {
                    data.cancel = true;
                    this.routerExtensions.navigate(['/home'])
                } else if (activeScreen == "account") {
                    data.cancel = true;
                    this.routerExtensions.back();
                }
                else if (activeScreen == "payment") {
                    data.cancel = true;
                    this.routerExtensions.back();
                }
                else if (activeScreen == "player") {
                    data.cancel = true;
                    this.userService.showHideFooter(true);
                    this.userService.showHidePlayer(true);
                    this.routerExtensions.back();
                }
                else {
                    data.cancel = true;
                    this.routerExtensions.back();
                }
            });
        });

        this.page.actionBarHidden = true;

        NotificationInstance.SongServiceRef = this.songService;


        if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {
            console.log('token', Values.readString(Values.X_ACCESS_TOKEN, ''))
            this.headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
            });

            this.user = JSON.parse(Values.readString(Values.USER, ''));
            this.userService.loginUpdate(true);
            const type = getConnectionType();
            if (type == connectionType.none) {
                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "from": "app"
                    },
                };
                this.routerExtensions.navigate(['/library'], extendedNavigationExtras);
            }
            else {
                this.routerExtensions.navigate(['/home']);
            }
        } else {
            // this.routerExtensions.navigate(['/splash'])
            this.routerExtensions.navigate(['/home']);
        }

        this.userService.isLogin.subscribe((state: boolean) => {
            if (state == false) {

                Values.remove(Values.X_ACCESS_TOKEN);
                Values.remove(Values.USER);

                this.recentlyPlayedService.reset();
                this.songService.setSongList(null);

                this.routerExtensions.navigate(['/splash'], { clearHistory: true });

                if (this.songService && this.songService.player && this.songService.player.isAudioPlaying()) {
                    this.songService.pause();
                }
                this.notificationService.cancel();
            }
        });

        this.userService.showPlayer.subscribe((state: boolean) => {
            if (state != undefined && state != null) {
                var data = Values.readNumber(Values.CURRENT_SONG_INDEX, 0)
                if (data != undefined && data != null) {
                    this.showPlayer = state;
                }
            }
        })

        this.userService.showFooter.subscribe((state: boolean) => {
            if (state != undefined && state != null) {
                this.showFooter = state;
            }
        })

        this.userService.showError.subscribe((state: boolean) => {
            if (state == true) {
                this.errorText = this.userService.errorMessage;
                console.log(this.errorText)
                setTimeout(() => {
                    this.errorDialog.show();
                }, 10);
            }
        });
        this.songService.songParamChanges.subscribe((params => {
            console.log('songgggggggggggggg chageddddededededddddddededededededeedede')
            if (this.isAdLoaded) {
                this.showAds()
            }
        }));
        this.songService.purchaseEvent.subscribe((state: boolean) => {
            if (state) {
                this.buyDialog.show()
            }
        })

        this.userService.networkChanges.subscribe((state: boolean) => {
            if (state) {
                // console.log("Connected")

                this.connectionStatusText = "Connected"

                this.labelBackground = "green"
            }
            else {
                // console.log("Not Connected")
                this.connectionStatusText = "Not Connected"
                this.labelBackground = "black"
            }
        })

        // this.userService.networkChanges.subscribe((text: string) => {
        //     this.connectionStatusText = text;
        // },
        //     error => {

        //     })

    }

    ngOnInit(): void {
        this.showPlayer = true;
        this.showFooter = true;
        this.isAdLoaded = false;
        this.dialogText = "Purchase this Song";
        setTimeout(() => {
            this.loadAd();
        }, 500);
        // if (stringifiedList) {
        //     list = JSON.parse(stringifiedList);
        // }

        // if (index != undefined && index != null) {
        //     if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
        //         this.userService.showHidePlayer(true);
        //     } else {
        //         this.userService.showHidePlayer(false)
        //     }
        // } else {
        //     this.userService.showHidePlayer(false)
        // }
    }

    onErrorOKTap($event) {
        this.errorDialog.hide();
    }

    loadAd() {
        admob.preloadInterstitial({
            testing: Values.TESTING,
            iosInterstitialId: Values.IOS_INTERSTITIAL_ID,
            androidInterstitialId: Values.INTERSTITIAL_ID,

            // Android automatically adds the connected device as test device with testing:true, iOS does not
            iosTestDeviceIds: ["ce97330130c9047ce0d4430d37d713b2"],
            keywords: ["View", "Kew"], // add keywords for ad targeting
            onAdClosed: () => {
                console.log("interstitial closed")
                this.isAdLoaded = false;
                setTimeout(() => {
                    this.loadAd();
                }, 500)
            }
        }).then(() => {
            this.isAdLoaded = true;

            console.log("interstitial preloaded - you can now call 'showInterstitial' whenever you're ready to do so");
        },
            function (error) {
                console.log("admob preloadInterstitial error: " + error);
            }
        )
    }

    showAds() {
        admob.showInterstitial().then(
            () => {
                // this will resolve almost immediately, and the interstitial is shown without a delay because it was already loaded
                console.log("interstitial showing");
            },
            function (error) {
                console.log("admob showInterstitial error: " + error);
            }
        )
    }

    showDialog() {
        this.buyDialog.show();
    }

    buySong() {
        if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {
            let song = this.songService.currentPlaylist[this.songService.currentIndex]
            let extendedNavigationExtras: ExtendedNavigationExtras = {
                queryParams: {
                    name: song.name,
                    id: song.id,
                    price: song.price,
                    for: 'song'
                }
            };
            console.log("EXTENDED NAVIGATIPN EXTRASSSSSS:::::", extendedNavigationExtras);
            this.routerExtensions.navigate(['/orderPayment'], extendedNavigationExtras);
        } else {
            let extendedNavigationExtras: ExtendedNavigationExtras = {
                queryParams: {
                    from: "player"
                }
            };
            this.routerExtensions.navigate(['/splash'], extendedNavigationExtras);

        }
    }

    onCancel() {
        this.buyDialog.hide()
    }
}

