import { Component, OnInit, ViewChild } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpClient } from "@angular/common/http";
import { ad } from "tns-core-modules/utils/utils"
import { Values } from "~/app/values/values";
import { ModalComponent } from "~/app/modal/modal.component";
import { UserService } from "~/app/services/user.service";
import * as firebase from "nativescript-plugin-firebase";
import { ActivatedRoute } from "@angular/router";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { SongService } from "~/app/services/song.service";

@Component({
    selector: "app-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.component.css'],
})

export class LoginComponent implements OnInit {
    @ViewChild('errorDialog', { static: false }) errorDialog: ModalComponent;

    pageHeading: string;
    usernameHint: string;
    username: string;
    passwordHint: string;
    password: string;
    signinButton: string;
    forgotPassword: string;
    appLogo: string;
    googleLogo: string;
    twitterLogo: string;
    facebookLogo: string;
    errorText: string;
    isLoading: boolean;
    from: string;
    constructor(private route: ActivatedRoute,private songService: SongService, private page: Page, private http: HttpClient, private routerExtensions: RouterExtensions,
        private userService: UserService) {
        this.page.actionBarHidden = true;
        this.route.queryParams.subscribe(params => {
            console.log('login params', params)
            if (params["from"] && params["from"] != undefined) {
                this.from = params["from"]
            }
        })
    }

    ngOnInit() {
        this.userService.showHideFooter(false);
        this.userService.showHidePlayer(false);
        this.userService.activeScreen("login");
        this.pageHeading = "Sign In your Account";
        this.usernameHint = "Username";
        this.username = "";
        this.passwordHint = "Password";
        this.password = "";
        this.signinButton = "SIGN IN";
        this.forgotPassword = "Forgot Password?";
        this.appLogo = "res://logo";
        this.googleLogo = "res://google";
        this.twitterLogo = "res://twitter";
        this.facebookLogo = "res://facebook";
        this.isLoading = false;
    }

    onUsernameTextChange(args: any) {
        var textField = <TextField>args.object;
        this.username = textField.text;
    }

    onPasswordTextChange(args: any) {
        var textField = <TextField>args.object;
        this.password = textField.text;
    }

    onSigninButton() {

        ad.dismissSoftInput();
        if (this.username == "") {
            this.errorDialog.show();
            this.errorText = "Please enter username";
            return;
        }
        else if (this.password == "") {
            this.errorDialog.show();
            this.errorText = "Please enter password";
            return;
        }
        else if (this.password.length < 6) {
            this.errorDialog.show();
            this.errorText = "Password should be at least 6 digits";
            return;
        }
        else {

            this.isLoading = true;
            var user = {
                userName: this.username,
                password: this.password
            }

            var headers = {
                'Content-Type': 'application/json'
            }
            this.http.post(Values.BASE_URL + `users/login`, user, { headers: headers }).subscribe((res: any) => {
                console.trace('Response:', res);
                if (res && res.isSuccess) {
                    console.log('Response:', res);
                    Values.writeString(Values.USER, JSON.stringify(res.data));
                    Values.writeString(Values.X_ACCESS_TOKEN, res.data.token);
                    Values.writeString(Values.HAS_SUBSCRIPTION, res.data.hasSubscription);
                    this.isLoading = false;
                    this.userService.showHideFooter(true);
                    if (this.from == 'player') {
                        let song = this.songService.currentPlaylist[this.songService.currentIndex]
                        let extendedNavigationExtras: ExtendedNavigationExtras = {
                            queryParams: {
                                name: song.name,
                                id: song.id,
                                price: song.price,
                                for: 'song'
                            }
                        };
                        console.log("EXTENDED NAVIGATIPN EXTRASSSSSS:::::", extendedNavigationExtras);
                        this.routerExtensions.navigate(['/orderPayment'], extendedNavigationExtras);
                    }else{

                        this.routerExtensions.navigate(['/home'], { clearHistory: true })
                    }
                }
            }, (error) => {
                console.log('Error:', error);
                this.isLoading = false;
                if (error.error.statusCode == 401 && error.error.error == "password_mismatch") {
                    this.errorText = "Incorrect Password"
                } else if (error.error.statusCode == 404 && error.error.error == "user_not_found") {
                    this.errorText = "User does not exist"
                } else {
                    this.errorText = "Something went wrong";
                }
                this.errorDialog.show();
                Values.writeString(Values.USER, "");
            })
        }
    }

    onGoogle() {
        var that = this;
        this.isLoading = true;
        firebase.login({
            type: firebase.LoginType.GOOGLE
            // Optional 
            // googleOptions: {
            //     hostedDomain: "mygsuitedomain.com",
            //     // NOTE: no need to add 'profile' nor 'email', because they are always provided
            //     // NOTE 2: requesting scopes means you may access those properties, but they are not automatically fetched by the plugin
            //     scopes: ['https://www.googleapis.com/auth/user.birthday.read']
            // }
        }).then((result) => {
            JSON.stringify(result);
            console.log('google result', result);
            if (result && result.providers) {
                result.providers.forEach(element => {
                    if (element.token !== null && element.token !== undefined) {
                        setTimeout(() => {
                            var _name = result.displayName.split(' ', 1);
                            var _userName = _name[0] + (Math.floor(1000 + Math.random() * 9000)).toString();
                            let user = {
                                userName: _userName,
                                email: result.email,
                                uId: result.uid,
                                tempToken: element.token,
                                loginType: 'google'
                            }
                            this.http.post(Values.BASE_URL + `users`, user).subscribe((res: any) => {
                                console.trace("google:::SIGNUP", res)
                                if (res && res.isSuccess && res.data) {
                                    Values.writeString(Values.USER, JSON.stringify(res.data));
                                    Values.writeString(Values.X_ACCESS_TOKEN, res.data.token);
                                    Values.writeString(Values.HAS_SUBSCRIPTION, res.data.hasSubscription);
                                    this.isLoading = false;
                                    this.routerExtensions.navigate(['/home'], { clearHistory: true })

                                }
                            }, (error) => {
                                console.log('Error:', error);
                                this.isLoading = false;
                                if (error.error.statusCode == 422 && error.error.error == "user_already_exists") {
                                    this.errorText = "User already exists"
                                } else {
                                    this.errorText = "Something went wrong";
                                }
                                this.errorDialog.show();
                            })

                        }, 200)
                    }
                });
            }
        }, (errorMessage) => {
            this.isLoading = false;
            this.userService.showErrorDialog(true, "Please check your network connection.");
            // alert(errorMessage)
            console.log(errorMessage);
        }
        );
    }

    onTwitter() {
        alert("twitter clicked");
    }

    onFacebook() {
        // alert("facebook clicked");
        this.isLoading = true;
        firebase.login({
            type: firebase.LoginType.FACEBOOK,
            // Optional
            facebookOptions: {
                // defaults to ['public_profile', 'email']
                scopes: ['public_profile', 'email'] // note: this property was renamed from "scope" in 8.4.0
            }
        }).then((result) => {
            JSON.stringify(result);
            console.log(result)
            if (result && result.providers) {
                result.providers.forEach(element => {
                    if (element.id == "facebook.com" && element.token !== null && element.token !== undefined) {
                        setTimeout(() => {
                            var _name = result.displayName.split(' ', 1);
                            var _userName = _name[0] + (Math.floor(1000 + Math.random() * 9000)).toString();
                            let user = {
                                userName: _userName,
                                email: result.email,
                                uId: result.uid,
                                tempToken: element.token,
                                loginType: 'facebook'
                            }
                            this.http.post(Values.BASE_URL + `users`, user).subscribe((res: any) => {
                                console.trace("facebook:::SIGNUP", res)
                                if (res && res.isSuccess && res.data) {
                                    Values.writeString(Values.USER, JSON.stringify(res.data));
                                    Values.writeString(Values.X_ACCESS_TOKEN, res.data.token);
                                    Values.writeString(Values.HAS_SUBSCRIPTION, res.data.hasSubscription);
                                    this.isLoading = false;
                                    this.routerExtensions.navigate(['/home'], { clearHistory: true })

                                }
                            }, (error) => {
                                console.log('Error:', error);
                                this.isLoading = false;
                                if (error.error.statusCode == 422 && error.error.error == "user_already_exists") {
                                    this.errorText = "User already exists"
                                } else {
                                    this.errorText = "Something went wrong";
                                }
                                this.errorDialog.show();
                            })

                        }, 500)
                    }
                });
            }

        },
            (errorMessage) => {
                this.isLoading = false;
                console.log(errorMessage);
            }
        );
    }

    onForgotPassword() {
        this.routerExtensions.navigate(['/forgotPassword']);
    }

    onErrorOKTap($event) {
        this.errorDialog.hide();
    }

}