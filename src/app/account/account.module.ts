import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AccountComponent } from "./components/account.component";
import { AccountRoutingModule } from './account-routing.module';
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

@NgModule({
  imports: [
    AccountRoutingModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
  ],
  declarations: [AccountComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AccountModule { }
