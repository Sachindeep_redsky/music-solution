import { ChangeDetectorRef } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '~/app/services/user.service';
import { Values } from '~/app/values/values';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';
import { Page } from 'tns-core-modules/ui/page/page';
import { SongService } from '~/app/services/song.service';

@Component({
  selector: 'ns-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit, OnDestroy {

  pageHeading: string;
  profilePic: string;
  profileName: string;
  subscriptionPlan: string;
  profileHeading: string;
  menu: string;
  purchasedTicketsHeading: string;
  subscriptionPlanHeading: string;
  settingsHeading: string;
  aboutAppHeading: string;
  headers: HttpHeaders;
  user: any;
  isInternetConnection: boolean;
  isRendering: boolean;
  internetMessage: string;
  isLogin: boolean;
  wifiOffIcon: string;

  constructor(private routerExtensions: RouterExtensions, private userService: UserService,
    private http: HttpClient, private page: Page, private songService: SongService, private changeDetector: ChangeDetectorRef) {
    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {
      this.isLogin = true;
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });
      this.user = JSON.parse(Values.readString(Values.USER, ''));
    } else {
      this.isLogin = false;
    }
    this.isRendering = true;
    this.pageHeading = "Account";
    this.profilePic = "res://user";
    this.profileName = "";
    this.subscriptionPlan = "none";
    this.profileHeading = "Profile";
    this.menu = "res://dot_menu_black"
    this.purchasedTicketsHeading = "Purchased Tickets";
    this.subscriptionPlanHeading = "Subscription Plan";
    this.settingsHeading = "Settings";
    this.aboutAppHeading = "About App";
    this.internetMessage = "There is no internet connection available."
    this.isInternetConnection = this.userService.isConnected;

    this.page.on('navigatedTo', (data) => {
      this.userService.activeScreen("account");
      if (data.isBackNavigation) {
        this.userService.activeScreen("account");
        this.user = JSON.parse(Values.readString(Values.USER, ''));
        if (this.user && this.user.profile && this.user.profile.name) {
          this.profileName = this.user.profile.name;
        } else {
          this.profileName = this.user.userName;
        }

        if (this.user && this.user.profile && this.user.profile.image.resize_url) {
          this.profilePic = this.user.profile.image.resize_url;
        } else {
          this.profilePic = "res://user"
        }

        if (this.user && this.user.hasSubscription) {
          this.subscriptionPlan = this.user.hasSubscription;
        } else {
          this.subscriptionPlan = 'none'
        }
        // this.getUser(this.user._id)
        this.changeDetector.detectChanges();

      }
    })
    this.userService.networkChanges.subscribe((state) => {
      if (state == true ) {
        this.isInternetConnection = true;
        console.log("USER:::::::", this.user);
        if (this.isLogin) {
          this.getUser(this.user._id);
        }
        
      }
      if (state == false) {
        this.isInternetConnection = false;
      }
    });
    if (this.isInternetConnection && this.isLogin) {

      this.getUser(this.user._id);
    }

  }

  ngOnInit() {

    this.page.actionBarHidden = true;
    this.userService.activeScreen("account");
    this.userService.showHideFooter(true);
    let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
    let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');
    let list

    if (stringifiedList) {
      list = JSON.parse(stringifiedList);
    }

    if (index != undefined && index != null) {
      if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
        this.userService.showHidePlayer(true);
      } else {
        this.userService.showHidePlayer(false)
      }
    } else {
      this.userService.showHidePlayer(false)
    }

  }

  onTryAgain() { }

  getUser(id: string) {
    console.log("ID::::", id);
    this.isRendering = false;

    this.http.get(`${Values.BASE_URL}users/${id}`, {
      headers: this.headers
    }).subscribe((res: any) => {
      if (res != null && res != undefined) {
        if (res.isSuccess == true) {

          console.trace('User:::', JSON.stringify(res))
          Values.writeString(Values.USER, JSON.stringify(res.data))
          this.user = res.data;
          if (this.user && this.user.profile && this.user.profile.name) {
            this.profileName = this.user.profile.name;
          } else {
            this.profileName = this.user.userName;
          }

          if (this.user && this.user.profile && this.user.profile.image.resize_url) {
            this.profilePic = this.user.profile.image.resize_url;
          } else {
            this.profilePic = "res://user"
          }

          if (this.user && this.user.hasSubscription) {
            this.subscriptionPlan = this.user.hasSubscription;
          } else {
            this.subscriptionPlan = 'none'
          }

          setTimeout(() => {
            this.isRendering = true;
          }, 10)
          // setTimeout(() => {
          //   if (res.data && res.data.profile)
          //     this.profileName = res.data.profile.name;
          //   this.profilePic = res.data.profile.image.resize_url;
          //   this.subscriptionPlan = res.data.subscriptionPlan;
          //   setTimeout(() => {
          //     this.isRendering = true;
          //   }, 10)
          // }, 1)
        }
      }
    }, error => {
      console.log(error.error.error);
      console.log(error);
      setTimeout(() => {
        this.isRendering = true;
      }, 10)
    });
  }

  onProfileMenu() {

    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "id": this.user._id
      }
    };

    this.routerExtensions.navigate(['/edit-profile'], extendedNavigationExtras);

  }

  onPurchasedTickets() {
    this.routerExtensions.navigate(['/purchasedTickets']);
  }

  onSubscriptionMenu() {
    this.routerExtensions.navigate(['/subscription']);
  }

  onSettingsMenu() {
    this.routerExtensions.navigate(['/settings']);
  }

  onAboutMenu() {
    this.routerExtensions.navigate(['/about-app']);
  }

  onPlayer() {
    this.routerExtensions.navigate(['/artist']);
  }

  onLogin(){
    this.routerExtensions.navigate(['/splash']);
  }

  ngOnDestroy(): void {
    this.page.off('navigatedTo', (() => { }))
  }

}
