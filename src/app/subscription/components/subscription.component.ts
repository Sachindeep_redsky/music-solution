import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '~/app/services/user.service';
import { Page } from 'tns-core-modules/ui/page/page';
import { Values } from '~/app/values/values';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';

@Component({
  selector: 'ns-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit {

  subscriptionList = [];
  user: any;
  headers: HttpHeaders;
  pageHeading: string;
  planTimeOne: string;
  planTimeTwo: string;
  planTimeThree: string;
  planHeading: string;
  priceOne: string;
  priceTwo: string;
  priceThree: string;
  buyButton: string;

  isRendering: boolean;

  constructor(private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient, private page: Page) {

    this.page.actionBarHidden = true;
    this.isRendering = false;

    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });

      this.user = JSON.parse(Values.readString(Values.USER, ''));
      this.getSubscription();
    }
  }

  ngOnInit() {

    this.userService.activeScreen("subscription");
    this.userService.showHideFooter(true);
    this.userService.showHidePlayer(false);
    this.pageHeading = "Subscription";
    this.planTimeOne = "Annual Plan";
    this.planTimeTwo = "3 Months";
    this.planTimeThree = "1 Month"
    this.planHeading = "Listen & Download Unlimited Songs";
    this.priceOne = "99";
    this.priceTwo = "39";
    this.priceThree = "10";
    this.buyButton = "BUY";

  }

  getSubscription() {
    this.http.get(Values.BASE_URL + `plans?status=active`, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            if (res.data.length > 0) {
              for (var i = 0; i < res.data.length; i++) {
                this.subscriptionList.push({
                  id: res.data[i]._id,
                  name: res.data[i].name,
                  price: res.data[i].price,
                })
              }
              setTimeout(() => {
                this.isRendering = true;
              }, 10)
            }
          }
        }
      }, error => {
        console.log(error);
        if (error.error.error == 'unauthorized_user') {
          this.userService.showErrorDialog(true, 'Session timeout. Login again');
          this.userService.loginUpdate(false);
          this.routerExtensions.navigate(['/login']);
        }
        setTimeout(() => {
          this.isRendering = true;
        }, 10)
      });
  }

  buyPlan(subscription) {
    if (this.user.hasSubscription == "") {

      let extendedNavigationExtras: ExtendedNavigationExtras = {
        queryParams: {
          name: subscription.name,
          id: subscription.id,
          price: subscription.price,
          for: "plan"
        }
      };

      this.routerExtensions.navigate(['/orderPayment'], extendedNavigationExtras);
    } else {
      this.userService.showErrorDialog(true, 'Subscription plan already exists');
    }
  }
}
