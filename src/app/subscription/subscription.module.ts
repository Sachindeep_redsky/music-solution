import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { SubscriptionRoutingModule } from './subscription-routing.module';
import { SubscriptionComponent } from './components/subscription.component';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';

@NgModule({
  imports: [
    SubscriptionRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule
  ],
  declarations: [SubscriptionComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class SubscriptionModule { }
