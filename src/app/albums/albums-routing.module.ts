import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { AlbumsComponent } from './components/albums.component';

const routes: Routes = [
  { path: "", component: AlbumsComponent }

];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class AlbumsRoutingModule { }
