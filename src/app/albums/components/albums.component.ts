import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '~/app/services/user.service';
import { Page } from 'tns-core-modules/ui/page/page';
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { Values } from '~/app/values/values';
import { SongService } from '~/app/services/song.service';
import { ModalComponent } from '~/app/modal/modal.component';
import { RecentlyPlayedService } from '~/app/services/recently-played.service';
import * as Toast from 'nativescript-toast';
import { ImageSource } from '@nativescript/core/image-source/image-source';

@Component({
  selector: 'ns-artist',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})

export class AlbumsComponent implements OnInit {

  @ViewChild('songBuyDialog', { static: false }) buyDialog: ModalComponent;

  backButton: string;
  mainSongName: string;
  albumName: string;
  artistName: string;
  price: string;
  albumImage: ImageSource;
  songs: any;
  type: string;
  dialogText: string;
  songMenu: string;
  heading: string;
  playButton: string;
  albumId: string;
  headers: HttpHeaders;
  isAddButton: boolean;
  isAlbumPurchase: boolean;
  isLibrary: boolean = false;
  user: any;
  isRendering: boolean;
  noSongsAdded: string;
  selectedSong: any;
  addButton: string;
  isBuy: boolean;
  isRemove: boolean
  likeIcon: string;
  removeIcon: string;
  demoImage: string;
  showThumb: boolean;
  hasSongs: boolean;
  isLoadingSongs: boolean;
  isLogin: boolean;
  constructor(private route: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient,
    private page: Page, private changeDetectorRef: ChangeDetectorRef, private songService: SongService, private recentlyPlayedService: RecentlyPlayedService) {

    this.page.actionBarHidden = true;
    this.isRendering = false;
    this.showThumb = false;
    this.hasSongs = false;
    this.isLoadingSongs = true;
    this.demoImage = "http://3.0.96.134:2000/files/images/FaceBook_Cover_vickysrk770_93-1587111434499.jpg";

    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {
      this.isLogin = true;
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
      });

      this.user = JSON.parse(Values.readString(Values.USER, ''));
    } else {
      this.isLogin = false
      this.user = {
        _id: ""
      }
    }
  }

  ngOnInit() {
    this.userService.activeScreen("albums");
    this.userService.showHideFooter(false);
    this.userService.showHidePlayer(false);
    this.songs = [];

    this.route.queryParams.subscribe(params => {
      console.log('params', params);
      if (params["id"] != undefined) {
        this.albumId = params["id"];
        Values.writeString(Values.LIBRARY_ID, this.albumId);

      }
      if (params["type"] != undefined) {
        this.type = params["type"];

      }
    });

    if (this.type == 'library') {
      this.getLibrary();
      this.isLibrary = true;
      this.isBuy = false;
    } else {
      this.getAlbum();
      // this.getSongs();
      this.isBuy = true;
      this.isLibrary = false;
    }

    this.isAddButton = false;
    this.isRendering = true;
    this.addButton = "res://add"
    this.backButton = "res://back_white";
    this.likeIcon = "res://heart_icon_red";
    this.removeIcon = "res://minus"
    this.artistName = "Selena Gomez";
    this.dialogText = "Purchase this Song";
    this.mainSongName = "";
    this.price = "2";
    this.songMenu = "res://dot_menu_black";
    this.heading = "Songs";
    this.playButton = "res://play_icon_white";
    this.noSongsAdded = "No Songs Added";
    this.isAlbumPurchase = false;
    this.page.on('navigatedTo', (data) => {
      if (data.isBackNavigation) {
        this.userService.activeScreen("albums");
        this.isRendering = false;
        this.albumId = this.recentlyPlayedService.libraryId;
        this.type = this.recentlyPlayedService.libraryFrom;
        if (this.type == 'library') {
          this.getLibrary();
          this.isLibrary = true;
          this.isBuy = false;
        }
        this.changeDetectorRef.detectChanges();

      }
    })
  }

  getAlbum() {

    this.http.get(Values.BASE_URL + `categories/${this.albumId}?userId=${this.user._id}`, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        console.log('Res:::', res)
        if (res != null && res != undefined) {
          if (res.isSuccess == true) {
            this.albumName = res.data.name;
            ImageSource.fromUrl(res.data.image.url).then((image) => {
              this.albumImage = image;
              this.showThumb = true;
            }, error => {
              ImageSource.fromUrl(this.demoImage).then((image) => {
                this.albumImage = image;
                this.showThumb = true;
              })
            })
            this.price = res.data.price;
            if (res.data.isAlbumPurchase = 'true') {
              // this.isBuy = false;
              this.isAlbumPurchase = true;
            } else {
              this.isBuy = true;
              this.isAlbumPurchase = false;
            }
            this.getSongs();
            this.changeDetectorRef.detectChanges();
          }
        }
      }, error => {

        ImageSource.fromUrl(this.demoImage).then((image) => {
          this.albumImage = image;
          this.showThumb = true;
        })
        setTimeout(() => {
          this.isRendering = true;
          this.changeDetectorRef.detectChanges()
        }, 2)
        console.log(error);
        if (error.error.error == 'unauthorized_user') {
          this.userService.showErrorDialog(true, 'Session timeout. Login again');
          this.userService.loginUpdate(false);
          this.routerExtensions.navigate(['/login']);
        }
      });
  }

  getSongs() {

    this.songs = [];
    this.hasSongs = false;
    this.isLoadingSongs = true;
    console.log('user issdddddddddddddddddddddddd', this.user._id)
    console.log('albunmmm typeeee', this.albumId, this.type)

    this.http.get(Values.BASE_URL + `songs?userId=${this.user._id}&${this.type}=${this.albumId}`, {
      headers: this.headers
    }).subscribe((res: any) => {
      console.log('Songs:::', res)
      if (res != null && res != undefined) {
        if (res.isSuccess == true) {
          if (res.data.songs.length > 0) {

            if (this.isBuy) {

              // if (res.data.songs && res.data.songs[0] && res.data.songs[0].image && res.data.songs[0].image.resize_url) {
              //   // this.albumImage = res.data.songs[0].image.resize_url;
              //   ImageSource.fromUrl(res.data.songs[0].image.resize_url).then((image) => {
              //     this.albumImage = image;
              //   }, error => {
              //     ImageSource.fromResource('music_black').then((image) => {
              //       this.albumImage = image;
              //     })
              //   })
              // } else {
              //   ImageSource.fromResource('music_black').then((image) => {
              //     this.albumImage = image;
              //   })
              // }
            }

            for (var i = 0; i < res.data.songs.length; i++) {
              this.songs.push({
                id: res.data.songs[i].id,
                name: res.data.songs[i].name,
                artist: res.data.songs[i].artistName,
                image: res.data.songs[i].image.resize_url,
                song: { url: res.data.songs[i].song.url },
                isSongPurchase: res.data.songs[i].isSongPurchase,
                price: res.data.songs[i].price,
                views: res.data.songs[i].views
              })
            }
            this.hasSongs = true;

          } else {
            this.hasSongs = false;
          }
          setTimeout(() => {
            this.isRendering = true;
            this.isLoadingSongs = false;
            this.changeDetectorRef.detectChanges()
          }, 2)
          // this.changeDetectorRef.detectChanges();
        }
      }
    }, error => {
      this.hasSongs = false;

      // ImageSource.fromResource('music_black').then((image) => {
      //   this.albumImage = image;
      // })

      setTimeout(() => {
        this.isRendering = true;
        this.isLoadingSongs = false;
        this.changeDetectorRef.detectChanges()
      }, 2)
      console.log(error.error.error);
      if (error.error.error == 'unauthorized_user') {
        this.userService.showErrorDialog(true, 'Session timeout. Login again');
        // alert('Session timeout. Login again');
        this.userService.loginUpdate(false);
        this.routerExtensions.navigate(['/login']);
      }
    });
  }

  getLibrary() {

    this.songs = [];
    this.hasSongs = false;
    this.isLoadingSongs = true;

    this.http.get(Values.BASE_URL + `playlists/${this.albumId}`, {
      headers: this.headers
    }).subscribe((res: any) => {
      if (res != null && res != undefined) {
        console.log('library res', res)
        if (res.isSuccess == true) {
          this.albumName = res.data.name;

          if (res.data.songs.length > 0) {
            if (res.data.songs && res.data.songs[0] && res.data.songs[0].image && res.data.songs[0].image.url) {
              ImageSource.fromUrl(res.data.songs[0].image.url).then((image) => {
                this.albumImage = image;
                this.showThumb = true;
              }, error => {
                ImageSource.fromUrl(this.demoImage).then((image) => {
                  this.albumImage = image;
                  this.showThumb = true;
                })
              })
            } else {
              ImageSource.fromUrl(this.demoImage).then((image) => {
                this.albumImage = image;
                this.showThumb = true;
              })
            }


            for (var i = 0; i < res.data.songs.length; i++) {
              this.songs.push({
                id: res.data.songs[i]._id,
                name: res.data.songs[i].name,
                artist: res.data.songs[i].artistName,
                image: res.data.songs[i].image.resize_url,
                song: { url: res.data.songs[i].song.url },
                views: res.data.songs[i].views,
                isSongPurchase: true
              })
            }
            this.hasSongs = true;

          } else {
            this.isAddButton = true;
            ImageSource.fromUrl(this.demoImage).then((image) => {
              this.albumImage = image;
              this.showThumb = true;
            })
            this.hasSongs = false;
          }
          this.isAddButton = true;

          console.log('Songs Arry', this.songs)
          setTimeout(() => {
            this.isLoadingSongs = false;
            this.isRendering = true;
            this.changeDetectorRef.detectChanges()
          }, 2)

          // this.changeDetectorRef.detectChanges();
        }
      }
    }, error => {
      ImageSource.fromUrl(this.demoImage).then((image) => {
        this.albumImage = image;
        this.showThumb = true;
      })
      console.log(error);
      setTimeout(() => {
        this.isLoadingSongs = false;
        this.isRendering = true;
        this.changeDetectorRef.detectChanges()
      }, 2)
      if (error.error.error == 'unauthorized_user') {
        this.userService.showErrorDialog(true, 'Session timeout. Login again');
        this.userService.loginUpdate(false);
        this.routerExtensions.navigate(['/login']);
      }
    });
    this.hasSongs = false;
  }



  onPlayTap() {
    this.onSong(this.songs[0], 0)
  }



  onBuy() {
    if (this.isLogin) {

      let extendedNavigationExtras: ExtendedNavigationExtras = {
        queryParams: {
          name: this.albumName,
          id: this.albumId,
          price: this.price,
          for: "album"
        }
      };

      this.routerExtensions.navigate(['/orderPayment'], extendedNavigationExtras);
    } else {
      this.routerExtensions.navigate(['/splash'])
    }
  }

  onSong(item: any, i: number) {

    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "id": item.id,
        "index": i,
        "list": JSON.stringify(this.songs)
      }
    }

    this.recentlyPlayedService.setLibraryValue(this.albumId, this.type);
    this.songService.setCurrentPlaylist(this.songs, i);
    this.routerExtensions.navigate(['/player'], extendedNavigationExtras);
  }

  onBack() {
    this.routerExtensions.back();
  }

  onAddSongs() {
    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        "id": this.albumId,
        "from": "album"
      }
    }

    this.recentlyPlayedService.setLibraryValue(this.albumId, this.type);
    this.routerExtensions.navigate(['/add-songs'], extendedNavigationExtras);
  }

  onRemove(item) {


    let model = {
      song: {
        _id: item.id
      },
      isRemoved: "true"
    };

    this.http.put(Values.BASE_URL + "playlists/" + this.albumId, model, {
      headers: this.headers
    })
      .subscribe((res: any) => {
        if (res != null && res != undefined) {
          console.log('res', res)
          if (res.isSuccess == true) {
            this.getLibrary();
            Toast.makeText("Song successfully Removed!!!", "long").show();
          }
        }
      }, error => {
        console.log("ERROR::::", error.error.error);
      });
  }

  buySong() {
    let extendedNavigationExtras: ExtendedNavigationExtras = {
      queryParams: {
        name: this.selectedSong.name,
        id: this.selectedSong.id,
        price: this.selectedSong.price,
        for: 'song'
      }
    };
    this.routerExtensions.navigate(['/orderPayment'], extendedNavigationExtras);
  }

  onCancel() {
    this.buyDialog.hide()
  }

}
