import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { AlbumsComponent } from './components/albums.component';
import { AlbumsRoutingModule } from './albums-routing.module';
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { PlayerModule } from './../shared/player/player.module';
import { NgModalModule } from "../modal/ng-modal";

@NgModule({
  imports: [
    AlbumsRoutingModule,
    FooterModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule,
    PlayerModule,
    NgModalModule
  ],
  declarations: [AlbumsComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AlbumsModule { }
