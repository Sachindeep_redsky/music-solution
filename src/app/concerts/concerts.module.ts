import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { FooterModule } from "../shared/footer/footer.module";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NgModalModule } from "../modal/ng-modal";
import { GridViewModule } from "nativescript-grid-view/angular";
import { PlayerModule } from '../shared/player/player.module';
import { ConcertsRoutingModule } from "./concerts-routing.module";
import { ConcertsComponent } from "./components/concerts.component";
import { ConcertDetailModule } from "../shared/concert-detail/concert-detail.module";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        GridViewModule,
        ConcertsRoutingModule,
        FooterModule,
        NativeScriptUIListViewModule,
        PlayerModule,
        NgModalModule,
        ConcertDetailModule,
    ],
    declarations: [
        ConcertsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ConcertsModule { }
