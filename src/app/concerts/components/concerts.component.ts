import { Component, OnInit, ViewChild } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Values } from "../../values/values";
import { UserService } from "../../services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Page } from "tns-core-modules/ui/page/page";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { SongService } from "../../services/song.service";

@Component({
    selector: "concerts",
    moduleId: module.id,
    templateUrl: "./concerts.component.html",
    styleUrls: ['./concerts.component.css']
})

export class ConcertsComponent implements OnInit {

    // @ViewChild('songBuyDialog', { static: false }) buyDialog: ModalComponent;

    concertsHeading: string;
    concertsList;
    user: any;
    headers: HttpHeaders;
    isRendering: boolean;
    stock_out: string;
    buy_now: string;
    isConcertDetail: boolean;
    concertViewClass: string;
    buyTickets: string;

    constructor(private songService: SongService, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient, private page: Page) {


        this.isRendering = false;

        if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

            this.headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
            });

            this.user = JSON.parse(Values.readString(Values.USER, ''));
            this.isConcertDetail = false;
            this.concertsList = [];
            this.getConcerts();
        }
    }

    ngOnInit(): void {

        let list;
        let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
        let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');

        this.page.actionBarHidden = true;
        this.userService.activeScreen("concerts");
        this.userService.showHideFooter(true);

        if (stringifiedList) {
            list = JSON.parse(stringifiedList);
        }

        if (index != undefined && index != null) {
            if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
                this.userService.showHidePlayer(true);
            } else {
                this.userService.showHidePlayer(false)
            }
        } else {
            this.userService.showHidePlayer(false)
        }

        this.concertsHeading = "Concerts";
        this.stock_out = "Stock out";
        this.buy_now = "Buy now";
        this.buyTickets = "Buy tickets";

        this.userService.showConcert.subscribe((state: boolean) => {
            if (state != undefined && state != null) {
                this.concertViewClass = "view-in";
                setTimeout(() => {
                    this.isConcertDetail = state;
                }, 1000);
            }
        });
    }

    getConcerts() {
        this.http.get(Values.BASE_URL + `events`, {
            headers: this.headers
        }).subscribe((res: any) => {
            if (res != null && res != undefined) {
                if (res.isSuccess == true) {
                    console.trace("events rec", res)
                    if (res.data.length > 0) {
                        for (var i = 0; i < res.data.length; i++) {
                            var inStock: boolean;
                            if (res.data[i].remainingTickets == "0") {
                                inStock = false;
                            }
                            else {
                                inStock = true;
                            }
                            this.concertsList.push({
                                id: res.data[i]._id,
                                title: res.data[i].title,
                                artist: res.data[i].artistName,
                                image: res.data[i].image.resize_url,
                                description: res.data[i].description,
                                price: res.data[i].price,
                                date: res.data[i].dateAndTime,
                                address: res.data[i].address,
                                remainingTickets: res.data[i].remainingTickets,
                                inStock: inStock
                            })
                        }
                        this.isRendering = true;
                    }
                }
            }
        }, error => {
            console.log(error.error.error);
            this.isRendering = false;
            if (error.error.error == 'unauthorized_user') {
                this.userService.showErrorDialog(true, 'Session timeout. Login again');
                this.userService.loginUpdate(false);
                this.routerExtensions.navigate(['/login']);
            }
        });
    }

    onEvent(item: any) {
        this.concertViewClass = "view-out";
        this.isConcertDetail = true;
        this.userService.showConcertData(item);
    }

}