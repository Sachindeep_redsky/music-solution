/// <reference path="../../../node_modules/tns-platform-declarations/android-29.d.ts" />

export class NotificationInstance {

    private static notification_builder: androidx.core.app.NotificationCompat.Builder = null
    private static notification_manager: android.app.NotificationManager = null
    private static pending_intents: { previous: android.app.PendingIntent, toggle: android.app.PendingIntent, next: android.app.PendingIntent } = null
    public static NotificationServiceRef = null;
    public static SongServiceRef = null;

    public static setNotificationBuilder(builder: androidx.core.app.NotificationCompat.Builder) {
        NotificationInstance.notification_builder = builder;
    }

    public static getNotificationBuilder(): androidx.core.app.NotificationCompat.Builder {
        return NotificationInstance.notification_builder;
    }

    public static setNotificationManager(manager: android.app.NotificationManager) {
        NotificationInstance.notification_manager = manager;
    }

    public static getNotificationManager(): android.app.NotificationManager {
        return NotificationInstance.notification_manager;
    }

    public static setPendingIntents(previous: android.app.PendingIntent, toggle: android.app.PendingIntent, next: android.app.PendingIntent) {
        NotificationInstance.pending_intents = { previous: previous, toggle: toggle, next: next }
    }

    public static getPendingIntents() {
        return NotificationInstance.pending_intents;
    }

    public static previous() {
        console.log('previousLLLLLL');
        NotificationInstance.SongServiceRef.previous();
    }

    public static toggle() {
        console.log('toggleeeeeeeeeee');
        NotificationInstance.SongServiceRef.toggle();
    }

    public static next() {
        console.log('nextttttt');
        NotificationInstance.SongServiceRef.next();
    }

}