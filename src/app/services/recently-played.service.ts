import { Injectable } from "@angular/core";
import { Subject } from "rxjs/internal/Subject";
import { Values } from "../values/values";
import { SongService } from "./song.service";
import { HttpHeaders, HttpClient } from "@angular/common/http";

@Injectable()
export class RecentlyPlayedService {

    private _recentlyChanged = new Subject<boolean>();
    public libraryId: string;
    public libraryFrom: string;
    user: any;
    headers: HttpHeaders;
    recentlyChanged = this._recentlyChanged.asObservable();

    constructor(private http: HttpClient) {

        this.checkSongExists();
    }

    setHistory(value: any) {

        let list = []
        let data = Values.readString(Values.RECENTLY_PLAYED, '');

        if (data) {
            list = JSON.parse(data);
            setTimeout(() => {
                for (let i = 0; i < list.length; i++) {
                    if (list[i].id == value.id) {
                        list.splice(i, 1)
                    }
                }
                list.unshift(value);
                if (list.length > 20) {
                    list.pop();
                }

            }, 1)
        } else {
            list.unshift(value);
        }
        setTimeout(() => {
            Values.writeString(Values.RECENTLY_PLAYED, JSON.stringify(list));
        }, 2)
    }

    getHistory() {
        let list = Values.readString(Values.RECENTLY_PLAYED, '');
        console.log('List:::', list)
        if (list) {
            return JSON.parse(list);
        }
    }

    setLibraryValue(id: string, from: string) {
        this.libraryFrom = from;
        this.libraryId = id;
    }

    reset() {
        this.libraryFrom = null;
        this.libraryId = null;
        Values.writeString(Values.RECENTLY_PLAYED, '');
    }

    checkSongExists(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let songs = [];
            let _list = Values.readString(Values.RECENTLY_PLAYED, '');
            // console.log('List:::', list)
            if (_list) {
                var list = JSON.parse(_list);
                list.forEach(song => {
                    console.log("recently list", song)
                    songs.push({
                        "id": song.id
                    })
                });

                if (songs != []) {
                    if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {
                        this.headers = new HttpHeaders({
                            'Content-Type': 'application/json',
                            'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
                        });
                    }
                    let _songs = JSON.stringify(songs);
                    this.http.get(Values.BASE_URL + `songs/isExists?id=${_songs}`, {
                        headers: this.headers
                    }).subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            console.log('songs exitsststststtstststtststststtsstststststt', res)
                            if (res.isSuccess == true) {
                                if (res.data && res.data != null && res.data != undefined) {
                                    if (res.data.songs && res.data.songs.length != 0) {
                                        for (let item of res.data.songs) {
                                            if (item) {
                                                if (item.doseExists == false) {
                                                    let i
                                                    let isExists = list.some((el, index) => {
                                                        if (el.id === item.id) {
                                                            i = index
                                                            return true
                                                        } else {
                                                            return false
                                                        }
                                                    })
                                                    console.log('isExists:', isExists)
                                                    if (isExists) {
                                                        list.splice(i, 1);
                                                        console.log('listttttttttt:', list)
                                                        setTimeout(() => {
                                                            Values.writeString(Values.RECENTLY_PLAYED, JSON.stringify(list));

                                                        }, 2)
                                                    }
                                                }
                                                resolve(true);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }, error => {
                        console.log('recently error', error);
                        resolve(false)
                    });
                }
            } else {
                resolve(false)
            }

        })
    }

}