import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import * as connectivity from "connectivity"
import { Values } from '../values/values';

@Injectable()
export class ConnectionService {

    private _connectionSubject;
    private _connectionTextSubject;
    private _pendingDataSubject;

    pendingDataChanges;
    connectionChanges;
    connectionTextChanges;
    private interval: any;
    isConnected: boolean;

    constructor() {
        this._connectionSubject = new Subject<boolean>();
        this._connectionTextSubject = new Subject<string>();
        this._pendingDataSubject = new Subject<boolean>();

        this.pendingDataChanges = this._pendingDataSubject.asObservable();
        this.connectionChanges = this._connectionSubject.asObservable();
        this.connectionTextChanges = this._connectionTextSubject.asObservable();

        this.interval = setInterval(() => {
            this.monitorConnection();
        }, 1000);
        setTimeout(() => {
            this.interval = setInterval(() => {
                this.stopMoniterConnection();

            }, 1000);
        }, 200)

        this.connectionChanges.subscribe((state: boolean) => {
            if (state) {
                this.isConnected = true;
                // console.log("Connnn")
            } else {
                this.isConnected = false;
                // console.log("Con no")
            }
        })
    }

    public monitorConnection() {
        connectivity.startMonitoring((newConnectionType) => {
            switch (newConnectionType) {
                case connectivity.connectionType.none:
                    this._connectionSubject.next(false);
                    break;

                case connectivity.connectionType.wifi:
                    this._connectionSubject.next(true);
                    break;

                case connectivity.connectionType.mobile:
                    this._connectionSubject.next(true);
                    break;
            }
        })
    }


    public stopMoniterConnection() {
        connectivity.stopMonitoring();

    }
    public connectionTextSetter(text: string) {
        this._connectionTextSubject.next(text);
    }

}