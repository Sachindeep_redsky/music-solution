import { isAndroid } from 'platform';
import { ImageSource } from 'tns-core-modules/image-source/image-source';
import { RouterExtensions } from 'nativescript-angular/router';
import { fromResource } from 'tns-core-modules/image-source/image-source';
import { Injectable } from "@angular/core";
import { TNSPlayer, IAudioPlayerEvents } from "nativescript-audio";
import { Subject } from "rxjs/internal/Subject";
import { fromNativeSource } from "tns-core-modules/image-source";
import { Values } from "../values/values";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RecentlyPlayedService } from './recently-played.service';
import { UserService } from './user.service';
import * as fs from "tns-core-modules/file-system";
import * as Toast from 'nativescript-toast';
import { Downloader, DownloadOptions } from 'nativescript-downloader';
declare const android: any;
const downloader = new Downloader();

@Injectable()
export class SongService {

    player: TNSPlayer;
    songList;

    private _playerStateSubject = new Subject<boolean>();
    private _currentPlaylistChange = new Subject<any>();
    private _songCompleteEvent = new Subject<boolean>();
    private _currentSongEvent = new Subject<any>();
    private _songParamChangeEvent = new Subject<any>();
    private _songProgressChangeEvent = new Subject<any>();
    private _songLoadingEvent = new Subject<boolean>();
    private _repeatChangeEvent = new Subject<boolean>();
    private _bufferingEvent = new Subject<boolean>();
    private _purchaseEvent = new Subject<boolean>();

    playerStateChanges = this._playerStateSubject.asObservable();
    currentPlaylistChange = this._currentPlaylistChange.asObservable();
    songCompleteEvent = this._songCompleteEvent.asObservable();
    currentSongChanges = this._currentSongEvent.asObservable();
    songParamChanges = this._songParamChangeEvent.asObservable();
    songProgressChanges = this._songProgressChangeEvent.asObservable();
    songLoadingChanges = this._songLoadingEvent.asObservable();
    repeatStateChanges = this._repeatChangeEvent.asObservable();
    bufferingEvent = this._bufferingEvent.asObservable();
    purchaseEvent = this._purchaseEvent.asObservable();

    currentPlaylist: any;
    currentIndex: number;
    songParams: any;
    timeoutInterval: any;
    isRepeat: boolean;
    isPlayerPlaying: boolean;
    songService: any;
    isBuffering: boolean;
    isSongLoading: boolean;
    isSongPurchased: any;
    isOfflineSong: boolean;
    appPath: string;
    isPurchasedDialog: boolean;
    user: any;

    constructor(private http: HttpClient, private recentlyPlayedService: RecentlyPlayedService, private userService: UserService, private routerExtensions: RouterExtensions) {
        if (isAndroid) {
            this.player = new TNSPlayer(this);
        }
        else {
            this.player = new (TNSPlayer as any)();
        }

        this.player.debug = false;
        var pl = this.player;
        this.isRepeat = false;
        this.isPlayerPlaying = false;
        this.isBuffering = false;
        this.isSongPurchased = false;
        this.isOfflineSong = false;
        this.isPurchasedDialog = false;
        this.songLoadingChanges.subscribe(state => {
            this.isSongLoading = state;
        })
        let documents = fs.knownFolders.currentApp();
        console.log('DOC::::', documents)
        this.appPath = documents.getFolder("MusicSolution").path;
        console.log('AppPath::::', this.appPath)
        Downloader.init();

        // (this.player as any).setBufferCallback(this.onBuffer)
    }



    async setSongIndex(index: number, shouldPlay: boolean) {

        let that = this;
        console.log('reacted set index:::', index)
        this.isPurchasedDialog = false;
        if (this.userService && this.userService)

            if (this.songList && this.songList[index]) {
                var song = this.songList[index];
                console.log('Song:::', song)
                let _localSong = <any>await this.checkDownloadedSong(song.name)
                console.log('_localSong:::', _localSong)
                if (_localSong.isSongMatch && _localSong.path) {
                    song.song.url = _localSong.path
                }
                console.log('Song after change:::', song)
                Values.writeNumber(Values.CURRENT_SONG_INDEX, index);
                if (this.songParams && this.songParams.id) {
                    if (!(song.id == this.songParams.id && song.index == this.songParams.index)) {
                        if (song.image && song.image == "offline") {
                            this._songLoadingEvent.next(true);
                            this.isOfflineSong = true;
                            this.getImage(song.song.url).then(image => {
                                this.setSongParams(song, index, image, undefined, song.id, true);
                                this.playSong(shouldPlay, song.song.url, index);
                                console.log("Imageeee:::", image)
                            }, error => {
                                console.log(error)
                                this.setSongParams(song, index, fromResource('music_black'), undefined, song.id, true);
                                this.playSong(shouldPlay, song.song.url, index);
                            })
                        } else if (song.image && song.image.indexOf("http://") != -1) {
                            console.log("ISONLINE")
                            this._songLoadingEvent.next(true);
                            this.isOfflineSong = false;
                            this.isSongPurchased = song.isSongPurchase
                            this.getSong(index, song.id).then(extras => {
                                console.log("GOT_EXTRAS")

                                ImageSource.fromUrl(song.image).then(image => {
                                    console.log("GOT_Image")
                                    this.setSongParams(song, index, image, extras, song.id, false);
                                    this.playSong(shouldPlay, song.song.url, index);
                                }, error => {
                                    this.setSongParams(song, index, fromResource('music_black'), extras, song.id, false);
                                    this.playSong(shouldPlay, song.song.url, index);
                                })
                            }, error => {
                                console.log('Error getting song:::', error);
                                console.log("Got no song:")

                                ImageSource.fromUrl(song.image).then(image => {
                                    console.log("Got image:", image)

                                    this.setSongParams(song, index, image, undefined, song.id, false);
                                    this.playSong(shouldPlay, song.song.url, index);
                                }, error => {
                                    console.log("no image:")
                                    this.setSongParams(song, index, fromResource('music_black'), undefined, song.id, false);
                                    this.playSong(shouldPlay, song.song.url, index);
                                })
                            })

                        } else {
                            console.log("got nothing:")
                            this._songLoadingEvent.next(true);

                            this.setSongParams(song, index, fromResource('music_black'), undefined, song.id, false);
                            this.playSong(shouldPlay, song.song.url, index);
                        }
                    }
                } else {
                    if (song.image && song.image == "offline") {
                        this._songLoadingEvent.next(true);
                        this.isOfflineSong = true;
                        this.getImage(song.song.url).then(image => {
                            this.setSongParams(song, index, image, undefined, song.id, true);
                            this.playSong(shouldPlay, song.song.url, index);
                        }, error => {
                            this.setSongParams(song, index, fromResource('music_black'), undefined, song.id, true);
                            this.playSong(shouldPlay, song.song.url, index);
                        })
                    } else if (song.image && song.image.indexOf("http://") != -1) {
                        console.log("ISONLINE")
                        this._songLoadingEvent.next(true);
                        this.isOfflineSong = false;
                        this.isSongPurchased = song.isSongPurchase;
                        this.getSong(index, song.id).then(extras => {
                            console.log("GOT_EXTRAS")

                            ImageSource.fromUrl(song.image).then(image => {
                                console.log("GOT_Image")
                                this.setSongParams(song, index, image, extras, song.id, false);
                                this.playSong(shouldPlay, song.song.url, index);
                            }, error => {
                                this.setSongParams(song, index, fromResource('music_black'), extras, song.id, false);
                                this.playSong(shouldPlay, song.song.url, index);
                            })
                        }, error => {
                            console.log('Error getting song:::', error);
                            console.log("Got no song:")

                            ImageSource.fromUrl(song.image).then(image => {
                                console.log("Got image:", image)

                                this.setSongParams(song, index, image, undefined, song.id, false);
                                this.playSong(shouldPlay, song.song.url, index);
                            }, error => {
                                console.log("no image:")
                                this.setSongParams(song, index, fromResource('music_black'), undefined, song.id, false);
                                this.playSong(shouldPlay, song.song.url, index);
                            })
                        })

                    } else {
                        console.log("got nothing:")
                        this._songLoadingEvent.next(true);

                        this.setSongParams(song, index, fromResource('music_black'), undefined, song.id, false);
                        this.playSong(shouldPlay, song.song.url, index);
                    }
                }
            } else {
                this.userService.showHidePlayer(false);
            }
    }

    playSong(shouldPlay, url, index) {
        this.isPlayerPlaying = true;
        var that = this;

        if (shouldPlay) {
            if (this.player.isAudioPlaying()) {
                this.pause();
            }

            clearInterval(that.timeoutInterval);

            var isRepeated = Values.readBoolean(Values.IS_REPEATED, false);

            const playerOptions = {
                audioFile: url,
                loop: isRepeated,
                autoplay: true,
                completeCallback: () => {
                }
            };

            setTimeout(() => {
                this.player
                    .playFromUrl(playerOptions)
                    .then(function (res) {
                        setTimeout(() => {
                            that.playerState(true);
                            that._currentSongEvent.next(that.songParams);
                            that._songLoadingEvent.next(false);
                            that.timeoutInterval = setInterval(() => {
                                that.getTime();
                            }, 1000);
                        }, 1)
                    })
                    .catch(function (err) {
                        console.log('something went wrong...', err);
                    });
            }, 2)
        } else {

            clearInterval(that.timeoutInterval);

            var isRepeated = Values.readBoolean(Values.IS_REPEATED, false);

            const playerOptions = {
                audioFile: url,
                loop: isRepeated,
                autoplay: true,
                completeCallback: () => {
                    if (this.isRepeat) {
                        this.setSongIndex(index, true)
                    } else {
                        this.next();
                    }
                }
            };

            setTimeout(() => {

                this.player
                    .playFromUrl(playerOptions)
                    .then(function (res) {
                        setTimeout(() => {
                            that.playerState(true);
                            that._currentSongEvent.next(that.songParams);
                            setTimeout(() => {
                                that.player.pause().then(() => {
                                    that.playerState(false);
                                    that._currentSongEvent.next(that.songParams);
                                    that._songLoadingEvent.next(false);
                                    that.timeoutInterval = setInterval(() => {
                                        that.getTime();
                                    }, 1000);
                                }, error => {
                                    console.log('Error:::', error)
                                })
                            }, 1)
                        }, 1)
                    })
                    .catch(function (err) {
                        console.log('something went wrong...', err);
                    });
            }, 2);

        }
    }

    private setSongParams(song, index, image, extras, id, isOffline) {

        var songParams = { name: '', artist: '', album: '', thumb: fromResource('music_black'), index: -1, isLiked: undefined, likeId: undefined, id: undefined, isOffline: undefined }
        songParams.name = song.name;
        songParams.artist = song.artist;
        songParams.album = song.album;
        songParams.thumb = image;
        songParams.index = index;
        songParams.id = id;
        songParams.isOffline = isOffline;
        if (extras) {
            songParams.isLiked = extras.isLiked;
            songParams.likeId = extras.likeId;
            songParams.album = extras.album;
        }
        this.songParams = songParams;
        // console.log("setParams:", this.songParams);
        // this._songLoadingEvent.next(false);
        this._songParamChangeEvent.next(songParams);
    }

    getPlayer() {
        return this.player;
    }

    playerState(state: boolean) {
        this._playerStateSubject.next(state)
    }

    setSongList(list) {
        this.songList = list;
        if (list) {
            Values.writeString(Values.CURRENT_SONG_LIST, JSON.stringify(list));
        } else {
            Values.writeString(Values.CURRENT_SONG_LIST, '');
        }
    }

    next() {

        if (this.songList.length > 0) {
            if (this.isRepeat) {
                this.setSongIndex(this.songParams.index, true);
            } else {
                if (Values.IS_SHUFFLE) {
                    this.setSongIndex(this.randomInteger(0, this.songList.length - 1), true)
                }
                else {
                    if (this.songParams.index + 1 <= (this.songList.length - 1)) {
                        this.setSongIndex(this.songParams.index + 1, true);
                    } else {
                        this.setSongIndex(0, true);
                    }
                }
            }
        }
    }

    previous() {

        if (this.songList.length > 0) {
            if (this.isRepeat) {
                this.setSongIndex(this.songParams.index, true);
            }
            else {
                if (Values.IS_SHUFFLE) {
                    this.setSongIndex(this.randomInteger(0, this.songList.length - 1), true)
                } else {
                    if (this.songParams.index - 1 >= 0) {
                        this.setSongIndex(this.songParams.index - 1, true);
                    } else {
                        this.setSongIndex(this.songList.length - 1, true);
                    }
                }
            }
        }
    }

    toggle() {
        if (this.player.isAudioPlaying()) {
            this.player.pause().then(() => {
                this.playerState(false);
                clearInterval(this.timeoutInterval);
            })
        } else {
            if (this.isPurchasedDialog) {
                this._purchaseEvent.next(true);
                return
            }
            var song = this.songList[this.songParams.index];
            // console.log("SONGEEEEEEEEEEEEEEEEEEEEEEEEE", song);
            this.getSong(this.songParams.index, song.id);
            this.player.play().then(() => {
                this.timeoutInterval = setInterval(() => {
                    // console.log('song played')
                    this.getTime();
                }, 1000);
                this.playerState(true);
            })
        }
    }

    // play() {
    //     this.player.play().then(() => {
    //         this.playerState(true);
    //     })
    // }

    pause() {
        this.player.pause().then(() => {
            this.playerState(false);
        })
    }

    getImage(data): Promise<any> {

        return new Promise((resolve, reject) => {
            try {
                const mmr = new android.media.MediaMetadataRetriever();
                mmr.setDataSource(data);

                let bfo = new android.graphics.BitmapFactory.Options()
                let rawArt = mmr.getEmbeddedPicture();

                if (rawArt != null && rawArt != undefined) {
                    let art = android.graphics.BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);
                    let image = fromNativeSource(art);

                    resolve(image)
                } else {
                    reject(false)
                }

            } catch (error) {
                console.log('Error:', error)
                var thumb = fromResource("music_black");
                resolve(thumb)
            }
        })
    }

    getSong(index: number, id): Promise<any> {

        return new Promise((resolve, reject) => {
            let _user = Values.readString(Values.USER, '');
            var user = { _id: "" }
            if (_user && _user != '') {
                user = JSON.parse(_user);
            }

            var headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
            });

            // console.log('Song Id:::', id, "User ID:::", user._id)

            this.http.get(Values.BASE_URL + `songs/${id}?userId=${user._id}`, {
                headers: headers
            })
                .subscribe((res: any) => {

                    if (res != null && res != undefined) {
                        console.log('SONGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG', res.data)
                        if (res.isSuccess) {
                            this.isSongPurchased = res.data.isPurchase;
                            this.recentlyPlayedService.setHistory(res.data);
                            Values.writeString(Values.CURRENT_SONG, JSON.stringify(res.data));
                            var extras = { isLiked: undefined, likeId: undefined, album: undefined }
                            if (res.data.isLiked == 'true') {
                                extras.isLiked = true
                                extras.likeId = res.data.likeId
                            } else {
                                extras.isLiked = false;
                            }
                            if (res.data.songType == 'isSingle') {
                                extras.album = "Single Track"
                            } else {
                                extras.album = res.data.albumName.charAt(0).toUpperCase() + res.data.albumName.slice(1);
                            }

                            resolve(extras);
                        }
                    }
                }, error => {
                    console.log(error);
                    if (error.error.error == 'unauthorized_user') {
                        this.userService.showErrorDialog(true, 'Session timeout. Login again');
                        this.userService.loginUpdate(false);
                        this.routerExtensions.navigate(['/login']);
                    } else {
                        reject(error)
                    }
                });
        })

    }

    getTime() {
        var current = 0;
        let remaining: number = 0;
        let progress: number = 0;
        // var user = JSON.parse(Values.readString(Values.USER, ''));
        var currentSec
        var currentMin
        var currentHour

        var remainingSec
        var remainingMin
        var remainingHour


        var remainingTime
        var passedTime;

        current = this.getPlayer().currentTime;
        this.getPlayer().getAudioTrackDuration().then((durationResult: string) => {
            let duration = parseInt(durationResult, 10)
            // console.log("DURATIONNNNNNNNNNNNNNNNNNNN::::::", duration);
            if (duration != NaN && duration != undefined && duration != 0) {
                if (current != NaN && current != undefined) {
                    // console.log('Duration:::', duration, 'curr:::', current)
                    if (duration > 0 && current > 0) {
                        if (Math.floor(duration / 1000) == Math.floor(current / 1000)) {
                            this.songCompleted();
                        }
                    }
                    progress = (current / duration) * 100;
                    remaining = duration - current;

                    currentSec = this.getSeconds(current);
                    currentMin = this.getMinutes(current);
                    currentHour = this.getHours(current);

                    if (!this.isOfflineSong) {
                        // if (this.isSongPurchased != 'true' || this.user.subscriptionPlan == "") {
                        // console.log("SONG PURCHASED::::", this.isSongPurchased);
                        // console.log("HAS SUBSCRIPTION::::",this.user.hasSubscription);
                        // if (this.isSongPurchased != 'true') {
                        //     if (this.player.isAudioPlaying()) {
                        //         if (current >= 30000) {
                        //             this.pause();
                        //             clearInterval(this.timeoutInterval)
                        //             this._purchaseEvent.next(true)
                        //             this.isPurchasedDialog = true;
                        //         }
                        //     }
                        // }
                        // else {
                        this._purchaseEvent.next(false);
                        // }
                    }

                    if (remaining >= 0) {
                        remainingSec = this.getSeconds(remaining)
                        remainingMin = this.getMinutes(remaining)
                        remainingHour = this.getHours(remaining)
                    } else {
                        remainingSec = 0;
                        remainingMin = 0;
                        remainingHour = 0;
                    }

                    var remSec;
                    var remMin;
                    var remHour;

                    var pasSec;
                    var pasMin;
                    var pasHour;

                    if (remainingSec < 10) {
                        remSec = "0" + remainingSec;
                    }
                    else {
                        remSec = remainingSec;
                    }



                    if (remainingMin < 10) {
                        remMin = "0" + remainingMin;
                    }
                    else {
                        remMin = remainingMin;
                    }

                    if (remainingHour < 10) {
                        remHour = "0" + remainingHour;
                    }
                    else {
                        remHour = remainingHour;
                    }

                    remainingTime = remHour + ":" + remMin + ":" + remSec + "";

                    if (currentSec < 10) {
                        pasSec = "0" + currentSec;
                    }
                    else {
                        pasSec = currentSec;
                    }

                    if (currentMin < 10) {
                        pasMin = "0" + currentMin;
                    }
                    else {
                        pasMin = currentMin;
                    }

                    if (currentHour < 10) {
                        pasHour = "0" + currentHour;
                    }
                    else {
                        pasHour = currentHour;
                    }

                    passedTime = pasHour + ":" + pasMin + ":" + pasSec + "";
                }
            }
            else {
                progress = 0;
                remainingTime = '00' + ":" + '00' + ":" + '00' + "";
                passedTime = '00' + ":" + '00' + ":" + '00' + "";
            }

            this._songProgressChangeEvent.next({ progress: progress, remainingTime: remainingTime, passedTime: passedTime, duration: duration })
        });
    }

    getSeconds(duration: number) {
        return Math.floor((duration / 1000) % 60);
    }

    getMinutes(duration: number) {
        return Math.floor((duration / 1000 / 60) % 60);
    }

    getHours(duration: number) {
        return Math.floor((duration / 1000) / 3600);
    }

    setCurrentPlaylist(state: any, index: number) {
        this.currentPlaylist = state;
        this.currentIndex = index;
        this._currentPlaylistChange.next(state);
    }

    songCompleted() {
        // if (this.isRepeat) {
        //     this.setSongIndex(this, true)
        // } else {
        //     this.next();
        // }
        console.log('Called complete')
        this.next();

        // this._songCompleteEvent.next(true);
    }

    randomInteger(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    onBuffer(isBuffering: boolean, percentage: number) {
        // console.log('Buffering with args', isBuffering, percentage);
        this.songService._bufferingEvent.next(isBuffering);
        this.songService.isBuffering = isBuffering;
    }

    async downloadSong(item: any) {
        return new Promise(async (resolve, reject) => {

            let _localSong = <any>await this.checkDownloadedSong(item.name);
            let isSongMatch = _localSong.isSongMatch
            try {


                if (!isSongMatch) {
                    let songLocalPath = fs.Folder.fromPath(this.appPath).path
                    var downloaderId = downloader.createDownload({
                        url: item.song.url,
                        path: songLocalPath,
                    });

                    downloader
                        .start(downloaderId, progressData => {
                            console.log(`Progress : ${progressData.value}%`);
                        })
                        .then(completed => {
                            console.log(`Syllabus downloaded successfully at path : ${completed.path}`);
                            if (completed.path) {
                                var file = fs.File.fromPath(completed.path);
                                if (file) {

                                    file.renameSync(item.name + '.mp3', error => {
                                        console.log('Error renaming:::', error);
                                    })
                                    Toast.makeText('Song saved successfully', 'long').show()
                                    resolve(true)
                                }
                            }
                        })
                        .catch(error => {
                            console.log(error.message);
                        });
                } else {
                    Toast.makeText("Song already exists in Downloaded list", "long").show();
                    resolve(true)
                }

            } catch (error) {
                console.log(error)
            }
        })
    }
    checkDownloadedSong(song: string) {
        return new Promise((resolve, reject) => {
            let documents = fs.knownFolders.currentApp();
            let appPath = documents.getFolder("MusicSolution").path;
            let isSongMatch = false;
            let path = "";

            try {

                const folder = fs.Folder.fromPath(appPath);
                folder.getEntities()
                    .then((entities) => {
                        // entities is an array of files and folders.
                        entities.forEach((entity) => {
                            if (entity.name == song + '.mp3') {
                                isSongMatch = true;
                                path = entity.path


                            }
                            //  entity.path
                        });
                        resolve({ isSongMatch: isSongMatch, path: path })
                    })
            } catch {
                resolve({ isSongMatch: isSongMatch, path: path })

            }
        })
    }
}