/// <reference path="../../../node_modules/tns-platform-declarations/android-29.d.ts" />

import { ad } from "utils/utils"
import { Injectable } from "@angular/core";
import { NotificationInstance } from './notification.instance';
import { SongService } from "./song.service";

declare var com: any;
declare var cmom: any;

@Injectable()

export class NotificationService {


    INTENT_ACTION_NEXT = 'ACTION_NEXT';
    INTENT_ACTION_PREVIOUS = 'ACTION_PREVIOUS';
    INTENT_ACTION_TOGGLE = 'ACTION_TOGGLE';

    toogleState: boolean;
    notificationManager: any;

    constructor(private songService: SongService) {

        this.toogleState = true;

        this.songService.currentSongChanges.subscribe((params => {
            this.postNotification()
        }));

        this.songService.playerStateChanges.subscribe((state => {
            this.toogleState = state;
            this.postNotification()
        }))

        this.songService.songParamChanges.subscribe((params => {
            this.postNotification()
        }));

    }

    postNotification() {

        var ongoing;
        var toggleIcon;

        var context = ad.getApplicationContext();
        var previousIcon = context.getResources().getIdentifier("previous_notification", "drawable", context.getPackageName())
        var nextIcon = context.getResources().getIdentifier("next_notification", "drawable", context.getPackageName())

        if (this.toogleState) {
            toggleIcon = context.getResources().getIdentifier("pause_notification", "drawable", context.getPackageName());
            ongoing = true;
        } else {
            toggleIcon = context.getResources().getIdentifier("play_notification", "drawable", context.getPackageName());
            ongoing = false;
        }

        if (NotificationInstance.getNotificationBuilder() && NotificationInstance.getNotificationManager() && NotificationInstance.getPendingIntents()) {

            var notificationBuilder = NotificationInstance.getNotificationBuilder();
            var notificationManager = NotificationInstance.getNotificationManager();
            var pendingIntents = NotificationInstance.getPendingIntents();

            try {
                if (this.songService && this.songService.songParams) {
                    var notification = notificationBuilder.
                        setContentTitle(this.songService.songParams.name)
                        .setContentText(this.songService.songParams.artist)
                        .setLargeIcon(this.songService.songParams.thumb.android)
                        .setOngoing(ongoing)
                        .addAction(previousIcon, "", pendingIntents.previous) // #0
                        .addAction(toggleIcon, "", pendingIntents.toggle)  // #1
                        .addAction(nextIcon, "", pendingIntents.next)     // #2
                        .build();

                    NotificationInstance.setNotificationBuilder(notificationBuilder);
                    NotificationInstance.setNotificationManager(notificationManager);

                    notificationManager.notify(599, notification);
                }

            } catch (error) {
                console.log('Error Notifing', error)
            }
        } else

            var mediaSessionCompat = new android.support.v4.media.session.MediaSessionCompat(context, "tag")
        var notificationBuilder = new androidx.core.app.NotificationCompat.Builder(context, "MUSIC_CHANNEL");
        var notificationManager = <android.app.NotificationManager>context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= 26) {
            var importance = android.app.NotificationManager.IMPORTANCE_LOW;
            var channel = new android.app.NotificationChannel('MUSIC_CHANNEL', 'Music_Channel', importance);
            notificationManager.createNotificationChannel(channel);
        }

        var mainIntent = new android.content.Intent(context, com.tns.NativeScriptActivity.class);
        var previousButtonIntent = new android.content.Intent(context, cmom.tns.NotificationActionHandler.class);
        var toggleButtonIntent = new android.content.Intent(context, cmom.tns.NotificationActionHandler.class);
        var nextButtonIntent = new android.content.Intent(context, cmom.tns.NotificationActionHandler.class);

        previousButtonIntent.setAction('ACTION_PREVIOUS');
        toggleButtonIntent.setAction('ACTION_TOGGLE')
        nextButtonIntent.setAction('ACTION_NEXT')

        var pendingIntent = android.app.PendingIntent.getActivity(context, 11, mainIntent, android.app.PendingIntent.FLAG_UPDATE_CURRENT);
        var previousButtonPendingIntent = android.app.PendingIntent.getBroadcast(context, 0, previousButtonIntent, android.app.PendingIntent.FLAG_UPDATE_CURRENT);
        var toggleButtonPendingIntent = android.app.PendingIntent.getBroadcast(context, 1, toggleButtonIntent, android.app.PendingIntent.FLAG_UPDATE_CURRENT);
        var nextButtonPendingIntent = android.app.PendingIntent.getBroadcast(context, 2, nextButtonIntent, android.app.PendingIntent.FLAG_UPDATE_CURRENT);

        try {
            if (this.songService && this.songService.songParams) {

                var notification = notificationBuilder
                    .setContentTitle(this.songService.songParams.name)
                    .setContentText(this.songService.songParams.artist)
                    .setSmallIcon(context.getResources().getIdentifier("logo",
                        "drawable", context.getPackageName()))
                    .setLargeIcon(this.songService.songParams.thumb.android)
                    .setOnlyAlertOnce(true)
                    .setShowWhen(false)
                    .setOngoing(ongoing)
                    .setContentIntent(pendingIntent)
                    .addAction(previousIcon, "", previousButtonPendingIntent) // #0
                    .addAction(toggleIcon, "", toggleButtonPendingIntent)  // #1
                    .addAction(nextIcon, "", nextButtonPendingIntent)     // #2
                    .setStyle(new androidx.media.app.NotificationCompat.MediaStyle().setMediaSession(mediaSessionCompat.getSessionToken()))
                    .build();

                notificationManager.notify(599, notification);
            }
        }
        catch (e) {
            console.log('Error notifing:::', e)
        }
    }

    cancel() {
        if (NotificationInstance.getNotificationBuilder() && NotificationInstance.getNotificationManager() && NotificationInstance.getPendingIntents()) {
            try {
                var notificationManager = NotificationInstance.getNotificationManager();
                notificationManager.cancel(599);
                console.log('Cancelling')
            } catch (e) {
                console.log('Cancelling Error:', e)
            }
        } else {
            var context = ad.getApplicationContext();
            var notificationManager = <android.app.NotificationManager>context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(599);
        }
    }

    

}