import { Injectable } from '@angular/core';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { Subject } from 'rxjs/internal/Subject';

import * as connectivity from "connectivity";

@Injectable()
export class UserService {

    public isConnected: boolean;
    private _activescreen = new Subject<string>();
    private _networkSubject = new Subject<boolean>();
    private _userSubject = new Subject<any>();
    private _homeSubject = new Subject<boolean>();
    private _homeUpdationSubject = new Subject<any>();
    private _actionBarState = new Subject<boolean>();
    private _actionBarText = new Subject<string>();
    private _playerButton = new Subject<boolean>();
    private _actionBarSearch = new Subject<boolean>();
    private _drawerSwipeState = new Subject<boolean>();
    private _isLogin = new Subject<boolean>();
    private _showPlayer = new Subject<boolean>();
    private _showFooter = new Subject<boolean>();
    private _showError = new Subject<boolean>();
    private _showConcert = new Subject<boolean>();
    private _concertData = new Subject<boolean>();

    networkChanges = this._networkSubject.asObservable();
    userChanges = this._userSubject.asObservable();
    homeChanges = this._homeSubject.asObservable();
    homeUpdation = this._homeUpdationSubject.asObservable();
    actionBarChanges = this._actionBarState.asObservable();
    actionBarTextChanges = this._actionBarText.asObservable();
    actionBarSearchChanges = this._actionBarSearch.asObservable();
    playerButtonChanges = this._playerButton.asObservable();
    drawerSwipeChanges = this._drawerSwipeState.asObservable();
    activescreen = this._activescreen.asObservable();
    isLogin = this._isLogin.asObservable();
    showPlayer = this._showPlayer.asObservable();
    showFooter = this._showFooter.asObservable();
    showError = this._showError.asObservable();
    showConcert = this._showConcert.asObservable();
    concertData = this._concertData.asObservable();

    currentUser;

    _radRef: RadSideDrawer;
    currentPage: string;
    public errorMessage: string;

    public concertDetail: any;

    isLoggedIn: boolean;

    constructor() {

        this.isLoggedIn = false;

        this.checkNetwork();
        this.monitorNetworkStart()
        this.networkChanges.subscribe((state: boolean) => {
            if (state) {
                this.isConnected = true;
                console.log("Connected")
            }
            else {
                this.isConnected = false;
                console.log("Not Connected")
            }
        })
    }

    newUser(user: any) {
        this._userSubject.next(user);
    }

    setUser(user: any, xRoleKey: string) {
        if (xRoleKey != undefined && xRoleKey != "" && xRoleKey != null) {
            // Values.writeString(Values.X_ROLE_KEY, xRoleKey);
            this.currentUser = user;
            this._userSubject.next(this.currentUser);
        }
    }

    monitorNetworkStart() {

        connectivity.startMonitoring((newConnectionType) => {
            switch (newConnectionType) {
                case connectivity.connectionType.none:
                    this._networkSubject.next(false)
                    this.isConnected = false;
                    break;

                case connectivity.connectionType.wifi:
                    this._networkSubject.next(true)
                    this.isConnected = true;
                    break;

                case connectivity.connectionType.mobile:
                    this._networkSubject.next(true)
                    this.isConnected = true;
                    break;

            }
        });
    }
    checkNetwork() {
        const type = connectivity.getConnectionType();

        switch (type) {
            case connectivity.connectionType.none:
                console.log("No connection");
                this.isConnected = false;
                break;
            case connectivity.connectionType.wifi:
                console.log("WiFi connection");
                this.isConnected = true;
                break;
            case connectivity.connectionType.mobile:
                console.log("Mobile connection");
                this.isConnected = true;
                break;
            default:
                this.isConnected = false
                break;
        }
    }

    homeSelector(state: boolean) {
        this._homeSubject.next(state);
    }

    updateHomeData(data: any) {
        this._homeUpdationSubject.next(data);
    }

    openDrawer() {
        this._radRef.showDrawer();
    }

    actionBarState(state: boolean) {
        this._actionBarState.next(state);
    }

    actionBarText(text: string) {
        this._actionBarText.next(text);
    }

    buttonState(state: boolean) {
        this._playerButton.next(state);
    }

    actionBarSearch(state: boolean) {
        this._actionBarSearch.next(state);
    }

    drawerSwipe(state: boolean) {
        this._drawerSwipeState.next(state);
    }

    logout() {
        this.currentUser = null;
        this._userSubject.next(null);
    }

    activeScreen(screen: string) {
        console.log('screen', screen)
        this._activescreen.next(screen);
        this.currentPage = screen;
    }

    loginUpdate(state: boolean) {
        this.isLoggedIn = state;
        this._isLogin.next(state);
    }

    showHidePlayer(state: boolean) {
        this._showPlayer.next(state);
    }

    showHideFooter(state: boolean) {
        this._showFooter.next(state);
    }

    showErrorDialog(state: boolean, message: string) {
        this.errorMessage = message;
        this._showError.next(state);
        console.log('service error', this.errorMessage)
    }

    showHideConcert(state: boolean) {
        this._showConcert.next(state);
    }

    showConcertData(data: any) {
        this.concertDetail = data;
    }

}