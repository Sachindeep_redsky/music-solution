import { Component, OnInit, ViewChild } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { RouterExtensions } from "nativescript-angular/router";
import { ModalComponent } from "~/app/modal/modal.component";
import { ad } from "tns-core-modules/utils/utils"
import { HttpClient } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { UserService } from "~/app/services/user.service";

@Component({
    moduleId: module.id,
    selector: "m-register",
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {

    @ViewChild('errorDialog', { static: false }) errorDialog: ModalComponent;

    pageHeading: string;
    usernameHint: string;
    username: string;
    passwordHint: string;
    password: string;
    confirmPasswordHint: string;
    confirmPassword: string;
    emailHint: string;
    email: string;
    signupButton: string;
    signIn: string;
    appLogo: string;
    errorText: string;
    isLoading: boolean;
    constructor(private page: Page, private http: HttpClient, private routerExtensions: RouterExtensions, private userService: UserService) {
        this.page.actionBarHidden = true;
    }

    ngOnInit() {

        this.userService.showHideFooter(false);
        this.userService.showHidePlayer(false);
        this.userService.activeScreen("register");
        this.pageHeading = "Sign Up for New Account";
        this.usernameHint = "Username";
        this.username = "";
        this.passwordHint = "Password";
        this.password = "";
        this.confirmPasswordHint = "Confirm Password";
        this.confirmPassword = "";
        this.emailHint = "Email id";
        this.email = "";
        this.signupButton = "SIGN UP";
        this.signIn = "SIGN IN";
        this.appLogo = "res://logo";
        this.errorText = '';
        this.isLoading = false;

    }

    onUsernameTextChange(args: any) {
        var textField = <TextField>args.object;
        this.username = textField.text;
    }

    onPasswordTextChange(args: any) {
        var textField = <TextField>args.object;
        this.password = textField.text;
    }

    onConfirmPasswordTextChange(args: any) {
        var textField = <TextField>args.object;
        this.confirmPassword = textField.text;
    }

    onEmailTextChange(args: any) {
        var textField = <TextField>args.object;
        this.email = textField.text;
    }

    onSignin() {
        this.routerExtensions.navigate(['/login']);
    }

    onSignUpButton() {

        ad.dismissSoftInput();

        if (this.username == "") {
            this.errorDialog.show();
            this.errorText = "Please enter full name";
            return;
        }
        else if (this.email == "") {
            this.errorDialog.show();
            this.errorText = "Please enter email";
            return;
        }
        else if (this.password == "") {
            this.errorDialog.show();
            this.errorText = "Please enter password";
            return;
        }
        else if (this.password.length < 6) {
            this.errorDialog.show();
            this.errorText = "Password should be at least 6 digits";
            return;
        }
        else if (this.confirmPassword != this.password) {
            this.errorDialog.show();
            this.errorText = "Password mismatch";
            return;
        }
        else {
            this.isLoading = true;

            var user = {
                userName: this.username,
                email: this.email,
                password: this.password,
                loginType: 'app'
            }

            var headers = {
                'Content-Type': 'application/json'
            }

            this.http.post(Values.BASE_URL + `users`, user, { headers: headers }).subscribe((res: any) => {
                console.log("RES:::SIGNUP", res)
                if (res && res.isSuccess && res.data) {
                    let extendedNavigationExtras: ExtendedNavigationExtras = {
                        queryParams: {
                            email: this.email,
                            from: 'register'
                        },
                    };
                    this.isLoading = false;
                    this.routerExtensions.navigate(['/verify-otp'], extendedNavigationExtras);
                }
            }, (error) => {
                console.log('Error:', error);
                this.isLoading = false;
                if (error.error.statusCode == 422 && error.error.error == "user_already_exists") {
                    this.errorText = "User already exists"
                } else {
                    this.errorText = "Something went wrong";
                }
                this.errorDialog.show();
            })
        }

    }

    onErrorOKTap(args: any) {
        this.errorDialog.hide();
    }

}