import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Values } from "~/app/values/values";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Page } from "tns-core-modules/ui/page/page";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { RecentlyPlayedService } from "~/app/services/recently-played.service";
import { SongService } from "~/app/services/song.service";
import { initializeOnAngular } from "nativescript-image-cache";



@Component({
    selector: "home",
    templateUrl: "./home.component.html",
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit, OnDestroy {


    pageHeading: string;
    recentlyPlayed = [];
    selectedSong: any;
    recentlyHeading: string;
    recommendedSongs = [];
    mostViewedSongs = [];
    recommendedHeading: string;
    mostViewedHeading: string;
    mostRatedHeading: string;
    popularAlbumsHeading: string;
    popularAlbums = []
    popularAlbumHeading: string;
    popularArtists = []
    popularArtistHeading: string;
    genresMoodsHeading: string;
    mostRatedSongs = []
    popularPlaylists = [];
    popularPlaylistsHeading: string;
    genresMoods;
    headers: HttpHeaders;
    dialogText: string;
    user: any;
    showPlayer: boolean;
    sliderInterval: any
    isRendering: boolean;
    isRecentlyPlayedRendering: boolean;
    isRecommendedRendering: boolean;
    isMostViewedRendering: boolean;
    isMostRatedRendering: boolean;
    isPopularAlbumRendering: boolean;
    isPopularPlaylistRendering: boolean;
    isEventsRendering: boolean;
    showRecentlyPlayed: boolean;
    showRecommendedSongs: boolean;
    showMostViewedSongs: boolean;
    showMostRatedSongs: boolean;
    showPopularAlbum: boolean;
    showPopularPlaylist: boolean;
    concerts_text: string;
    concerts_icon: string;
    searchText: string;
    isInternetConnection: boolean;
    internetMessage: string;
    appPath: string;
    downloadImage: string;
    wifiOffIcon: string;
    isLoading: boolean;
    constructor(private routerExtensions: RouterExtensions, private userService: UserService, private ref: ChangeDetectorRef,
        private http: HttpClient, private page: Page, private recentlyPlayedService: RecentlyPlayedService, private songService: SongService) {
        this.userService.showHideFooter(true);
        this.userService.activeScreen("home");
        this.isRendering = false;
        this.isRecentlyPlayedRendering = false;
        this.isRecommendedRendering = false;
        this.isMostViewedRendering = false;
        this.isMostRatedRendering = false;
        this.isPopularAlbumRendering = false;
        this.isPopularPlaylistRendering = false;
        this.isEventsRendering = true;
        this.showRecentlyPlayed = false;
        this.showMostViewedSongs = false;
        this.showMostRatedSongs = false;
        this.showPopularAlbum = false;
        this.showPopularPlaylist = false;

        this.searchText = "";

        if (Values.readString(Values.X_ACCESS_TOKEN, '') != '' && Values.readString(Values.X_ACCESS_TOKEN, '') != undefined) {

            this.headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
            });
            this.user = JSON.parse(Values.readString(Values.USER, ''));

        } else {
            this.user = {
                _id: ""
            }
        }
    }

    ngOnInit(): void {
        this.page.actionBarHidden = true;
        this.userService.activeScreen("home");
        setTimeout(() => {
            this.userService.showHideFooter(true);
        }, 100)
        let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
        let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');
        let list;

        if (stringifiedList) {
            list = JSON.parse(stringifiedList);
        }

        if (index != undefined && index != null) {
            if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
                this.userService.showHidePlayer(true);
            } else {
                this.userService.showHidePlayer(false)
            }
        } else {
            this.userService.showHidePlayer(false)
        }

        this.pageHeading = "Explore";
        this.recentlyHeading = "Recently Played";
        this.recommendedHeading = "Recommended for you";
        this.mostRatedHeading = "Most Rated"
        this.mostViewedHeading = "Most Viewed"
        this.popularAlbumHeading = "Popular Albums";
        this.popularPlaylistsHeading = "Popular Playlists"
        this.popularArtistHeading = "Popular Artists";
        this.genresMoodsHeading = "Genres & Moods";
        this.popularAlbumsHeading = "Latest Albums";
        this.dialogText = "Purchase this Song";
        this.concerts_text = "Concerts";
        this.concerts_icon = "res://events";
        this.downloadImage = "res://download"
        this.showPlayer = true;
        this.recentlyPlayed = []
        this.recommendedSongs = []
        this.mostViewedSongs = []
        this.mostRatedSongs = []
        this.popularAlbums = []
        this.popularPlaylists = [];
        this.popularArtists = [
            { name: "DIVINE", image: "~/assets/artist1.png" },
        ];
        this.isInternetConnection = this.userService.isConnected;

        this.wifiOffIcon = "res://wifi_off";

        this.internetMessage = "Having trouble connecting";

        this.userService.networkChanges.subscribe((state) => {
            if (state == true) {

                this.initialCall();
                this.isInternetConnection = true;
            }
            if (state == false) {
                this.isInternetConnection = false;
            }
        });
        if (this.isInternetConnection) {
            this.initialCall();
        }

        this.page.on('navigatedTo', (data) => {
            if (data.isBackNavigation) {
                console.log("BACKKKKKKKKKKKKKKKKKKKKK");
                this.userService.showHideFooter(true);
                this.userService.activeScreen("home");
                let index = Values.readNumber(Values.CURRENT_SONG_INDEX, 0);
                let stringifiedList = Values.readString(Values.CURRENT_SONG_LIST, '');

                let list

                if (stringifiedList) {
                    list = JSON.parse(stringifiedList);
                }
                if (index != undefined && index != null) {
                    if ((this.songService && this.songService.songList && this.songService.songList[index]) || list && list[index]) {
                        this.userService.showHidePlayer(true);
                    } else {
                        this.userService.showHidePlayer(false)
                    }
                } else {
                    this.userService.showHidePlayer(false)
                }

                this.recentlyPlayed = [];
                this.getHistory()
                this.ref.detectChanges()
            }
        });

        setTimeout(() => {
            this.isRendering = true;
        }, 10)

    }

    initialCall() {
        this.getRecommendedSongs();
        // this.getEvents();
        this.getMostViewedSongs();
        this.getMostRatedSongs();
        this.getLatestAlbums();
        this.getPopularPlaylists();
        this.getHistory();
    }

    ngOnDestroy() {
        clearInterval(this.sliderInterval)
    }

    onTryAgain() {
        this.userService.checkNetwork();
        this.isInternetConnection = this.userService.isConnected;
    }

    onSearchTextChange(args: any) {
        this.searchText = args.object.text;
    }

    onMusicSearch() {
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "searchText": this.searchText
            },
        };
        this.routerExtensions.navigate(['/search'], extendedNavigationExtras);
    }

    getHistory() {
        this.recentlyPlayedService.checkSongExists().then((value) => {
            let list = this.recentlyPlayedService.getHistory();
            if (list && list.length > 0) {
                this.recentlyPlayed = [];
                setTimeout(() => {
                    list.forEach(song => {
                        this.recentlyPlayed.push({
                            id: song.id,
                            name: song.name,
                            artist: song.artistName,
                            image: song.image.resize_url,
                            song: { url: song.song.url },
                            price: song.price,
                            isSongPurchase: song.isSongPurchase
                        })
                    });
                    this.showRecentlyPlayed = true;
                    this.isRecentlyPlayedRendering = true;
                }, 50)
            } else {
                this.showRecentlyPlayed = false;
                this.isRecentlyPlayedRendering = true;
            }

        })

    }

    getRecommendedSongs() {
        this.http.get(Values.BASE_URL + `songs?userId=${this.user._id}&status=active&isRecommanded=true&pageNo=1&items=20`, {
            headers: this.headers
        }).subscribe((res: any) => {
            if (res != null && res != undefined) {
                if (res.isSuccess == true) {
                    if (res.data.songs.length > 0) {
                        this.recommendedSongs = [];
                        for (var i = 0; i < res.data.songs.length; i++) {
                            console.log("ISSONGPURCHASED:::::", res.data.songs[i].name, res.data.songs[i].isSongPurchase);
                            this.recommendedSongs.push({
                                id: res.data.songs[i].id,
                                name: res.data.songs[i].name,
                                artist: res.data.songs[i].artistName,
                                image: res.data.songs[i].image.resize_url,
                                song: { url: res.data.songs[i].song.url },
                                price: res.data.songs[i].price,
                                isSongPurchase: res.data.songs[i].isSongPurchase,
                                isLoading: false
                            })
                        }
                        this.showRecommendedSongs = true;
                        setTimeout(() => {
                            this.isRecommendedRendering = true;
                        }, 50)
                    } else {
                        this.showRecommendedSongs = false;
                        this.isRecommendedRendering = false;
                    }
                }
            }
        }, error => {
            console.log(error.error.error);
            if (error.error.error == 'unauthorized_user') {
                this.userService.showErrorDialog(true, 'Session timeout. Login again');
                this.userService.loginUpdate(false);
                this.routerExtensions.navigate(['/login']);
            }
        });
    }

    getMostViewedSongs() {
        this.http.get(Values.BASE_URL + `songs?userId=${this.user._id}&topViewed=true&pageNo=1&items=20`, {
            headers: this.headers
        })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        if (res.data.songs.length > 0) {
                            this.mostViewedSongs = [];
                            for (var i = 0; i < res.data.songs.length; i++) {
                                this.mostViewedSongs.push({
                                    id: res.data.songs[i].id,
                                    name: res.data.songs[i].name,
                                    artist: res.data.songs[i].artistName,
                                    image: res.data.songs[i].image.resize_url,
                                    song: { url: res.data.songs[i].song.url },
                                    price: res.data.songs[i].price,
                                    isSongPurchase: res.data.songs[i].isSongPurchase,
                                    isLoading: false

                                })
                            }
                        } else {
                        }
                        setTimeout(() => {
                            this.isMostViewedRendering = true;
                        }, 50)
                    }
                }
            }, error => {
                if (error.error.error == 'unauthorized_user') {
                    this.userService.showErrorDialog(true, 'Session timeout. Login again');
                    // alert('Session timeout. Login again');
                    this.userService.loginUpdate(false);
                    this.routerExtensions.navigate(['/login']);
                }
            });

    }

    getMostRatedSongs() {

        this.http.get(Values.BASE_URL + `songs?userId=${this.user._id}&topRated=true&pageNo=1&items=20`, {
            headers: this.headers
        })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        if (res.data.songs.length > 0) {
                            this.mostRatedSongs = [];
                            for (var i = 0; i < res.data.songs.length; i++) {

                                this.mostRatedSongs.push({
                                    id: res.data.songs[i].id,
                                    name: res.data.songs[i].name,
                                    artist: res.data.songs[i].artistName,
                                    image: res.data.songs[i].image.resize_url,
                                    song: { url: res.data.songs[i].song.url },
                                    price: res.data.songs[i].price,
                                    isSongPurchase: res.data.songs[i].isSongPurchase,
                                    isLoading: false

                                })
                            }
                        } else {
                        }
                        setTimeout(() => {
                            this.isMostRatedRendering = true;
                        }, 50)
                    }
                }
            }, error => {
                console.log(error.error.error);
                if (error.error.error == 'unauthorized_user') {
                    this.userService.showErrorDialog(true, 'Session timeout. Login again');
                    this.userService.loginUpdate(false);
                    this.routerExtensions.navigate(['/login']);
                }
            });
    }

    getLatestAlbums() {

        this.http.get(Values.BASE_URL + `categories?pageNo=1&items=10&status=active&type=album`, {
            headers: this.headers
        }).subscribe((res: any) => {
            if (res != null && res != undefined) {
                if (res.isSuccess == true) {
                    if (res.data.category.length > 0) {
                        this.popularAlbums = [];
                        for (var i = 0; i < res.data.category.length; i++) {

                            this.popularAlbums.push({
                                id: res.data.category[i]._id,
                                name: res.data.category[i].name,
                                artist: res.data.category[i].artistName,
                                image: res.data.category[i].image.resize_url,
                            })
                        }
                    } else {
                    }
                    setTimeout(() => {
                        this.isPopularAlbumRendering = true;
                    }, 50)
                }
            }
        }, error => {

            console.log(error.error.error);
            if (error.error.error == 'unauthorized_user') {
                this.userService.showErrorDialog(true, 'Session timeout. Login again');
                this.userService.loginUpdate(false);
                this.routerExtensions.navigate(['/login']);
            }
        });

    }

    getPopularPlaylists() {
        this.http.get(Values.BASE_URL + `categories?pageNo=1&items=10&status=active&type=category`, {
            headers: this.headers
        })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        if (res.data.category.length > 0) {
                            this.popularPlaylists = [];
                            for (var i = 0; i < res.data.category.length; i++) {
                                this.popularPlaylists.push({
                                    id: res.data.category[i]._id,
                                    name: res.data.category[i].name,
                                    artist: res.data.category[i].artistName,
                                    image: res.data.category[i].image.resize_url,
                                })
                            }
                        } else {
                        }
                        setTimeout(() => {
                            this.isPopularPlaylistRendering = true;
                        }, 50)
                    }
                }
            }, error => {
                console.log(error.error.error);
                if (error.error.error == 'unauthorized_user') {
                    this.userService.showErrorDialog(true, 'Session timeout. Login again');
                    this.userService.loginUpdate(false);
                    this.routerExtensions.navigate(['/login']);
                }
            });
    }

    async onDownload(item: any, i: number) {
        item.isLoading = true
        await this.songService.downloadSong(item);
        item.isLoading = false
    }

    onPopularArtists(item: any) {
        this.routerExtensions.navigate(['/artist']);
    }

    onPopularAlbums(item) {

        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": item.id,
                "type": 'albumId'
            },
        };

        this.routerExtensions.navigate(['/albums'], extendedNavigationExtras);
    }

    onMostRatedSongs(item, i: number) {

        this.selectedSong = item;
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": item.id,
                "index": i,
                "list": JSON.stringify(this.mostRatedSongs)
            },
        };

        this.songService.setCurrentPlaylist(this.mostRatedSongs, i);
        this.routerExtensions.navigate(['/player'], extendedNavigationExtras);

    }

    onMostViewedSongs(item, i: number) {
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": item.id,
                "list": JSON.stringify(this.mostViewedSongs),
                "index": i
            },
        };
        this.songService.setCurrentPlaylist(this.mostViewedSongs, i);
        this.routerExtensions.navigate(['/player'], extendedNavigationExtras);
    }

    onRecommendedSongs(item: any, i: number) {
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": item.id,
                "list": JSON.stringify(this.recommendedSongs),
                "index": i
            },
        };
        this.songService.setCurrentPlaylist(this.recommendedSongs, i);
        this.routerExtensions.navigate(['/player'], extendedNavigationExtras);
    }

    onPopularPlaylists(item) {
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": item.id,
                "type": 'categoryId'
            },
        };
        this.routerExtensions.navigate(['/albums'], extendedNavigationExtras);
    }

    onRecentlyPlayed(item, i: number) {
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": item.id,
                "list": JSON.stringify(this.recentlyPlayed),
                "index": i
            },
        };
        this.songService.setCurrentPlaylist(this.recentlyPlayed, i);
        this.routerExtensions.navigate(['/player'], extendedNavigationExtras);
    }

    onConcerts() {
        this.routerExtensions.navigate(['/concerts'])
    }

    changePageEvent(args) {
        var changeEventText = 'Changed to slide index: ' + args.index;
    };



}



