import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { FooterModule } from "../shared/footer/footer.module";
import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./components/home.component";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { PlayerModule } from './../shared/player/player.module';
import { NgModalModule } from "../modal/ng-modal";
import { GridViewModule } from "nativescript-grid-view/angular";
import { ConcertDetailModule } from "../shared/concert-detail/concert-detail.module";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        GridViewModule,
        HomeRoutingModule,
        FooterModule,
        NativeScriptUIListViewModule,
        PlayerModule,
        NgModalModule,
        ConcertDetailModule,
    ],
    declarations: [
        HomeComponent,
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers: []
})
export class HomeModule { }
